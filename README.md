Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>

[![pipeline status](https://gitlab.com/piluke/BasicEventEngine/badges/master/pipeline.svg)](https://gitlab.com/piluke/BasicEventEngine/commits/master)

![In-Game Screenshot](https://gitlab.com/piluke/BasicEventEngine/raw/master/screenshot.png)

BasicEventEngine is an event-driven game engine which the user can interface
with via the Texture, Sound, Font, Path, Timeline, Mesh, Light, Script, Object,
and Room classes. The Room class is used to organize the resources for each
section of the game and the Object class is used to act upon the events from
the main loop. Example Objects and Rooms are included in the `resources/`
directory with an example map in `maps/` which makes use of them.

The available events are the virtual functions of the Object class which are on
lines 100 to 130 in the file `bee/resource/object.hpp`

The software is still in alpha so do not expect any sort of stability. I'm
slowly adding more content to the wiki and Doxygen via `doc/Doxyfile` so be
sure to check those out first and feel free to email me if you have any further
questions! Also I'm glad to accept contributions to the engine code or feature
requests, preferably via Gitlab. And above all, report bugs! :)

p.s. See LICENSE for info about the MIT License

## How to use on Linux

This program can compile under Arch Linux, Ubuntu 16.04, and Windows 10 but at
the moment support is only offered for Linux.

1. Install git and [git-lfs][1] so that you can download the resources for the
example.

        # For Arch Linux, git-lfs is available in the AUR
        pikaur -S git git-lfs

2. Install the required libraries:

        # Arch Linux
        sudo pacman -S cmake sdl2 sdl2_image sdl2_ttf sdl2_mixer sdl2_net glew glm freeglut libxmu assimp xz libarchive curl
        # Ubuntu
        sudo apt-get install cmake libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-mixer-dev libsdl2-net-dev libglew-dev libglm-dev freeglut3-dev libxmu-dev libassimp-dev xz-utils libarchive-dev libcurl-dev

3. *(Optional)* Edit the base resources in `resources/` and the corresponding
subdirectories in order to change which resources are available prior to
loading maps.

4. Compile and run the program with the following command:

        # Build with debug mode in the build/ subdirectory
        ./build.sh debug build

See [the wiki page][2] for additional uses of `build.sh`:

## How to use on Windows

Even though this program can compile on Windows 10, I am not offering any
support for the Windows platform because it is terribly annoying to get
anything to compile correctly. Luckily I've made a handy compile script for you
called `./build.bat` which should work if your environment is setup like mine.

1. *(Recommended)* Clone the project with [Git for Windows][3]. Make sure to
check the box to also install Git LFS so the example resources can be easily
downloaded.

2. Install the required libraries from their official websites: SDL2,
SDL2_image, SDL2_ttf, SDL2_mixer, SDL2_net, GLEW, GLM, ASSIMP, and XZ Utils.

    The current project setup assumes that each of these
libraries has its DLL in the `win/bin/` directory, its header file in the
`win/include/`, and its .lib file in ``win/lib/``. Since it can be a pain to
find and set these all up, feel free to download the libraries from [here][4]
and extract them to `win/`.

3. Install [CMake][5] and [Visual Studio Community 2017][6] (CMake for this
project and VS for the CPython and libcurl dependencies).

    This project is also setup for out-of-the-box debugging with Visual Studio.
Just open `build-win/{game}.sln`

4. *(Optional)* Edit the base resources in `resources/` and the corresponding
subdirectories in order to change which resources are available prior to
loading maps.

5. Configure the desired build type, e.g. "Debug" or "Release", by editing line
16 of `build.bat`. The game's executable and version numbers can also be
configured by editing the corresponding value in that script.

6. Compile and run the program with the following command:

        ./build.bat

## How to use on MacOS

1. Install [Homebrew][7] to manage the libraries in the next step.

2. Install the required libraries:

        brew install git git-lfs cmake sdl2 sdl2_image sdl2_ttf sdl2_mixer sdl2_net glm assimp xz libarchive curl

3. Clone the project after installing Git LFS in the previous step.

3. *(Optional)* Edit the base resources in `resources/` and the corresponding
subdirectories in order to change which resources are available prior to
loading maps.

4. Compile and run the program with the following command:

        # Build with debug mode in the build-mac/ subdirectory
        ./build.sh debug build-mac

[1]: https://git-lfs.github.com/                               "Git LFS instructions"
[2]: https://github.com/piluke/BasicEventEngine/wiki/build.sh  "BEE Wiki for build.sh"
[3]: https://gitforwindows.org/                                "Git for Windows"
[4]: https://lukemontalvo.us/BasicEventEngine/win.zip          "Windows Library Files"
[5]: https://cmake.org/download/                               "CMake"
[6]: https://www.visualstudio.com/                             "Visual Studio"
[7]: https://brew.sh/                                          "Homebrew"
