/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_MESSAGEBOX_H
#define TESTS_UTIL_MESSAGEBOX_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/messagebox.hpp"
#include "../../bee/util/platform.hpp"

#include "../../bee/init/gameoptions.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("messagebox") {
	if ((!bee::get_option("extensive_assert").i)||(bee::get_option("is_headless").i)) {
		return;
	}

	REQUIRE(util::show_message("Press Button 1", "Button 1", "Button 2", "Button 3") == 0);
	REQUIRE(util::show_message("Press Button 2", "Button 1", "Button 2", "Button 3") == 1);
	REQUIRE(util::show_message("Press Button 3", "Button 1", "Button 2", "Button 3") == 2);
	REQUIRE(util::show_message("Press Button 2", "Button 1", "Button 2", "") == 1);
	REQUIRE(util::show_message("Press Button 1", "Button 1", "", "") == 0);
	REQUIRE(util::show_question("Press Yes") == true);
	REQUIRE(util::show_question("Press No") == false);
	REQUIRE(util::show_message("Press OK") == 0);
	REQUIRE(util::show_warning("Press OK") == 0);
	REQUIRE(util::show_error("Press OK") == 0);
	if (util::platform::get_platform() != util::E_PLATFORM::MACOS) { // There's no close button on modal dialogs on MacOS
		REQUIRE(util::show_message("Close the window without pressing OK") == -1);
	}
}

TEST_SUITE_END();

#endif // TESTS_UTIL_MESSAGEBOX_H
