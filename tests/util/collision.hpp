/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_COLLISION_H
#define TESTS_UTIL_COLLISION_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/collision.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("collision/rect") {
	SDL_Rect a1 = {0, 0, 10, 10};
	SDL_Rect b1 = {5, 5, 10, 20};
	CHECK(util::check_collision(a1, b1) == true);

	SDL_Rect a2 = {0, 0, 10, 10};
	SDL_Rect b2 = {15, 0, 10, 20};
	CHECK(util::check_collision(a2, b2) == false);

	SDL_Rect a3 = {0, 0, 10, 10};
	SDL_Rect b3 = {0, 15, 10, 20};
	CHECK(util::check_collision(a3, b3) == false);

	SDL_Rect a4 = {0, 0, 10, 10};
	SDL_Rect b4 = {-30, 0, 10, 20};
	CHECK(util::check_collision(a4, b4) == false);

	SDL_Rect a5 = {0, 0, 10, 10};
	SDL_Rect b5 = {0, -30, 10, 20};
	CHECK(util::check_collision(a5, b5) == false);
}
TEST_CASE("collision/bounce") {
	CHECK(util::angle_hbounce(60.0) == 120.0);
	CHECK(util::angle_hbounce(90.0) == 90.0);
	CHECK(util::angle_hbounce(120.0) == 60.0);
	CHECK(util::angle_hbounce(180.0) == 0.0);
	CHECK(util::angle_hbounce(240.0) == 300.0);

	CHECK(util::angle_vbounce(60.0) == 300.0);
	CHECK(util::angle_vbounce(90.0) == 270.0);
	CHECK(util::angle_vbounce(120.0) == 240.0);
	CHECK(util::angle_vbounce(180.0) == 180.0);
	CHECK(util::angle_vbounce(300.0) == 60.0);
}

TEST_SUITE_END();

#endif // TESTS_UTIL_COLLISION_H
