/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_STRING_H
#define TESTS_UTIL_STRING_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/string.hpp"

#include "../../bee/init/gameoptions.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("string/charcode") {
	CHECK(util::chr(65) == "A");
	Uint8 ca1[] = {65, 66, 67};
	CHECK(util::chra(3, ca1) == std::string("ABC"));
	std::vector<Uint8> ca2 = util::orda("ABC");
	CHECK(util::chra(ca2) == util::chra(ca2.size(), ca1));
}
TEST_CASE("string/vector") {
	std::vector<std::string> v1 = {"ab", "1.0", "\"c d\"", "test"};
	CHECK(util::splitv("ab 1.0 \"c d\" test", ' ', true) == v1);

	std::vector<std::string> v2 = {"ab", "1.0", "test"};
	CHECK(util::splitv("ab 1.0 test", ' ', true) == v2);

	std::vector<std::string> v3 = {"a", "b", "c"};
	CHECK(util::splitv("a,b,c", ',', true) == v3);
	CHECK(util::joinv(v3, ',') == "a,b,c");
	CHECK(util::splitv(util::joinv(v3, ','), ',', true) == v3);

	std::vector<std::string> v4 = {"\"a", "b\"", "c"};
	std::vector<std::string> vr4 = {"\"a,b\"", "c"};
	CHECK(util::splitv(util::joinv(v4, ','), ',', true) == vr4);

	std::vector<std::string> v5 = {"ab \"a;b\" \"b\"", " cd ef", " \"g;\"\\\"", " \\\"h"};
	CHECK(util::splitv("ab \"a;b\" \"b\"; cd ef; \"g;\"\\\"; \\\"h", ';', true) == v5);
}
TEST_CASE("string/manipulation") {
	CHECK(util::ltrim("  ABC  ") == "ABC  ");
	CHECK(util::rtrim("  ABC  ") == "  ABC");
	CHECK(util::trim("  ABC  ") == "ABC");
	CHECK(util::trim("\tABC\t") == "ABC");
	CHECK(util::trim("\nABC\n") == "ABC");
	CHECK(util::trim(" \t\n") == "");

	CHECK(util::string::lower("ABC") == "abc");
	CHECK(util::string::upper("abc") == "ABC");
	CHECK(util::string::title("abc") == "Abc");
	CHECK(util::string::title("abc def") == "Abc Def");
	CHECK(util::string::letters("ABC123,./") == "ABC");
	CHECK(util::string::digits("ABC123,./") == "123");
	CHECK(util::string::lettersdigits("ABC123,./") == "ABC123");

	CHECK(util::string::tobool("true") == true);
	CHECK(util::string::tobool("false") == false);
	CHECK(util::string::tobool("0") == false);
	CHECK(util::string::tobool("1") == true);
	CHECK(util::string::tobool("True") == true);
	CHECK(util::string::tobool("False") == false);
	CHECK(util::string::tobool("random text") == true);
	CHECK(util::string::tobool("20") == true);

	CHECK(util::string::frombool(true) == "true");
	CHECK(util::string::frombool(false) == "false");

	CHECK(util::string::replace("a,b,c", ",", ":") == "a:b:c");
	CHECK(util::string::replace("a:test:b:test:c", ":test:", ",") == "a,b,c");

	CHECK(util::string::escape("\"Test\"/\\test") == "\\\"Test\\\"/\\\\test");
	CHECK(util::string::unescape("\\\"Test\\\"/\\\\test") == "\"Test\"/\\test");
	CHECK(util::string::unescape(util::string::escape("\"Test\"/\\test")) == "\"Test\"/\\test");

	CHECK(util::string::repeat(5, "a") == "aaaaa");
	CHECK(util::string::repeat(5, "abc") == "abcabcabcabcabc");

	std::vector<std::vector<std::string>> table {
		{"in.", "age", "instrument"},
		{"lam", "22", "bass"},
		{"esd", "21", "piano"},
		{"javj", "21", "guitar"}
	};
	CHECK("\n"+util::string::tabulate(table, true) == "\n\
in.  age instrument\n\
-------------------\n\
lam  22  bass      \n\
esd  21  piano     \n\
javj 21  guitar    \n"
	);
	CHECK("\n"+util::string::tabulate(table) == "\n\
in.  age instrument\n\
lam  22  bass      \n\
esd  21  piano     \n\
javj 21  guitar    \n"
	);

	CHECK(util::string::is_floating("") == false);
	CHECK(util::string::is_floating("-3.14") == true);
	CHECK(util::string::is_floating("1e5") == true);
	CHECK(util::string::is_floating("-.e") == false);
	CHECK(util::string::is_floating("-.e2") == false);
	CHECK(util::string::is_floating("-a.e2") == false);

	CHECK(util::string::matches("abc", "[a-z]*") == true);
	CHECK(util::string::matches("abc", "...") == true);
	CHECK(util::string::matches("abc", ".*") == true);
}
TEST_CASE("string/clipboard") {
	if ((!bee::get_option("extensive_assert").i)||(bee::get_option("is_headless").i)) {
		return;
	}

	SDL_SetMainReady();
	REQUIRE(SDL_Init(SDL_INIT_VIDEO) == 0);

	std::string s (util::clipboard_get_text());
	REQUIRE(util::clipboard_set_text("test") == 0);
	REQUIRE(util::clipboard_get_text() == "test");
	REQUIRE(util::clipboard_has_text() == true);
	REQUIRE(util::clipboard_set_text("") == 0);
	REQUIRE(util::clipboard_has_text() == false);
	util::clipboard_set_text(s);

	SDL_Quit();
}

TEST_SUITE_END();

#endif // TESTS_UTIL_STRING_H
