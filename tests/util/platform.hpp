/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_PLATFORM_H
#define TESTS_UTIL_PLATFORM_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/util/platform.hpp"

#include "../../bee/init/gameoptions.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("platform/general") {
	#if defined(__linux__)
		CHECK(util::platform::get_platform() == util::E_PLATFORM::LINUX);
	#elif defined(WIN32)
		CHECK(util::platform::get_platform() == util::E_PLATFORM::WINDOWS);
	#elif defined(__APPLE__)
		CHECK(util::platform::get_platform() == util::E_PLATFORM::MACOS);
	#else
		CHECK(util::platform::get_platform() == util::E_PLATFORM::UNKNOWN);
	#endif
	CHECK(!util::platform::get_path().empty());
}
TEST_CASE("platform/files") {
	std::string tmpdir = util::platform::mkdtemp("/tmp/bee-XXXXXX");
	REQUIRE(tmpdir != "");
	tmpdir += "/";
	REQUIRE(util::platform::dir_exists(tmpdir) == 1);
	CHECK(util::platform::file_size(tmpdir) > 0);
	REQUIRE(util::platform::mkdir(tmpdir+"platform/", 0755) == 0);
	auto dl (util::platform::dir_list(tmpdir));
	CHECK(std::find(dl.begin(), dl.end(), "platform") != dl.end());
	REQUIRE(util::platform::remove(tmpdir+"platform/") == 0);
	REQUIRE(util::platform::remove(tmpdir) == 0); // Remove the temp directory since it wasn't created by the file util functions
}
TEST_CASE("platform/network") {
	const unsigned char addr1[] = {127, 0, 0, 1};
	CHECK(util::platform::inet_ntop(&addr1) == "127.0.0.1");
	const unsigned char addr2[] = {4, 4, 4, 4};
	CHECK(util::platform::inet_ntop(&addr2) == "4.4.4.4");
	const unsigned char addr3[] = {(unsigned char)-1, (unsigned char)-1, (unsigned char)-1, (unsigned char)-1};
	CHECK(util::platform::inet_ntop(&addr3) == "255.255.255.255");
	const unsigned char addr4[] = {(char)256, 255, 0, 64};
	CHECK(util::platform::inet_ntop(&addr4) == "0.255.0.64");
}
TEST_CASE("platform/commandline") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	for (int c=0; c<=16; ++c) {
		util::platform::commandline_color(&std::cerr, c);
		fprintf(stderr, "%02x", c);
	}
	util::platform::commandline_color_reset(&std::cerr);
	std::cerr << "\n";
}
TEST_CASE("platform/commandline_interactive") {
	if ((!bee::get_option("extensive_assert").i)||(bee::get_option("is_headless").i)) {
		return;
	}

	std::string s;
	if (util::platform::has_commandline_input()) {
		std::getline(std::cin, s);
	}
	REQUIRE(util::platform::has_commandline_input() == false);
	std::cerr << "Press enter to continue...";
	while (!util::platform::has_commandline_input()) {}
	REQUIRE(util::platform::has_commandline_input() == true);
	std::getline(std::cin, s);

	util::platform::commandline_clear();
}

TEST_SUITE_END();

#endif // TESTS_UTIL_PLATFORM_H
