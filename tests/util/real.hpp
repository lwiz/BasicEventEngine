/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_UTIL_REAL_H
#define TESTS_UTIL_REAL_H 1

#include "doctest.h" // Include the required unit testing library

#include <glm/gtc/type_ptr.hpp> // Include for glm::make_mat4()

#include "../../bee/init/gameoptions.hpp"

#include "../../bee/util/real.hpp"

TEST_SUITE_BEGIN("util");

TEST_CASE("real/math") {
	CHECK(util::sign(5) == 1);
	CHECK(util::sign(0) == 0);
	CHECK(util::sign(-5) == -1);
	CHECK(util::sign(5.0) == 1);
	CHECK(util::sign(0.0) == 0);
	CHECK(util::sign(-5.0) == -1);
	CHECK(util::sqr(5) == 25);
	CHECK(util::sqr(5.0) == 25.0);
	CHECK(util::logn(5.0, 1.0) == 0.0);
	CHECK(util::logn(5.0, 5.0) == doctest::Approx(1.0));
	CHECK(util::logn(5.0, 10.0) == doctest::Approx(1.431).epsilon(0.001));
	CHECK(util::degtorad(90.0) == doctest::Approx(PI/2.0));
	CHECK(util::degtorad(360.0) == doctest::Approx(2.0*PI));
	CHECK(util::radtodeg(PI) == doctest::Approx(180.0));
	CHECK(util::radtodeg(PI/3.0) == doctest::Approx(60.0));
	CHECK(util::opposite_angle(0.0) == 180.0);
	CHECK(util::opposite_angle(60.0) == 240.0);
	CHECK(util::opposite_angle(270.0) == 90.0);
	CHECK(util::opposite_angle(360.0) == 180.0);
	CHECK(util::opposite_angle(-90.0) == 90.0);
	CHECK(util::absolute_angle(0.0) == 0.0);
	CHECK(util::absolute_angle(360.0) == 0.0);
	CHECK(util::absolute_angle(-90.0) == 270.0);
	CHECK(util::absolute_angle(810.0) == 90.0);
	CHECK(util::absolute_angle(-810.0) == 270.0);
}
TEST_CASE("real/movement") {
	auto a = util::direction_of(btVector3(0.0, 0.0, 0.0), btVector3(10.0, 10.0, 10.0));
	auto ijk = btVector3(1.0, 1.0, 1.0).normalized();
	CHECK(a.angle(ijk) == 0.0);
	CHECK(a.length() == doctest::Approx(1.0));
	auto b = util::direction_of(btVector3(10.0, 10.0, 10.0), btVector3(0.0, 0.0, 0.0));
	CHECK(b.angle(-ijk) == 0.0);
	CHECK(b.length() == doctest::Approx(1.0));
	CHECK(util::direction_of(1.0, 2.0, 3.0, 4.0) == doctest::Approx(util::opposite_angle(util::direction_of(3.0, 4.0, 1.0, 2.0))));
	CHECK(util::direction_of(0.0, 0.0, 1.0, 0.0) == doctest::Approx(0.0));
	CHECK(util::direction_of(0.0, 0.0, 0.0, 1.0) == doctest::Approx(90.0));
	CHECK(util::direction_of(0.0, 0.0, -1.0, 0.0) == doctest::Approx(180.0));
	CHECK(util::direction_of(0.0, 0.0, 0.0, -1.0) == doctest::Approx(270.0));

	CHECK(util::distance(0.0, 0.0, 1.0, 0.0) == 1.0);
	CHECK(util::distance(0.0, 0.0, 3.0, 4.0) == 5.0);
	CHECK(util::distance(0.0, 0.0, 0.0, 0.0) == 0.0);

	CHECK(util::bt_to_glm_v3(btVector3(0.0, 1.0, 2.0)) == glm::vec3(0.0, 1.0, 2.0));
	CHECK(util::glm_to_bt_v3(glm::vec3(0.0, 1.0, 2.0)) == btVector3(0.0, 1.0, 2.0));
	CHECK(util::bt_to_glm_v3(util::glm_to_bt_v3(glm::vec3(0.0, 1.0, 2.0))) == glm::vec3(0.0, 1.0, 2.0));
	CHECK(util::ai_to_glm_v3(aiVector3D(0.0, 1.0, 2.0)) == glm::vec3(0.0, 1.0, 2.0));

	float m1[16] = {
		// Column major
		0.0,  4.0,  8.0, 12.0,
		1.0,  5.0,  9.0, 13.0,
		2.0,  6.0, 10.0, 14.0,
		3.0,  7.0, 11.0, 15.0
	};
	CHECK(util::ai_to_glm_m4(aiMatrix4x4(
		// Row major
		m1[0], m1[4], m1[8],  m1[12],
		m1[1], m1[5], m1[9],  m1[13],
		m1[2], m1[6], m1[10], m1[14],
		m1[3], m1[7], m1[11], m1[15]
	)) == glm::make_mat4(m1));
	float m2[16] = {
		// Column major
		-81.0, 44.0, 14.0, 0.0,
		4.0, -67.0, 52.0, 0.0,
		46.0, 28.0, -49.0, 0.0,
		0.0, 0.0, 0.0, 1.0
	};
	auto ai_m2 = util::ai_to_glm_m4(aiQuaternion(2.0, 3.0, 4.0, 5.0));
	auto glm_m2 = glm::make_mat4(m2);
	CHECK(ai_m2 == glm_m2);

	CHECK(util::interp_linear(0, 10, 0.5) == 5);
	CHECK(util::interp_linear(0, 10, 0.2) == 2);
	CHECK(util::interp_linear(0, 5, 0.5) == 2);
	CHECK(util::interp_linear(0, 1, 1.0) == 1);
	CHECK(util::interp_linear(0.0, 10.0, 0.5) == 5.0);
	CHECK(util::interp_linear(0.0, 100.0, 0.1) == 10.0);
	CHECK(util::interp_linear(0.0, 10.0, 0.22222) == 2.2222);
	CHECK(util::interp_linear(0.0, 5.0, 0.5) == 2.5);
	CHECK(util::interp_linear(0.0, 1.0, 1.0) == 1.0);
}
TEST_CASE("real/bounds") {
	CHECK(util::is_between(5, 3, 6) == true);
	CHECK(util::is_between(6, 3, 6) == true);
	CHECK(util::is_between(7, 3, 6) == false);
	CHECK(util::is_between(5, 6, 3) == false);
	CHECK(util::is_between(3, 3, 3) == true);
	CHECK(util::is_between(5.0, 3.0, 6.0) == true);

	CHECK(util::is_angle_between(90, 0, 180) == true);
	CHECK(util::is_angle_between(180, 0, 180) == true);
	CHECK(util::is_angle_between(270, 0, 180) == false);
	CHECK(util::is_angle_between(90.0, 0.0, 180.0) == true);
	CHECK(util::is_angle_between(0, 270, 90) == true);
	CHECK(util::is_angle_between(360, 270, 90) == true);

	CHECK(util::fit_bounds(2, 3, 6) == 3);
	CHECK(util::fit_bounds(5, 3, 6) == 5);
	CHECK(util::fit_bounds(6, 3, 6) == 6);
	CHECK(util::fit_bounds(7, 3, 6) == 6);
	CHECK(util::fit_bounds(0, 6, 6) == 6);
	CHECK(util::fit_bounds(10, 6, 3) == 10);
	CHECK(util::fit_bounds(5, 6, 3) == 6);
	CHECK(util::fit_bounds(4, 6, 3) == 3);
	CHECK(util::fit_bounds(5.0, 3.0, 6.0) == 5.0);

	CHECK(util::qmod(5, 0) == 0);
	CHECK(util::qmod(5, 1) == 1);
	CHECK(util::qmod(5, 3) == 2);
	CHECK(util::qmod(3, 3) == 0);
	CHECK(util::qmod(2, 3) == 2);
	CHECK(util::qmod(5.f, 3) == 2.f);
	CHECK(util::qmod(5.f, 3) == fmod(5.f, 3.f));
	CHECK(util::qmod(5.0, 3) == 2.0);
	CHECK(util::qmod(5.0, 3) == fmod(5.0, 3.0));
}
TEST_CASE("real/random") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	CHECK(util::random::get(20) < 20u);
	CHECK(util::is_between(static_cast<int>(util::random::get_range(20, 40)), 20, 40));
	REQUIRE(util::random::set_seed(5) == 5u);
	REQUIRE(util::random::get_seed() == 5u);
	CHECK(util::random::get(0) == 953453411u);
	REQUIRE(util::random::reset_seed() != 1u);
	REQUIRE(util::random::randomize() != 1u);
}
TEST_CASE("real/checksum") {
	CHECK(util::checksum::internal::reflect(0, 8) == 0);
	CHECK(util::checksum::internal::reflect(2, 8) == 128);
	CHECK(util::checksum::internal::reflect(0, 32) == 0);
	CHECK(util::checksum::internal::reflect(2, 32) == 2147483648);

	CHECK(util::checksum::internal::table(0) == 0);
	CHECK(util::checksum::internal::table(2) == 3993919788);

	std::vector<unsigned char> v1 = {65, 66, 67};
	unsigned int crc1 = util::checksum::get(v1);
	CHECK(crc1 == 1625830473);
	CHECK(util::checksum::verify(v1, crc1) == true);

	std::vector<unsigned char> v2 = {120, 121, 122};
	unsigned int crc2 = util::checksum::get(v2);
	CHECK(crc2 == 1178330758);
	CHECK(util::checksum::verify(v2, crc2) == true);
}

TEST_SUITE_END();

#endif // TESTS_UTIL_REAL_H
