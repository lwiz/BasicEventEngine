/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_PYTHON_PYTHON_H
#define TESTS_PYTHON_PYTHON_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/init/gameoptions.hpp"

#include "../../bee/util/platform.hpp"

#include "../../bee/messenger/messagecontents.hpp"

#include "../../bee/core/enginestate.hpp"
#include "../../bee/core/instance.hpp"
#include "../../bee/fs/python.hpp"

#include "../../bee/python/python.hpp"
#include "../../bee/python/beemodule.hpp"

#include "../../bee/resource/texture.hpp"
#include "../../bee/resource/sound.hpp"
#include "../../bee/resource/font.hpp"
#include "../../bee/resource/path.hpp"
#include "../../bee/resource/timeline.hpp"
#include "../../bee/resource/mesh.hpp"
#include "../../bee/resource/light.hpp"
#include "../../bee/resource/script.hpp"
#include "../../bee/resource/object.hpp"
#include "../../bee/resource/room.hpp"

TEST_SUITE_BEGIN("python");

TEST_CASE("repeat") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	for (int i=0; i<10; i++) {
		REQUIRE(bee::python::init() == 0);
		REQUIRE(bee::python::run_string("import bee") == 0);
		REQUIRE(bee::python::close() == 0);
	}
}

TEST_CASE("general") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	REQUIRE(bee::python::init() == 0);

	REQUIRE(bee::python::get_traceback() == "");
	CHECK(bee::python::run_string("a = 1") == 0);
	CHECK(bee::python::run_string("") == 0);
	CHECK(bee::python::run_string("\
try:\n\
	eval('def syntax_error_test:')\n\
except SyntaxError:\n\
	pass\n\
else:\n\
	assert False\
") == 0);
	CHECK(bee::python::run_string("\
try:\n\
	a=2/0\n\
except ZeroDivisionError:\n\
	pass\n\
else:\n\
	assert False\
") == 0);
	CHECK(bee::python::get_traceback() == "");

	// Multiple import
	for (int i=0; i<10; i++) {
		PyObject* module = bee::fs::python::import("resources/scripts/util.py", "");
		CHECK(module != nullptr);
		Py_XDECREF(module);
	}

	CHECK(bee::python::run_file("resources/scripts/asserts.py") == 0);

	REQUIRE(bee::python::close() == 0);
}

TEST_CASE("variant_pyobj_conversion/data") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	REQUIRE(bee::python::init() == 0);

	constexpr auto& ptv = bee::python::pyobj_to_variant;
	constexpr auto& vtp = bee::python::variant_to_pyobj;

	// Simple data
	PyObject *obj1, *obj2;
	obj1 = Py_None;
	CHECK(ptv(obj1) == bee::Variant());
	obj2 = vtp(bee::Variant());
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	// No E_DATA_TYPE::CHAR tests since Python characters are just strings
	obj1 = PyLong_FromLong(2);
	CHECK(ptv(obj1) == bee::Variant(2));
	obj2 = vtp(bee::Variant(2));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	obj1 = PyBool_FromLong(true);
	CHECK(ptv(obj1) == bee::Variant(true));
	obj2 = vtp(bee::Variant(true));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	obj1 = PyFloat_FromDouble(4.0);
	CHECK(ptv(obj1) == bee::Variant(4.0));
	obj2 = vtp(bee::Variant(4.0));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	obj1 = PyUnicode_FromString("test");
	CHECK(ptv(obj1) == bee::Variant("test"));
	obj2 = vtp(bee::Variant("test"));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);

	// Containers
	std::vector<bee::Variant> vec {
		bee::Variant(2),
		bee::Variant("str")
	};
	obj1 = PyList_New(vec.size());
	for (size_t i=0; i<vec.size(); i++) {
		PyList_SetItem(obj1, i, bee::python::variant_to_pyobj(vec.at(i)));
	}
	CHECK(ptv(obj1) == bee::Variant(vec));
	obj2 = vtp(bee::Variant(vec));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);

	std::map<bee::Variant,bee::Variant> map {
		{bee::Variant("key1"), bee::Variant(2.0)},
		{bee::Variant("key2"), bee::Variant(4.0)}
	};
	obj1 = PyDict_New();
	for (auto& item : map) {
		PyDict_SetItem(obj1, bee::python::variant_to_pyobj(item.first), bee::python::variant_to_pyobj(item.second));
	}
	CHECK(ptv(obj1) == bee::Variant(map));
	obj2 = vtp(bee::Variant(map));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);

	REQUIRE(bee::python::close() == 0);
}
TEST_CASE("variant_pyobj_conversion/resources") {
	if (!bee::get_option("extensive_assert").i) {
		return;
	}

	REQUIRE(bee::python::init() == 0);

	bee::messenger::handle();

	constexpr auto& ptv = bee::python::pyobj_to_variant;
	constexpr auto& vtp = bee::python::variant_to_pyobj;

	PyObject *obj1, *obj2;
	bee::Texture* tex = bee::Texture::add("", "");
	obj1 = bee::python::Texture_from(tex);
	CHECK(ptv(obj1) == bee::Variant(tex));
	obj2 = vtp(bee::Variant(tex));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete tex;

	bee::Sound* snd = bee::Sound::add("", "", false);
	obj1 = bee::python::Sound_from(snd);
	CHECK(ptv(obj1) == bee::Variant(snd));
	obj2 = vtp(bee::Variant(snd));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete snd;

	bee::Font* font = bee::Font::add("", "", 12);
	obj1 = bee::python::Font_from(font);
	CHECK(ptv(obj1) == bee::Variant(font));
	obj2 = vtp(bee::Variant(font));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete font;

	bee::Path* path = bee::Path::add("", "");
	obj1 = bee::python::Path_from(path);
	CHECK(ptv(obj1) == bee::Variant(path));
	obj2 = vtp(bee::Variant(path));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete path;

	bee::Timeline* tl = bee::Timeline::add("", "");
	obj1 = bee::python::Timeline_from(tl);
	CHECK(ptv(obj1) == bee::Variant(tl));
	obj2 = vtp(bee::Variant(tl));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete tl;

	bee::Mesh* mesh = bee::Mesh::add("", "");
	obj1 = bee::python::Mesh_from(mesh);
	CHECK(ptv(obj1) == bee::Variant(mesh));
	obj2 = vtp(bee::Variant(mesh));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete mesh;

	bee::Light* lt = bee::Light::add("", "");
	obj1 = bee::python::Light_from(lt);
	CHECK(ptv(obj1) == bee::Variant(lt));
	obj2 = vtp(bee::Variant(lt));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete lt;

	bee::Script* scr = bee::Script::add("", "");
	obj1 = bee::python::Script_from(scr);
	CHECK(ptv(obj1) == bee::Variant(scr));
	obj2 = vtp(bee::Variant(scr));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete scr;

	bee::Object* obj = bee::Object::add("", "");
	obj1 = bee::python::Object_from(obj);
	CHECK(ptv(obj1) == bee::Variant(obj));
	obj2 = vtp(bee::Variant(obj));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);
	delete obj;

	bee::Room* rm = bee::Room::add("", "");
	obj1 = bee::python::Room_from(rm);
	CHECK(ptv(obj1) == bee::Variant(rm));
	obj2 = vtp(bee::Variant(rm));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);

	// Temporarily change the Room to test Instances
	bee::Room* old_room = bee::engine->current_room;
	bee::engine->current_room = rm;
	rm->init();
	rm->room_start();

	bee::Instance* inst = rm->add_instance({}, {0.0, 0.0, 0.0}, {});
	obj1 = bee::python::Instance_from(inst);
	CHECK(ptv(obj1) == bee::Variant(inst));
	obj2 = vtp(bee::Variant(inst));
	CHECK(PyObject_RichCompareBool(obj1, obj2, Py_EQ) == 1);
	Py_DECREF(obj1);
	Py_DECREF(obj2);

	// Change the Room back
	rm->room_end();
	bee::engine->current_room = old_room;
	delete rm;

	// Remove FS and Resource errors from initializing empty Resources
	bee::messenger::internal::remove_messages([] (const bee::MessageContents& msg) -> bool {
		return true;
	});

	REQUIRE(bee::python::close() == 0);
}

TEST_SUITE_END();

#endif // TESTS_PYTHON_PYTHON_H
