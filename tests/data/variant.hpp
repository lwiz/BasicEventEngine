/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef TESTS_DATA_VARIANT_H
#define TESTS_DATA_VARIANT_H 1

#include "doctest.h" // Include the required unit testing library

#include "../../bee/data/variant.hpp"

#include "../../bee/core/instance.hpp"

#include "../../bee/resource/texture.hpp"
#include "../../bee/resource/sound.hpp"
#include "../../bee/resource/font.hpp"
#include "../../bee/resource/path.hpp"
#include "../../bee/resource/timeline.hpp"
#include "../../bee/resource/mesh.hpp"
#include "../../bee/resource/light.hpp"
#include "../../bee/resource/script.hpp"
#include "../../bee/resource/object.hpp"
#include "../../bee/resource/room.hpp"

TEST_SUITE_BEGIN("data");

TEST_CASE("variant1") {
	bee::Variant v1 (bee::E_DATA_TYPE::CHAR);
	v1.c = 'A';

	bee::Variant v2 (v1);

	char c = 'A';

	REQUIRE(v1 == v2);
	REQUIRE(v1.c == c);
}
TEST_CASE("variant2") {
	bee::Variant v1 (bee::E_DATA_TYPE::MAP);
	v1.m.emplace(bee::Variant("a"), bee::Variant(2.0));

	bee::Variant v2 ("{\"a\": 2.0}", true);

	std::map<bee::Variant,bee::Variant> m;
	m.emplace(bee::Variant("a"), bee::Variant(2.0));

	REQUIRE(v1 == v2);
	REQUIRE(v1.m == m);
	REQUIRE(v1.m.at("a") == m.at("a"));
	REQUIRE(v1.m.find("b") == v1.m.end());
	REQUIRE(v1.m.size() == 1);
}
TEST_CASE("variant3") {
	bee::Variant v1 ("[\t]", true);
	bee::Variant v2 (bee::E_DATA_TYPE::VECTOR);
	REQUIRE(v1 == v2);

	bee::Variant v3 ("{\t}", true);
	bee::Variant v4 (bee::E_DATA_TYPE::MAP);
	REQUIRE(v3 == v4);

	bee::Variant v5 ("[\t{}\t]", true);
	bee::Variant v6 (bee::E_DATA_TYPE::VECTOR);
	v6.v.push_back(v4);
	REQUIRE(v5 == v6);

	bee::Variant v7 ("{\t\"\":\t[]\t}", true);
	bee::Variant v8 (bee::E_DATA_TYPE::MAP);
	v8.m.emplace(bee::Variant(""), v2);
}
TEST_CASE("variant/type_index") {
	bee::Variant v;
	v = static_cast<bee::Texture*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Texture*)));
	v = static_cast<bee::Sound*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Sound*)));
	v = static_cast<bee::Font*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Font*)));
	v = static_cast<bee::Path*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Path*)));
	v = static_cast<bee::Timeline*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Timeline*)));
	v = static_cast<bee::Mesh*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Mesh*)));
	v = static_cast<bee::Light*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Light*)));
	v = static_cast<bee::Script*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Script*)));
	v = static_cast<bee::Object*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Object*)));
	v = static_cast<bee::Room*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Room*)));
	v = static_cast<bee::Instance*>(nullptr);
	REQUIRE(v.get_ptype() == std::type_index(typeid(bee::Instance*)));
}

TEST_SUITE_END();

#endif // TESTS_DATA_VARIANT_H
