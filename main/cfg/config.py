# Configure default values
volume(0.2)

# Load map
bee.fs.load_level("lv_test", "maps/lv_test.tar.xz", "", True)

# Output status
log("Loaded config.py")
