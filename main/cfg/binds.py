bind("SDLK_BACKQUOTE", "$ConsoleToggle")
bind("SDLK_ESCAPE", "$Quit")

bind("SDLK_F12", "screenshot()")
bind("SDLK_HOME", "pause()")
bind("SDLK_F3", "bee.set_option(\"is_debug_enabled\", not bee.get_option(\"is_debug_enabled\"))")
