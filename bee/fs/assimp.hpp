/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_FS_ASSIMP_H
#define BEE_FS_ASSIMP_H 1

#include <string>

#include <assimp/scene.h>
#include <assimp/IOSystem.hpp>
#include <assimp/IOStream.hpp>

#include "fs.hpp"

namespace bee { namespace fs { namespace assimp {
	struct IOSystem: public Assimp::IOSystem {
		bool Exists(const char*) const override;
		char getOsSeparator() const override;
		Assimp::IOStream* Open(const char*, const char*) override;
		void Close(Assimp::IOStream*) override;
	};
	class IOStream: public Assimp::IOStream {
		size_t pos;
	public:
		FilePath fp;

		IOStream(FilePath);
		virtual ~IOStream();

		size_t FileSize() const override;

		size_t Read(void*, size_t, size_t) override;
		size_t Write(const void*, size_t, size_t) override;
		void Flush() override;

		aiReturn Seek(size_t, aiOrigin) override;
		size_t Tell() const override;
	};

	int init();

	aiScene* import(const std::string&, unsigned int);
	const char* get_error_string();
}}}

#endif // BEE_FS_ASSIMP_H
