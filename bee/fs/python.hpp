/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_FS_PYTHON_H
#define BEE_FS_PYTHON_H 1

#include <string>

#include <Python.h>

namespace bee { namespace fs { namespace python {
	namespace internal {
		PyObject* get_file_callable(PyObject*);
	}

	PyObject* import(const std::string&, const std::string&);
}}}

#endif // BEE_FS_PYTHON_H
