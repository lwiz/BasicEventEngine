/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "light.hpp" // Include the class resource header

#include "../util/files.hpp"
#include "../util/debug.hpp"
#include "../util/string.hpp"
#include "../util/real.hpp"

#include "../init/gameoptions.hpp"

#include "../messenger/messenger.hpp"

#include "../core/rooms.hpp"
#include "../fs/fs.hpp"

#include "../render/render.hpp"

#include "texture.hpp"
#include "room.hpp"

namespace bee {
	/**
	* Construct the data struct and initialize all values.
	*/
	LightData::LightData() :
		type(E_LIGHT_TYPE::AMBIENT),
		position(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)),
		direction(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)),
		attenuation(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)),
		color({255, 255, 255, 255}),

		comp_type(E_COMPUTATION::NOTHING),
		cache(nullptr),
		has_updated(true)
	{}
	LightData::LightData(Light* l) :
		type(l->get_type()),
		position(l->get_position()),
		direction(l->get_direction()),
		attenuation(l->get_attenuation()),
		color(l->get_color()),

		comp_type(E_COMPUTATION::NOTHING),
		cache(nullptr),
		has_updated(true)
	{}
	LightData::~LightData() {
		if (cache != nullptr) {
			delete cache;
			cache = nullptr;
		}
	}

	void LightData::set_position(glm::vec4 _position) {
		if (position != _position) {
			has_updated = true;
		}
		position = _position;
	}
	void LightData::set_direction(glm::vec4 _direction) {
		_direction = glm::normalize(_direction);
		if (direction != _direction) {
			has_updated = true;
		}
		direction = _direction;
	}
	void LightData::set_attenuation(glm::vec4 _attenuation) {
		_attenuation.w = static_cast<decltype(_attenuation.w)>(
			cos(util::degtorad(_attenuation.w))
		);
		if (attenuation != _attenuation) {
			has_updated = true;
		}
		attenuation = _attenuation;
	}
	void LightData::set_color(RGBA _color) {
		if (color != _color) {
			has_updated = true;
		}
		color = _color;
	}

	void LightData::draw(E_COMPUTATION _comp_type, std::shared_ptr<LightData> ld) {
		if (get_option("is_headless").i) {
			return;
		}
		render::queue_light(_comp_type, ld); // Add the Light to the Room lighting queue
	}
	void LightData::draw(std::shared_ptr<LightData> ld) {
		draw(E_COMPUTATION::DYNAMIC, ld);
	}

	std::map<size_t,Light*> Light::list;
	size_t Light::next_id = 0;

	/**
	* Default construct the Light.
	* @note This constructor should only be directly used for temporary Lights, the other constructor should be used for all other cases.
	*/
	Light::Light() :
		Resource(),

		id(-1),
		name(),
		path(),

		ldata(),
		is_loaded(false)
	{}
	/**
	* Construct the Light, add it to the Light resource list, and set the new name and path.
	* @param _name the name of the Light to use
	* @param _path the path of the Light's config file
	*
	* @throws int(-1) Failed to initialize Resource
	*/
	Light::Light(const std::string& _name, const std::string& _path) :
		Light() // Default initialize all variables
	{
		if (add_to_resources() == static_cast<size_t>(-1)) { // Attempt to add the Light to its resource list
			messenger::send({"engine", "resource"}, E_MESSAGE::WARNING, "Failed to add Light resource: \"" + _name + "\" from " + _path);
			throw -1;
		}

		set_name(_name);
		set_path(_path);
	}
	/**
	* Remove the Light from the resource list.
	*/
	Light::~Light() {
		list.erase(id);
	}

	/**
	* @returns the number of Light resources
	*/
	size_t Light::get_amount() {
		return list.size();
	}
	/**
	* @param id the resource to get
	*
	* @returns the resource with the given id or nullptr if not found
	*/
	Light* Light::get(size_t id) {
		if (list.find(id) != list.end()) {
			return list[id];
		}
		return nullptr;
	}
	/**
	* @param name the name of the desired Light
	*
	* @returns the Light resource with the given name or nullptr if not found
	*/
	Light* Light::get_by_name(const std::string& name) {
		for (auto& light : list) { // Iterate over the Lights in order to find the first one with the given name
			Light* l = light.second;
			if (l != nullptr) {
				if (l->get_name() == name) {
					return l; // Return the desired Light on success
				}
			}
		}
		return nullptr;
	}
	/**
	* Initiliaze, load, and return a newly created Light resource
	* @param name the name to initialize the Light with
	* @param path the path to initialize the Light with
	*
	* @returns the newly loaded Light
	*/
	Light* Light::add(const std::string& name, const std::string& path) {
		Light* new_light = new Light(name, path);
		new_light->load();
		return new_light;
	}

	/**
	* Add the Light to the appropriate resource list.
	*
	* @returns the Light id
	*/
	size_t Light::add_to_resources() {
		if (id == static_cast<size_t>(-1)) { // If the resource needs to be added to the resource list
			id = next_id++;
			list.emplace(id, this); // Add the resource with its new id
		}

		return id;
	}
	/**
	* Reset all resource variables for reinitialization.
	*
	* @retval 0 success
	*/
	int Light::reset() {
		this->free();

		// Reset all properties
		name = "";
		path = "";

		ldata->type = E_LIGHT_TYPE::AMBIENT;
		ldata->position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		ldata->direction = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		ldata->color = {255, 255, 255, 255};

		is_loaded = false;

		return 0;
	}

	/**
	* @returns a map of all the information required to restore the Light
	*/
	std::map<Variant,Variant> Light::serialize() const {
		std::map<Variant,Variant> info;

		info["id"] = static_cast<int>(id);
		info["name"] = name;
		info["path"] = path;

		info["type"] = static_cast<int>(ldata->type);
		info["position"] = {Variant(ldata->position.x), Variant(ldata->position.y), Variant(ldata->position.z), Variant(ldata->position.w)};
		info["direction"] = {Variant(ldata->direction.x), Variant(ldata->direction.y), Variant(ldata->direction.z), Variant(ldata->direction.w)};
		info["attenuation"] = {Variant(ldata->attenuation.x), Variant(ldata->attenuation.y), Variant(ldata->attenuation.z), Variant(ldata->attenuation.w)};
		info["color"] = {
			Variant(static_cast<int>(ldata->color.r)),
			Variant(static_cast<int>(ldata->color.g)),
			Variant(static_cast<int>(ldata->color.b)),
			Variant(static_cast<int>(ldata->color.a))
		};

		info["is_loaded"] = is_loaded;

		return info;
	}
	/**
	* Restore the Light from serialized data.
	* @param m the map of data to use
	*
	* @retval 0 success
	*/
	int Light::deserialize(std::map<Variant,Variant>& m) {
		this->free();

		id = m["id"].i;
		name = m["name"].s;
		path = m["path"].s;

		ldata->type = static_cast<E_LIGHT_TYPE>(m["type"].i);
		ldata->position = glm::vec4(
			m["position"].v[0].f,
			m["position"].v[1].f,
			m["position"].v[2].f,
			m["position"].v[3].f
		);
		ldata->direction = glm::vec4(
			m["direction"].v[0].f,
			m["direction"].v[1].f,
			m["direction"].v[2].f,
			m["direction"].v[3].f
		);
		ldata->attenuation = glm::vec4(
			m["attenuation"].v[0].f,
			m["attenuation"].v[1].f,
			m["attenuation"].v[2].f,
			m["attenuation"].v[3].f
		);
		ldata->color = RGBA(
			m["color"].v[0].i,
			m["color"].v[1].i,
			m["color"].v[2].i,
			m["color"].v[3].i
		);

		is_loaded = false;

		if ((m["is_loaded"].i)&&(load())) {
			return 1;
		}

		return 0;
	}
	/**
	* Print all relevant information about the resource.
	*/
	void Light::print() const {
		Variant m (serialize());
		messenger::send({"engine", "light"}, E_MESSAGE::INFO, "Light " + m.to_str(true));
	}

	size_t Light::get_id() const {
		return id;
	}
	std::string Light::get_name() const {
		return name;
	}
	std::string Light::get_path() const {
		return path;
	}
	E_LIGHT_TYPE Light::get_type() const {
		return ldata->type;
	}
	glm::vec4 Light::get_position() const {
		return ldata->position;
	}
	glm::vec4 Light::get_direction() const {
		return ldata->direction;
	}
	glm::vec4 Light::get_attenuation() const {
		return ldata->attenuation;
	}
	RGBA Light::get_color() const {
		return ldata->color;
	}
	bool Light::get_is_loaded() const {
		return is_loaded;
	}

	void Light::set_name(const std::string& _name) {
		name = _name;
	}
	/**
	* Set the relative or absolute resource path.
	* @param _path the new path to use
	* @note If the first character is '$' then the path will be relative to
	*       the Lights resource directory.
	*/
	void Light::set_path(const std::string& _path) {
		path = _path;
		if ((!_path.empty())&&(_path.front() == '$')) {
			path = "resources/lights"+_path.substr(1);
		}
		is_loaded = false;
	}
	void Light::set_type(E_LIGHT_TYPE _type) {
		ldata->type = _type;
	}
	void Light::set_position(const glm::vec4& _position) {
		ldata->position = _position;
	}
	void Light::set_direction(const glm::vec4& _direction) {
		ldata->direction = glm::normalize(_direction);
	}
	void Light::set_attenuation(const glm::vec4& _attenuation) {
		ldata->attenuation = _attenuation;
	}
	void Light::set_color(RGBA _color) {
		ldata->color = _color;
	}

	/**
	* Load the Light from its path.
	*
	* @retval 0 success
	* @retval 1 failed to load since it's already loaded
	* @retval 2 failed to find the file
	* @retval 3 failed to load the file
	* @retval 4 invalid value
	*/
	int Light::load() {
		if (is_loaded) { // Do not attempt to load the Light if it has already been loaded
	       messenger::send({"engine", "light"}, E_MESSAGE::WARNING, "Failed to load Light \"" + name + "\" because it has already been loaded");
	       return 1;
		}

		FilePath fp (fs::get_file(path));
		if (fp.get_path().empty()) {
			messenger::send({"engine", "light"}, E_MESSAGE::WARNING, "Failed to find Light \"" + name + "\" at \"" + path + "\"");
			return 2;
		}

		std::string cfg (fp.get());
		if (cfg.empty()) { // If the file could not be loaded, output a warning
			messenger::send({"engine", "light"}, E_MESSAGE::WARNING, "Failed to load Light \"" + name + "\" from file \"" + path + "\"");
			return 3;
		}

		// Parse the config file
		Variant m;
		m.interpret(cfg);

		// Clear the old data
		ldata = std::make_shared<LightData>();

		// Load the new data
		std::string type;
		if (m.m.find("type") != m.m.end()) {
			type = util::string::lower(m.m["type"].s);
		}

		if (type == "ambient") {
			ldata->type = E_LIGHT_TYPE::AMBIENT;
		} else if (type == "diffuse") {
			ldata->type = E_LIGHT_TYPE::DIFFUSE;
		} else if (type == "point") {
			ldata->type = E_LIGHT_TYPE::POINT;
		} else if (type == "spot") {
			ldata->type = E_LIGHT_TYPE::SPOT;
		} else {
			messenger::send({"engine", "light"}, E_MESSAGE::WARNING, "Failed to load Light \"" + name + "\": invalid light type \"" + type + "\"");
			return 4;
		}

		if (m.m.find("position") != m.m.end()) {
			ldata->position = glm::vec4(
				m.m["position"].v[0].f,
				m.m["position"].v[1].f,
				m.m["position"].v[2].f,
				m.m["position"].v[3].f
			);
		}
		if (m.m.find("direction") != m.m.end()) {
			ldata->direction = glm::vec4(
				m.m["direction"].v[0].f,
				m.m["direction"].v[1].f,
				m.m["direction"].v[2].f,
				m.m["direction"].v[3].f
			);
		}
		if (m.m.find("attenuation") != m.m.end()) {
			ldata->attenuation = glm::vec4(
				m.m["attenuation"].v[0].f,
				m.m["attenuation"].v[1].f,
				m.m["attenuation"].v[2].f,
				m.m["attenuation"].v[3].f
			);
		}
		if (m.m.find("color") != m.m.end()) {
			ldata->color = RGBA(
				m.m["color"].v[0].i,
				m.m["color"].v[1].i,
				m.m["color"].v[2].i,
				m.m["color"].v[3].i
			);
		}

		is_loaded = true;

		return 0;
	}
	/**
	* Free the LightData.
	*
	* @retval 0 success
	*/
	int Light::free() {
		if (!is_loaded) { // Do not attempt to free the data if the Light has not been loaded
			return 0;
		}

		ldata = std::make_shared<LightData>();

		is_loaded = false;

		return 0;
	}

	/**
	* Queue the Light for drawing in the Room rendering loop.
	*/
	void Light::draw_static() {
		ldata->draw(E_COMPUTATION::STATIC, ldata);
	}
	/**
	* Queue the Light for drawing in the Room rendering loop.
	* @param pos the position to draw at
	* @param dir the direction to draw in
	* @param att the attenuation to draw with
	* @param color the color to draw in
	*/
	void Light::draw_static(const glm::vec4& pos, const glm::vec4& dir, const glm::vec4& att, RGBA color) {
		set_position(pos);
		set_direction(dir);
		set_attenuation(att);
		set_color(color);
		draw_static();
	}
}
