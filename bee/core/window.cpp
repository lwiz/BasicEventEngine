/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <SDL2/SDL.h> // Include the required SDL headers

#include <GL/glew.h> // Include the required OpenGL headers
#include <SDL2/SDL_opengl.h>
#include "../util/windefine.hpp"

#include "window.hpp"

#include "console.hpp"
#include "enginestate.hpp"
#include "rooms.hpp"

#include "../render/render.hpp"
#include "../render/renderer.hpp"
#include "../render/transition.hpp"

#include "../resource/texture.hpp"
#include "../resource/room.hpp"

namespace bee {
	/**
	* @see https://wiki.libsdl.org/SDL_GetWindowTitle for details
	* @returns the current window title string
	*/
	std::string get_window_title() {
		return SDL_GetWindowTitle(engine->renderer->window);
	}
	/**
	* @returns the window coordinates and dimensions
	*/
	SDL_Rect get_window() {
		std::pair<int,int> pos (get_window_pos());
		std::pair<int,int> size (get_window_size());
		return SDL_Rect({pos.first, pos.second, size.first, size.second});
	}
	/**
	* @returns the position of the game window
	*/
	std::pair<int,int> get_window_pos() {
		int wx, wy;
		SDL_GetWindowPosition(engine->renderer->window, &wx, &wy);
		return std::make_pair(wx, wy);
	}
	/**
	* @returns the size of the game window
	*/
	std::pair<int,int> get_window_size() {
		return std::make_pair(engine->width, engine->height);
	}

	/**
	* Set the title string of the current window.
	* @param title the string to set the title to
	*/
	void set_window_title(const std::string& title) {
		SDL_SetWindowTitle(engine->renderer->window, title.c_str());
	}
	/**
	* Set the icon of the current window.
	* @param tex the Texture to use for the icon
	*/
	void set_window_icon(Texture* tex) {
		if (tex != nullptr) {
			SDL_SetWindowIcon(engine->renderer->window, tex->load_surface());
		} else {
			SDL_SetWindowIcon(engine->renderer->window, nullptr);
		}
	}
	/**
	* Set the game window position.
	* @note Give -1 for a coordinate in order to leave it unchanged.
	* @param x the new x-coordinate to move the window to
	* @param y the new y-coordinate to move the window to
	*/
	void set_window_position(int x, int y) {
		if (x < 0) {
			x = get_window_pos().first;
		}
		if (y < 0) {
			y = get_window_pos().second;
		}

		SDL_SetWindowPosition(engine->renderer->window, x, y);
	}
	/**
	* Center the game window on the screen.
	*/
	void set_window_center() {
		set_window_position(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
	}
	/**
	* Set the size of the game window.
	* @note Give -1 for a dimension in order to leave it unchanged.
	* @param width the new width to change the window to
	* @param height the new height to change the window to
	* @param set_sdl whether to update the SDL window size also
	*/
	void set_window_size(int width, int height, bool set_sdl) {
		if (width < 0) {
			width = engine->width;
		}
		if (height < 0) {
			height = engine->height;
		}

		engine->width = width;
		engine->height = height;
		if (engine->renderer->window != nullptr) {
			if (set_sdl) {
				SDL_SetWindowSize(engine->renderer->window, engine->width, engine->height);
			}
			glViewport(0, 0, engine->width, engine->height);

			// Reset render targets
			render::internal::init_transitions();
			render::internal::init_lightmaps();
			get_current_room()->reload_viewports();

			console::internal::resize();
		}
	}
	/**
	* Set the size of the game window.
	* @note Give -1 for a dimension in order to leave it unchanged.
	* @param width the new width to change the window to
	* @param height the new height to change the window to
	*/
	void set_window_size(int width, int height) {
		set_window_size(width, height, true);
	}
}
