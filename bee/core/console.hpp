/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_CORE_CONSOLE_H
#define BEE_CORE_CONSOLE_H 1

#include "../messenger/messagecontents.hpp"

#include "../data/variant.hpp"

#include "../resource/script.hpp"

namespace bee {
	// Forward declaration
	class Instance;

namespace console {
	void reset();

	namespace internal {
		int init();
		int init_ui();
		void close();

		ScriptInterface* get_interface();
		void update_ui();

		void handle_input(const SDL_Event*);
		int run(const std::string&, bool);
		std::vector<Variant> complete(Instance*, const std::string&);
		void resize();
		void draw();
	}

	void open();
	void close();
	void toggle();
	bool get_is_open();
	void clear();

	bool has_var(const std::string&);
	void set_var(const std::string&, const Variant&);
	Variant get_var(const std::string&);
	int add_command(const std::string&, std::function<Variant (const std::vector<Variant>&)>);
	int remove_command(const std::string&);

	int run(const std::string&);
	void log(E_MESSAGE, const std::string&);
}}

#endif // BEE_CORE_CONSOLE_H
