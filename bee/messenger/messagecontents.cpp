/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <vector>
#include <memory>

#include "messagecontents.hpp"

namespace bee {
	MessageContents::MessageContents() :
		tickstamp(0),
		tags(),
		type(E_MESSAGE::GENERAL),
		descr(),
		data()
	{}
	MessageContents::MessageContents(Uint32 tm, const std::vector<std::string>& tg, E_MESSAGE tp, const std::string& de, const Variant& da) :
		tickstamp(tm),
		tags(tg),
		type(tp),
		descr(de),
		data(da)
	{}
}
