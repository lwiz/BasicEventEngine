/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_DATA_VARIANT_H
#define BEE_DATA_VARIANT_H 1

#include <map>
#include <vector>
#include <typeinfo>
#include <typeindex>
#include <memory>

#include "../enum.hpp"

#include "serialdata.hpp"

namespace bee {
	/// Used to hold various types and to allow multiple types in the same container
	class Variant {
		E_DATA_TYPE type;
		std::type_index ptype;

	public:
		union {
			unsigned char c;
			long i;
			double f;
			std::string s;
			std::vector<Variant> v;
			std::map<Variant,Variant> m;
			SerialData sd;
			void* p;
			std::shared_ptr<void> sp;
		};

		Variant();
		Variant(E_DATA_TYPE);
		Variant(const Variant&);
		Variant(const std::string&, bool);
		Variant(const char*);
		Variant(std::nullptr_t);
		//Variant(std::initializer_list<Variant>);

		explicit Variant(unsigned char);
		explicit Variant(bool);
		explicit Variant(int);
		explicit Variant(long);
		explicit Variant(float);
		explicit Variant(double);
		explicit Variant(const std::string&);
		explicit Variant(const std::vector<Variant>&);
		explicit Variant(const std::map<Variant,Variant>&);
		explicit Variant(const SerialData&);
		template <typename T>
		explicit Variant(T*);
		explicit Variant(const std::shared_ptr<void>&, const std::type_index&);

		~Variant();
		int reset();

		E_DATA_TYPE get_type() const;
		std::type_index get_ptype() const;

		int interpret(const std::string&);
		std::string to_str(bool) const;
		std::string to_str() const;

		Variant& operator=(const Variant&);
		Variant& operator=(std::nullptr_t);
		Variant& operator=(unsigned char);
		Variant& operator=(bool);
		Variant& operator=(int);
		Variant& operator=(long);
		Variant& operator=(float);
		Variant& operator=(double);
		Variant& operator=(const std::string&);
		Variant& operator=(const char*);
		Variant& operator=(const std::vector<Variant>&);
		Variant& operator=(const std::map<Variant,Variant>&);
		Variant& operator=(const SerialData&);
		template <typename T>
		Variant& operator=(T*);
		template <typename T>
		Variant& operator=(const std::shared_ptr<T>&);

		bool operator==(const Variant&) const;
		bool operator!=(const Variant&) const;

		friend bool operator<(const Variant&, const Variant&);

		friend std::ostream& operator<<(std::ostream&, const Variant&);
		friend std::istream& operator>>(std::istream&, Variant&);
	};
	bool operator<(const Variant&, const Variant&);

	std::ostream& operator<<(std::ostream&, const Variant&);
	std::istream& operator>>(std::istream&, Variant&);

	template <typename T>
	Variant::Variant(T* _p) :
		type(E_DATA_TYPE::POINTER),
		ptype(std::type_index(typeid(decltype(_p)))),
		p(nullptr)
	{
		/*
		* Force the user to include the full type definition before
		* instantiating this construtor. This prevents typeid() from returning
		* different values for the same type in different compilation units.
		*
		* A side effect of this is that void* can't be used anymore.
		*/
		if (_p != nullptr) {
			i = sizeof(*_p);
		}
		p = _p;
	}

	template <typename T>
	Variant& Variant::operator=(T* _p) {
		*this = Variant(_p);
		return *this;
	}
	template <typename T>
	Variant& Variant::operator=(const std::shared_ptr<T>& _sp) {
		*this = Variant(std::static_pointer_cast<void>(_sp), std::type_index(typeid(T)));
		return *this;
	}
}

// Shorthand for easier use in Instances
#define _a(x) (*s)[x]
#define _c(x) (*s)[x].c
#define _i(x) (*s)[x].i
#define _f(x) (*s)[x].f
#define _s(x) (*s)[x].s
#define _v(x) (*s)[x].v
#define _m(x) (*s)[x].m
#define _sd(x) (*s)[x].sd
#define _p(x) (*s)[x].p
#define _sp(x) (*s)[x].sp

#endif // BEE_DATA_VARIANT_H
