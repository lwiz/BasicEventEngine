/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_DATA_STATEMACHINE_H
#define BEE_DATA_STATEMACHINE_H 1

#include <functional>
#include <deque>
#include <set>
#include <map>
#include <string>

#include <SDL2/SDL.h> // Include the required SDL headers for Uint32

namespace bee {
	/// Used to manage state callbacks in the StateMachine class
	struct State {
		std::string name; ///< The state name
		std::set<std::string> outputs; ///< Names of valid states that can be pushed after this state

		std::function<void ()> start_func; ///< The function called when pushing this state
		std::function<void (Uint32)> update_func; ///< The function called to update this state
		std::function<void ()> end_func; ///< The function called when popping this state

		// See bee/data/statemachine.cpp for function comments
		State(const std::string&, const std::set<std::string>&);

		void start();
		void update(Uint32);
		void end();
	};

	/// Used to manage and run callbacks for a Pushdown State Machine
	class StateMachine {
		std::deque<std::string> stack; ///< The stack of current states
		std::map<std::string,State> graph; ///< The graph of possible states

		// See bee/data/statemachine.cpp for function comments
		int init();
	public:
		StateMachine();

		void print_state();
		void print_graph();

		int add_state(const State&);
		int remove_state(const std::string&);
		void clear();

		std::string get_state(size_t) const;
		std::string get_state() const;
		std::string get_states(bool) const;
		std::string get_states() const;
		std::string get_graph() const;
		bool has_state(const std::string&) const;

		int push_state(const std::string&);
		int pop_state();
		int pop_state(const std::string&);
		size_t pop_state_all(const std::string&);
		void pop_all();

		void update();
		void update_all();
	};
}

#endif // BEE_DATA_STATEMACHINE_H
