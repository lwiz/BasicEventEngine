/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

// Archive handling functions

#include <iostream>
#include <sstream>

#include <lzma.h>
#include <archive.h>
#include <archive_entry.h>

#include "archive.hpp"

#include "files.hpp"
#include "string.hpp"

namespace util { namespace archive {
	namespace internal {
		const int lzma_compression_preset = 6;

		int tar_append_tree(struct archive*, const std::string&, const std::string&);
		int tar_copy_data(struct archive*, struct archive*);
		lzma_ret lzma_code(lzma_stream*, std::stringstream*, std::stringstream*);
	}
	/**
	* Add the given path to the given archive.
	* @param a the archive to add to
	* @param path the file to add
	* @param root the root directory of the archive
	*
	* @retval 0 success
	* @retval 1 failed since the file doesn't exist
	* @retval 2 failed to add file header
	* @retval 3 failed to add file
	*/
	int internal::tar_append_tree(struct archive* a, const std::string& path, const std::string& root) {
		if (directory_exists(path)) {
			std::vector<std::string> files = directory_get_files(path);
			for (auto& f : files) {
				int r = tar_append_tree(a, path+"/"+f, root);
				if (r) {
					return r;
				}
			}
		} else if (file_exists(path)) {
			struct stat st;
			stat(path.c_str(), &st);

			struct archive_entry* entry = archive_entry_new();
			archive_entry_set_pathname(entry, string::replace(path, root+"/", "").c_str());
			archive_entry_set_filetype(entry, AE_IFREG);
			archive_entry_copy_stat(entry, &st);
			if (archive_write_header(a, entry) != ARCHIVE_OK) {
				return 2;
			}

			std::string cont = file_get_contents(path);
			if (archive_write_data(a, cont.c_str(), cont.size()) < 0) {
				return 3;
			}

			archive_entry_free(entry);
		} else {
			return 1;
		}

		return 0;
	}
	/**
	* Copy blocks from one archive to another.
	* @param a the archive to copy from
	* @param b the archive to copy to
	*
	* @retval ARCHIVE_OK success
	* @retval ARCHIVE_* failed to copy data
	*/
	int internal::tar_copy_data(struct archive* a, struct archive* b) {
		const void* buf;
		size_t size;
		__LA_INT64_T offset;

		while (true) {
			int r = archive_read_data_block(a, &buf, &size, &offset);
			if (r == ARCHIVE_EOF) {
				break;
			} else if (r < ARCHIVE_OK) {
				return r;
			}

			__LA_INT64_T bytes_written = archive_write_data_block(b, buf, size, offset);
			if (bytes_written < 0) {
				return ARCHIVE_FATAL;
			}
		}

		return ARCHIVE_OK;
	}
	/**
	* Create a Tar archive from a directory.
	* @param directory the directory path to archive
	* @param archive_path the file path to store the archive at
	*
	* @retval 0 success
	* @retval 1 failed to open the new tar archive
	* @retval 2 failed to add the directory to the archive
	* @retval 3 failed to close the tar archive
	*/
	int tar_create(const std::string& directory, const std::string& archive_path) {
		struct archive* a = archive_write_new();
		archive_write_set_format_pax_restricted(a);
		if (archive_write_open_filename(a, archive_path.c_str()) != ARCHIVE_OK) {
			std::cerr << "UTIL ARCHIVE Failed to open tarfile \"" << archive_path << "\" for writing\n";
			return 1;
		}

		int ret = internal::tar_append_tree(a, directory, directory);
		if (ret != ARCHIVE_OK) {
			std::cerr << "UTIL ARCHIVE Failed to create tarfile \"" << archive_path << "\"\n";
		}

		if (archive_write_close(a) != ARCHIVE_OK) {
			std::cerr << "UTIL ARCHIVE Failed to close tarfile \"" << archive_path << "\" after writing\n";
			ret = 3;
		}
		archive_write_free(a);

		return (ret == ARCHIVE_OK) ? 0 : 2;
	}
	/**
	* Archive a directory as a Tarfile.
	* @note The archive path will be directory+".tar"
	* @param directory the directory path to archive
	*
	* @see tar_create(const std::string&, const std::string&) for return values
	*/
	int tar_create(const std::string& directory) {
		std::string archive (directory);
		if (archive.back() == '/') {
			archive = archive.substr(0, archive.length()-1);
		}
		archive += ".tar";

		return tar_create(directory, archive);
	}
	/**
	* Extract the Tarfile to a directory.
	* @param archive_path the path of the tarfile
	* @param directory the directory path to store the extracted files at
	*
	* @retval 0 success
	* @retval 1 failed to open tarfile
	* @retval 2 failed to extract files from the archive
	* @retval 3 failed to close tarfile
	*/
	int tar_extract(const std::string& archive_path, const std::string& directory) {
		struct archive* a = archive_read_new();
		archive_read_support_format_tar(a);
		struct archive* ext = archive_write_disk_new();
		archive_write_disk_set_options(ext,
			ARCHIVE_EXTRACT_FFLAGS
			| ARCHIVE_EXTRACT_PERM
			//| ARCHIVE_EXTRACT_SECURE_NOABSOLUTEPATHS
			| ARCHIVE_EXTRACT_SECURE_NODOTDOT
			| ARCHIVE_EXTRACT_TIME
		);
		archive_write_disk_set_standard_lookup(ext);

		if (archive_read_open_filename(a, archive_path.c_str(), 10240) != ARCHIVE_OK) {
			std::cerr << "UTIL ARCHIVE Failed to open tarfile \"" << archive_path << "\" for extraction\n";
			return 1;
		}

		int ret = 0;
		while (true) {
			struct archive_entry* entry;
			int r = archive_read_next_header(a, &entry);
			if (r == ARCHIVE_EOF) {
				break;
			}

			if (r < ARCHIVE_OK) {
				std::cerr << "UTIL ARCHIVE Failed to read header: " << archive_error_string(a) << "\n";
				if (r < ARCHIVE_WARN) {
					ret = 2;
					break;
				}
			}

			std::string path (archive_entry_pathname(entry));
			if (path.front() == '/') {
				std::cerr << "UTIL ARCHIVE Failed to write header: Path is absolute\n";
				ret = 2;
				break;
			}
			archive_entry_set_pathname(entry, (directory+"/"+path).c_str());

			r = archive_write_header(ext, entry);
			if (r < ARCHIVE_OK) {
				std::cerr << "UTIL ARCHIVE Failed to write header: " << archive_error_string(ext) << "\n";
				if (r < ARCHIVE_WARN) {
					ret = 2;
					break;
				}
			}

			if (archive_entry_size(entry) > 0) {
				r = internal::tar_copy_data(a, ext);
				if (r < ARCHIVE_OK) {
					std::cerr << "UTIL ARCHIVE Failed to copy data: " << archive_error_string(ext) << "\n";
					if (r < ARCHIVE_WARN) {
						ret = 2;
						break;
					}
				}
			}

			r = archive_write_finish_entry(ext);
			if (r < ARCHIVE_OK) {
				std::cerr << "UTIL ARCHIVE Failed to finish entry: " << archive_error_string(ext) << "\n";
				if (r < ARCHIVE_WARN) {
					ret = 2;
					break;
				}
			}
		}

		if (archive_read_close(a) != ARCHIVE_OK) {
			std::cerr << "UTIL ARCHIVE Failed to close tarfile: " << archive_error_string(a) << "\n";
			ret = 3;
		}
		archive_read_free(a);
		if (archive_write_close(ext) != ARCHIVE_OK) {
			std::cerr << "UTIL ARCHIVE Failed to close directory: " << archive_error_string(ext) << "\n";
			ret = 3;
		}
		archive_write_free(ext);

		return ret;
	}
	/**
	* Extract an archived directory.
	* @note The name of the directory will be the archive path without the ".tar" extension
	* @param archive_path the path of the tarfile
	*
	* @retval -1 invalid archive extension
	* @see tar_extract(const std::string&, const std::string&) for other return values
	*/
	int tar_extract(const std::string& archive_path) {
		if (file_extname(archive_path) == ".tar") {
			return tar_extract(archive_path, file_plainname(archive_path));
		}
		return -1;
	}
	/**
	* Extract an archived directory to the temp directory.
	* @note The extracted directory path will be "/tmp/bee-XXXXX/" + archive_name without the ".tar" extension
	* @param archive_path the path of the tarfile
	*
	* @returns the name of the extracted directory or an empty string on failure
	*/
	std::string tar_extract_temp(const std::string& archive_path) {
		const std::string tmp_dir (directory_get_temp() + file_plainname(file_basename(archive_path)));
		if (tar_extract(archive_path, tmp_dir) != 0) {
			return "";
		}
		return tmp_dir;
	}

	/**
	* Process the LZMA stream with the given input and output.
	* @note This function is used by both xz_compress() and xz_decompress()
	* @param stream the LZMA stream for compression or decompression, initialized elsewhere
	* @param input the input stream
	* @param output the output stream
	*
	* @returns the LZMA return value from ::lzma_code()
	*/
	lzma_ret internal::lzma_code(lzma_stream* stream, std::stringstream* input, std::stringstream* output) {
		lzma_action action = LZMA_RUN;
		uint8_t inbuf[BUFSIZ];
		uint8_t outbuf[BUFSIZ];

		stream->next_in = nullptr;
		stream->avail_in = 0;
		stream->next_out = outbuf;
		stream->avail_out = sizeof(outbuf);
		lzma_ret ret = LZMA_OK;
		while (ret == LZMA_OK) {
			if ((stream->avail_in == 0)&&(input->good())) {
				stream->next_in = inbuf;
				input->read(reinterpret_cast<char*>(inbuf), sizeof(inbuf));
				stream->avail_in = input->gcount();

				if (input->eof()) {
					action = LZMA_FINISH;
				}
			}

			ret = lzma_code(stream, action);

			if ((stream->avail_out == 0)||(ret == LZMA_STREAM_END)) {
				output->write(reinterpret_cast<char*>(outbuf), sizeof(outbuf) - stream->avail_out);
				if (!output->good()) {
					ret = LZMA_PROG_ERROR;
					break;
				}
				stream->next_out = outbuf;
				stream->avail_out = sizeof(outbuf);
			}
		}
		lzma_end(stream);

		return ret;
	}
	/**
	* Compress the given file using LZMA.
	* @param file_path the path of the uncompressed file
	* @param archive_path the path of the compressed archive
	*
	* @retval 0 success
	* @retval 1 failed to initialize encoder
	* @retval 2 failed to encode stream
	*/
	int xz_compress(const std::string& file_path, const std::string& archive_path) {
		lzma_stream stream = LZMA_STREAM_INIT;
		lzma_ret ret = lzma_easy_encoder(&stream, internal::lzma_compression_preset, LZMA_CHECK_CRC64);
		if (ret != LZMA_OK) {
			std::cerr << "UTIL ARCHIVE Failed to initialize LZMA stream encoder for \"" << file_path << "\":";
			switch (ret) {
				case LZMA_MEM_ERROR: {
					std::cerr << "Memory allocation failed";
					break;
				}
				case LZMA_OPTIONS_ERROR: {
					std::cerr << "Unsupported preset " << internal::lzma_compression_preset;
					break;
				}
				case LZMA_UNSUPPORTED_CHECK: {
					std::cerr << "Unsupported integrity check";
					break;
				}
				default: {
					std::cerr << "Unknown error";
				}
			}
			std::cerr << "\n";
			return 1;
		}

		std::stringstream input (file_get_contents(file_path));
		std::stringstream output;
		ret = internal::lzma_code(&stream, &input, &output);

		if (ret != LZMA_STREAM_END) {
			std::cerr << "UTIL ARCHIVE Failed to read LZMA stream for \"" << archive_path << "\":";
			switch (ret) {
				case LZMA_MEM_ERROR: {
					std::cerr << "Memory allocation failed";
					break;
				}
				case LZMA_DATA_ERROR: {
					std::cerr << "File size limits exceeded";
					break;
				};
				default: {
					std::cerr << "Unknown error";
				}
			}
			std::cerr << "\n";
			return 2;
		}

		file_put_contents(archive_path, output.str());

		return 0;
	}
	/**
	* Compress the given file using LZMA.
	* @note The compressed archive path will be file_path+".xz"
	* @param file_path the path of the uncompressed file
	*
	* @see xz_compress(const std::string&, const std::string&) for return values
	*/
	int xz_compress(const std::string& file_path) {
		return xz_compress(file_path, file_path+".xz");
	}
	/**
	* Decompress the given archive using LZMA.
	* @param archive_path the path of the compressed archive
	* @param file_path the path of the uncompressed file
	*
	* @retval 0 success
	* @retval 1 failed to initialize decoder
	* @retval 2 failed to decode stream
	*/
	int xz_decompress(const std::string& archive_path, const std::string& file_path) {
		lzma_stream stream = LZMA_STREAM_INIT;
		lzma_ret ret = lzma_stream_decoder(&stream, UINT64_MAX, LZMA_CONCATENATED);
		if (ret != LZMA_OK) {
			std::cerr << "UTIL ARCHIVE Failed to initialize LZMA stream decoder for \"" << archive_path << "\":";
			switch (ret) {
				case LZMA_MEM_ERROR: {
					std::cerr << "Memory allocation failed";
					break;
				}
				case LZMA_OPTIONS_ERROR: {
					std::cerr << "Unsupported decompression flags";
					break;
				}
				default: {
					std::cerr << "Unknown error";
				}
			}
			std::cerr << "\n";
			return 1;
		}

		std::stringstream input (file_get_contents(archive_path));
		std::stringstream output;
		ret = internal::lzma_code(&stream, &input, &output);

		if (ret != LZMA_STREAM_END) {
			std::cerr << "UTIL ARCHIVE Failed to read LZMA stream for \"" << archive_path << "\":";
			switch (ret) {
				case LZMA_MEM_ERROR: {
					std::cerr << "Memory allocation failed";
					break;
				}
				case LZMA_FORMAT_ERROR: {
					std::cerr << "Not in the .xz format";
					break;
				}
				case LZMA_OPTIONS_ERROR: {
					std::cerr << "Unsupported decompression flags";
					break;
				}
				case LZMA_DATA_ERROR: {
					std::cerr << "Corrupt file";
					break;
				};
				case LZMA_BUF_ERROR: {
					std::cerr << "Truncated file";
					break;
				};
				default: {
					std::cerr << "Unknown error";
				}
			}
			std::cerr << "\n";
			return 2;
		}

		file_put_contents(file_path, output.str());

		return 0;
	}
	/**
	* Decompress the given archive using LZMA.
	* @note The uncompressed file path will be the archive path without the ".xz" extension
	* @param archive_path the path of the compressed archive
	*
	* @retval -1 invalid archive extension
	* @see xz_decompress(const std::string&, const std::string&) for other return values
	*/
	int xz_decompress(const std::string& archive_path) {
		if (file_extname(archive_path) == ".xz") {
			return xz_decompress(archive_path, file_plainname(archive_path));
		}
		return -1;
	}
	/**
	* Decompress a compressed archive to the temporary directory.
	* @note The uncompressed file path will be "/tmp/bee-XXXXX/" + archive_name without the ".xz" extension
	* @param archive_path the path of the compressed archive
	*
	* @returns the uncompressed file path or an empty string on failure
	*/
	std::string xz_decompress_temp(const std::string& archive_path) {
		std::string tmp_path (directory_get_temp() + file_basename(archive_path));
		if (file_extname(tmp_path) == ".xz") {
			tmp_path = file_plainname(tmp_path);
		}

		if (xz_decompress(archive_path, tmp_path) != 0) {
			return "";
		}
		return tmp_path;
	}
}}
