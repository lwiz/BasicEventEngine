/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_UTIL_TEMPLATE_H
#define BEE_UTIL_TEMPLATE_H 1

#include "template/real.hpp"
#include "template/string.hpp"

#endif // BEE_UTIL_TEMPLATE_H
