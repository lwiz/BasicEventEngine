###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

def _assert_eq(lhs, rhs, msg=""):
    _msg = "{} == {}".format(lhs, rhs)
    if msg:
        _msg = "{}: {}".format(msg, _msg)
    assert lhs == rhs, _msg

def assert_func(func):
    _assert_eq(func(), 0)
    return None

def curry(func):
    f_args = []
    f_kwargs = {}
    def f(*args, **kwargs):
        nonlocal f_args, f_kwargs
        if args or kwargs:
            f_args += args
            f_kwargs.update(kwargs)
            return f
        else:
            return func(*f_args, *f_kwargs)
    return f
def curry_eager(func):
    f_args = []
    f_kwargs = {}
    def f(*args, **kwargs):
        nonlocal f_args, f_kwargs
        if args or kwargs:
            # Run the function if enough arguments have been provided
            if len(f_args) >= func.__code__.co_argcount and len(f_kwargs) >= func.__code__.co_kwonlyargcount:
                f_args[len(args):] = args
                f_kwargs.update(kwargs)
                return func(*f_args, *f_kwargs)

            f_args += args
            f_kwargs.update(kwargs)
            return f
        else:
            return func(*f_args, *f_kwargs)
    return f
def curry_once(func, *args, **kwargs):
    def f(*f_args, **f_kwargs):
        return func(*args, *f_args, *kwargs, *f_kwargs)
    return f

def check_collision(a: tuple, b: tuple):
    # Define the boundaries for the first rectangle
    al = a[0]
    ar = a[0]+a[2]
    at = a[1]
    ab = a[1]+a[3]

    # Define the boundaries for the second rectangle
    bl = b[0]
    br = b[0]+b[2]
    bt = b[1]
    bb = b[1]+b[3]

    # Compare the boundaries of the two rectangles
    if ab < bt:
        return False
    elif at > bb:
        return False
    elif ar < bl:
        return False
    elif al > br:
        return False

    return True
