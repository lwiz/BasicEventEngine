###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

# Used to add commands below since bee.console.add_command() won't work while we're still loading this Script
def add_command(func, name=None):
    if not name:
        name = func.__name__

    if name not in globals():
        globals()[name] = func

    return func

# Make the console and console utility commands available as global functions for this Script only
commands = {**bee.console.commands.__dict__, **bee._import("resources/scripts/util.py").__dict__}
for name, func in commands.items():
    if name[0] == "_":
        continue
    add_command(func, name)

# Replace the displayhook inside a wrapper function to avoid polluting the console
def dh():
    import sys
    def dh(value):
        bee._displayhook(value)

        __builtins__["_"] = None
        if value is None:
            return
        __builtins__["_"] = value
    sys.displayhook = dh
dh()
del dh
