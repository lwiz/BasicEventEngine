/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "../../util.hpp"
#include "../../all.hpp"

#include "obj_script.hpp"

#define EV(x) {util::string::lower(#x), bee::E_EVENT::x}

namespace bee {
	namespace internal {
		const std::unordered_map<std::string, bee::E_EVENT> event_strings {
			EV(UPDATE),
			EV(CREATE),
			EV(DESTROY),
			EV(ALARM),
			EV(STEP_BEGIN),
			EV(STEP_MID),
			EV(STEP_END),
			EV(KEYBOARD_PRESS),
			EV(MOUSE_PRESS),
			EV(KEYBOARD_INPUT),
			EV(MOUSE_INPUT),
			EV(KEYBOARD_RELEASE),
			EV(MOUSE_RELEASE),
			EV(CONTROLLER_AXIS),
			EV(CONTROLLER_PRESS),
			EV(CONTROLLER_RELEASE),
			EV(CONTROLLER_MODIFY),
			EV(COMMANDLINE_INPUT),
			EV(PATH_END),
			EV(OUTSIDE_ROOM),
			EV(INTERSECT_BOUNDARY),
			EV(COLLISION),
			EV(CHECK_COLLISION_FILTER),
			EV(DRAW),
			EV(ANIMATION_END),
			EV(ROOM_START),
			EV(ROOM_END),
			EV(GAME_START),
			EV(GAME_END),
			EV(WINDOW),
			EV(NETWORK),
			EV(IO)
		};
	}

	ObjScript::ObjScript(const std::string& _scriptfile) :
		Object("__obj_script", _scriptfile),

		scriptfile(_scriptfile),
		script(nullptr),
		events(),

		is_loaded(false)
	{
		set_name(util::file_plainname(util::file_basename(scriptfile)));
	}
	ObjScript::~ObjScript() {
		this->free();
	}

	/**
	* Load the Object from its scriptfile.
	*
	* @retval 0 success
	* @retval 1 failed to load since it's already loaded
	* @retval 2 failed to load the Script
	*/
	int ObjScript::load() {
		if (is_loaded) { // If the Object has already been loaded, output a warning
			messenger::send({"engine", "object", "obj_script"}, E_MESSAGE::WARNING, "Failed to load Object \"" + scriptfile + "\" because it has already been loaded");
			return 1;
		}

		std::string filename (scriptfile);
		if ((!filename.empty())&&(filename.front() == '$')) {
			filename = "resources/objects"+filename.substr(1);
		}

		script = Script::add("__scr_obj_script:" + scriptfile, filename);
		if (script == nullptr) {
			return 2;
		}
		if (!script->get_is_loaded()) {
			delete script;
			script = nullptr;
			return 2;
		}

		ScriptInterface* si = script->get_interface();
		for (auto& event : internal::event_strings) {
			if (si->has_var(event.first)) {
				events.insert(event.second);
				implemented_events.insert(event.second);
			}
		}

		is_loaded = true;

		return 0;
	}
	/**
	* Free the Object and its internal Script.
	*
	* @retval 0 success
	*/
	int ObjScript::free() {
		if (!is_loaded) {
			return 0;
		}

		events.clear();
		implemented_events.clear();

		if (script != nullptr) {
			delete script;
			script = nullptr;
		}

		is_loaded = false;

		return 0;
	}

	bool ObjScript::get_is_loaded() const {
		return is_loaded;
	}

	void destroy(Instance* self) {
		std::string nameid (std::to_string(self->get_id()));
		if (!self->get_name().empty()) {
			nameid = self->get_name() + ":" + nameid;
		}
		messenger::send({"engine", "object", "obj_script"}, E_MESSAGE::WARNING, "Create event failed for Instance " + nameid + " of Object " + self->get_main_object()->get_name() + ", the Instance will be destroyed");
		get_current_room()->destroy(self);
	}

	void ObjScript::update(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Object::update(self);
		if (events.find(bee::E_EVENT::UPDATE) == events.end()) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		script->run_func("update", args, nullptr);
	}
	void ObjScript::create(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("create", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::destroy(Instance* self) {
		if (!is_loaded) {
			return;
		}

		if (events.find(bee::E_EVENT::DESTROY) != events.end()) {
			Variant args (std::vector<Variant>{
				Variant(self)
			});
			script->run_func("destroy", args, nullptr);
		}

		Object::destroy(self);
	}

	void ObjScript::alarm(Instance* self, const std::string& alarm) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(alarm)
		});
		if (script->run_func("alarm", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::step_begin(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Object::step_begin(self);
		if (events.find(bee::E_EVENT::STEP_BEGIN) == events.end()) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("step_begin", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::step_mid(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("step_mid", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::step_end(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("step_end", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::keyboard_press(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->key.type))},
				{"timestamp", Variant(static_cast<int>(e->key.timestamp))},
				{"windowID", Variant(static_cast<int>(e->key.windowID))},
				{"state", Variant(static_cast<int>(e->key.state))},
				{"repeat", Variant(static_cast<int>(e->key.repeat))},
				{"keysym", Variant(std::map<Variant,Variant>{
					{"scancode", Variant(static_cast<int>(e->key.keysym.scancode))},
					{"sym", Variant(static_cast<int>(e->key.keysym.sym))},
					{"mod", Variant(static_cast<int>(e->key.keysym.mod))}
				})},
				{"keyname", Variant(bee::kb::keystrings_get_string(e->key.keysym.sym))}
			})
		});
		if (script->run_func("keyboard_press", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::mouse_press(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->button.type))},
				{"timestamp", Variant(static_cast<int>(e->button.timestamp))},
				{"windowID", Variant(static_cast<int>(e->button.windowID))},
				{"which", Variant(static_cast<int>(e->button.which))},
				{"button", Variant(static_cast<int>(e->button.button))},
				{"state", Variant(static_cast<int>(e->button.state))},
				{"clicks", Variant(static_cast<int>(e->button.clicks))},
				{"x", Variant(static_cast<int>(e->button.x))},
				{"y", Variant(static_cast<int>(e->button.y))}
			})
		});
		if (script->run_func("mouse_press", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::keyboard_input(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->key.type))},
				{"timestamp", Variant(static_cast<int>(e->key.timestamp))},
				{"windowID", Variant(static_cast<int>(e->key.windowID))},
				{"state", Variant(static_cast<int>(e->key.state))},
				{"repeat", Variant(static_cast<int>(e->key.repeat))},
				{"keysym", Variant(std::map<Variant,Variant>{
					{"scancode", Variant(static_cast<int>(e->key.keysym.scancode))},
					{"sym", Variant(static_cast<int>(e->key.keysym.sym))},
					{"mod", Variant(static_cast<int>(e->key.keysym.mod))}
				})},
				{"keyname", Variant(bee::kb::keystrings_get_string(e->key.keysym.sym))}
			})
		});
		if (script->run_func("keyboard_input", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::mouse_input(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		static Variant event {std::map<Variant,Variant>{
			{"type", Variant(0)},
			//{"timestamp", Variant(0)},
			{"windowID", Variant(0)},
			//{"which", Variant(0)},
			//{"state", Variant(0)},

			//{"x", Variant(0)},
			//{"y", Variant(0)},
			{"xrel", Variant(0)},
			{"yrel", Variant(0)},

			//{"direction", Variant(0)},
		}};
		if (e->type == SDL_MOUSEMOTION) {
			event.m["type"].i = e->motion.type;
			event.m["windowID"].i = e->motion.windowID;
			event.m["xrel"].i = e->motion.xrel;
			event.m["yrel"].i = e->motion.yrel;
		} else if (e->type == SDL_MOUSEWHEEL) {
			event.m["type"].i = e->wheel.type;
			event.m["windowID"].i = e->wheel.windowID;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			event
		});
		if (script->run_func("mouse_input", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::keyboard_release(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->key.type))},
				{"timestamp", Variant(static_cast<int>(e->key.timestamp))},
				{"windowID", Variant(static_cast<int>(e->key.windowID))},
				{"state", Variant(static_cast<int>(e->key.state))},
				{"repeat", Variant(static_cast<int>(e->key.repeat))},
				{"keysym", Variant(std::map<Variant,Variant>{
					{"scancode", Variant(static_cast<int>(e->key.keysym.scancode))},
					{"sym", Variant(static_cast<int>(e->key.keysym.sym))},
					{"mod", Variant(static_cast<int>(e->key.keysym.mod))}
				})},
				{"keyname", Variant(bee::kb::keystrings_get_string(e->key.keysym.sym))}
			})
		});
		if (script->run_func("keyboard_release", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::mouse_release(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->button.type))},
				{"timestamp", Variant(static_cast<int>(e->button.timestamp))},
				{"windowID", Variant(static_cast<int>(e->button.windowID))},
				{"which", Variant(static_cast<int>(e->button.which))},
				{"button", Variant(static_cast<int>(e->button.button))},
				{"state", Variant(static_cast<int>(e->button.state))},
				{"clicks", Variant(static_cast<int>(e->button.clicks))},
				{"x", Variant(static_cast<int>(e->button.x))},
				{"y", Variant(static_cast<int>(e->button.y))}
			})
		});
		if (script->run_func("mouse_release", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::controller_axis(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->caxis.type))},
				{"timestamp", Variant(static_cast<int>(e->caxis.timestamp))},
				{"which", Variant(static_cast<int>(e->caxis.which))},
				{"axis", Variant(static_cast<int>(e->caxis.axis))},
				{"value", Variant(static_cast<int>(e->caxis.value))}
			})
		});
		if (script->run_func("controller_axis", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::controller_press(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->cbutton.type))},
				{"timestamp", Variant(static_cast<int>(e->cbutton.timestamp))},
				{"which", Variant(static_cast<int>(e->cbutton.which))},
				{"button", Variant(static_cast<int>(e->cbutton.button))},
				{"state", Variant(static_cast<int>(e->cbutton.state))}
			})
		});
		if (script->run_func("controller_press", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::controller_release(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->cbutton.type))},
				{"timestamp", Variant(static_cast<int>(e->cbutton.timestamp))},
				{"which", Variant(static_cast<int>(e->cbutton.which))},
				{"button", Variant(static_cast<int>(e->cbutton.button))},
				{"state", Variant(static_cast<int>(e->cbutton.state))}
			})
		});
		if (script->run_func("controller_release", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::controller_modify(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->cdevice.type))},
				{"timestamp", Variant(static_cast<int>(e->cdevice.timestamp))},
				{"which", Variant(static_cast<int>(e->cdevice.which))}
			})
		});
		if (script->run_func("controller_modify", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::commandline_input(Instance* self, const std::string& str) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(str)
		});
		if (script->run_func("commandline_input", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::path_end(Instance* self, const PathFollower& pf) {
		if (!is_loaded) {
			return;
		}

		PathFollower* _pf = const_cast<PathFollower*>(&pf);

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(_pf)
		});
		if (script->run_func("path_end", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::outside_room(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("outside_room", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::intersect_boundary(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("intersect_boundary", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::collision(Instance* self, Instance* other) {
		if (!is_loaded) {
			return;
		}

		if (events.find(bee::E_EVENT::COLLISION) == events.end()) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(other)
		});
		if (script->run_func("collision", args, nullptr) != 0) {
			destroy(self);
		}
	}
	bool ObjScript::check_collision_filter(const Instance* self, const Instance* other) const {
		if (!is_loaded) {
			return Object::check_collision_filter(self, other);
		}

		if (events.find(bee::E_EVENT::CHECK_COLLISION_FILTER) == events.end()) {
			return Object::check_collision_filter(self, other);
		}

		Variant result;

		Instance* _self = const_cast<Instance*>(self);
		Instance* _other = const_cast<Instance*>(other);

		Variant args (std::vector<Variant>{
			Variant(_self),
			Variant(_other)
		});
		script->run_func("check_collision_filter", args, &result);

		return result.i;
	}

	void ObjScript::draw(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("draw", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::animation_end(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("animation_end", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::room_start(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("room_start", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::room_end(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("room_end", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::game_start(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("game_start", args, nullptr) != 0) {
			destroy(self);
		}
	}
	void ObjScript::game_end(Instance* self) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self)
		});
		if (script->run_func("game_end", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::window(Instance* self, SDL_Event* e) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e->window.type))},
				{"timestamp", Variant(static_cast<int>(e->window.timestamp))},
				{"windowID", Variant(static_cast<int>(e->window.windowID))},
				{"event", Variant(static_cast<int>(e->window.event))},
				{"data1", Variant(static_cast<int>(e->window.data1))},
				{"data2", Variant(static_cast<int>(e->window.data2))}
			})
		});
		if (script->run_func("window", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::network(Instance* self, const NetworkEvent& e) {
		if (!is_loaded) {
			return;
		}

		std::map<Variant,Variant> data;
		for (auto& d : e.data) {
			data.emplace(Variant(d.first), d.second);
		}

		std::map<Variant,Variant> insts;
		for (auto& inst : e.instances) {
			std::vector<Variant> _data;
			_data.reserve(inst.second.size());
			for (auto& d : inst.second) {
				_data.push_back(Variant(static_cast<int>(d)));
			}

			insts.emplace(Variant(inst.first), _data);
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			Variant(std::map<Variant,Variant>{
				{"type", Variant(static_cast<int>(e.type))},
				{"id", Variant(e.id)},
				{"data", Variant(data)},
				{"instances", Variant(insts)},
			})
		});
		if (script->run_func("network", args, nullptr) != 0) {
			destroy(self);
		}
	}

	void ObjScript::io(Instance* self, const Variant& data) {
		if (!is_loaded) {
			return;
		}

		Variant args (std::vector<Variant>{
			Variant(self),
			data
		});
		if (script->run_func("io", args, nullptr) != 0) {
			destroy(self);
		}
	}
}

#undef EV
