###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

util = bee._import("resources/scripts/util.py")

def create(self):
    if not self.has_data("color"):
        self.set_data("color", (255, 255, 255, 255))

    if not self.has_data("w") and not self.has_data("h"):
        spr = self.get_sprite()
        if spr:
            self.set_data("w", spr.get_size()[0])
            self.set_data("h", spr.get_size()[1])
        else:
            self.set_data("w", 0)
            self.set_data("h", 0)

    if not self.has_data("is_visible"):
        self.set_data("is_visible", True)

    self.set_data("is_pressed", False)
    self.set_data("has_focus", False)
    self.set_data("has_hover", False)
    self.set_data("is_relative", True)

def mouse_press(self, e):
    self.set_data("is_pressed", False)
    self.set_data("has_focus", False)

    if e["button"] == bee.E_SDL_BUTTON["LEFT"]:
        a = list(self.get_aabb())
        a[2] = self.get_data("w")
        a[3] = self.get_data("h")
        a = tuple(a)

        mp = (e["x"], e["y"])
        if self.get_data("is_relative") == True:
            mp = bee.mouse.get_relative_pos()
        b = (mp[0]-1, mp[1]-1, 2, 2)

        if util.check_collision(a, b):
            self.set_data("is_pressed", True)
def mouse_input(self, e):
    return
    if e["type"] != bee.E_SDL_EVENT_TYPE["MOUSEMOTION"]:
        return

    self.set_data("has_hover", False)

    a = list(self.get_aabb())
    a[2] = self.get_data("w")
    a[3] = self.get_data("h")
    a = tuple(a)

    mp = (e["x"], e["y"])
    if self.get_data("is_relative") == True:
        mp = bee.mouse.get_relative_pos()
    b = (mp[0]-1, mp[1]-1, 2, 2)

    if util.check_collision(a, b):
        self.set_data("has_hover", True)
def mouse_release(self, e):
    has_focus = False
    if e["button"] == bee.E_SDL_BUTTON["LEFT"]:
        a = list(self.get_aabb())
        a[2] = self.get_data("w")
        a[3] = self.get_data("h")
        a = tuple(a)

        mp = (e["x"], e["y"])
        if self.get_data("is_relative") == True:
            mp = bee.mouse.get_relative_pos()
        b = (mp[0]-1, mp[1]-1, 2, 2)

        if util.check_collision(a, b):
            if self.get_data("is_pressed") == True:
                has_focus = True

    self.set_data("has_focus", has_focus)
    self.set_data("is_pressed", False)

def io(self, data: list):
	if data[0] in globals():
		globals()[data[0]](self, *data[1:])
