###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

obj_element = bee._import("resources/objects/ui/obj_element.py")

def create(self):
	obj_element.create(self)

	if not self.has_data("range"):
		self.set_data("range", 100)
	if not self.has_data("value"):
		self.set_data("value", 0)

	if not self.has_data("slider_size"):
		self.set_data("slider_size", 16)

	if not self.has_data("show_text"):
		self.set_data("show_text", False)

	if not self.has_data("is_continuous"):
		self.set_data("is_continuous", False)

	if not self.has_data("color_track"):
		self.set_data("color_track", (0, 0, 0, 255))

def mouse_input(self, e):
	obj_element.mouse_input(self, e)

	if self.get_data("is_pressed") != True:
		if self.get_data("has_hover") == True and e["type"] == bee.E_SDL_EVENT_TYPE["MOUSEWHEEL"]:
			flip = bee.mouse.get_wheel_flip(e["wheel"])

			v = self.get_data("value")

			v += flip * e["y"]
			self.set_data("value", max(0, min(v, self.get_data("range"))))

			bee.ui.slider_callback(self, self.get_data("value"))

		return

	if e["type"] == bee.E_SDL_EVENT_TYPE["MOUSEMOTION"] and e["state"] & bee.E_SDL_BUTTON_MASK["LEFT"]:
		cx = self.get_corner()[0]
		if e["x"] >= cx - self.get_data("slider_size")/2 \
		  and e["x"] <= cx + self.get_data("w") + self.get_data("slider_size")/2:
			v = (e["x"] - cx) * self.get_data("range") / self.get_data("w")
			self.set_data("value", max(0, min(v, self.get_data("range"))))

			if self.get_data("is_continuous") == True:
				bee.ui.slider_callback(self, self.get_data("value"))
def mouse_release(self, e):
	obj_element.mouse_release(self, e)

	if self.get_data("has_focus") == True and self.get_data("is_continuous") != True:
		bee.ui.slider_callback(self, self.get_data("value"))

def draw(self):
	if self.get_data("is_visible") != True:
		return

	bee.render.set_is_lightable(False)

	w = self.get_data("w")
	h = self.get_data("h")

	c_track = self.get_data("color_track")
	c_slider = self.get_data("color")

	cx, cy = self.get_corner()

	bee.render.draw_quad((cx, cy + h/2, 0), (w, 2, 0), -1, c_track)

	slider_size = self.get_data("slider_size")
	slider_x = cx + w * self.get_data("value") / self.get_data("range") - slider_size/2
	bee.render.draw_circle((slider_x + slider_size/2, cy + h/2, 0), slider_size/2, -1, c_slider)

	if self.get_data("show_text") == True:
		font = bee.Font("font_default")
		font.draw_fast(cx, cy + h*2/3, "0")
		font.draw_fast(cx + w-16, cy + h*2/3, str(self.get_data("range")))
		font.draw_fast(slider_x + slider_size/2 - 8, cy - h/3, str(self.get_data("value")))

	bee.render.set_is_lightable(True)

for a in dir(obj_element):
	if not a.startswith("_") and a not in globals():
		globals()[a] = getattr(obj_element, a)
