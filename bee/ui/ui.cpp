/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <set>
#include <vector>

#include "ui.hpp"

#include "../messenger/messenger.hpp"

#include "../core/instance.hpp"
#include "../core/rooms.hpp"

#include "../resource/sound.hpp"
#include "../resource/font.hpp"
#include "../resource/object.hpp"
#include "../resource/room.hpp"

#include "../resources/headers.hpp"

namespace bee { namespace ui {
	namespace internal {
		bool is_loaded = false;

		// Declare sounds
		Sound* snd_button_press = nullptr;
		Sound* snd_button_release = nullptr;

		// Declare objects
		Object* obj_button = nullptr;
		Object* obj_handle = nullptr;
		Object* obj_text_entry = nullptr;
		Object* obj_gauge = nullptr;
		Object* obj_slider = nullptr;
		Object* obj_optionbox = nullptr;

		std::map<Instance*,std::set<Instance*>> parents;
		std::map<Instance*,std::function<void (Instance*)>> button_callbacks;
		std::map<Instance*,std::function<void (Instance*, const std::string&)>> text_entry_callbacks;
		std::map<Instance*,std::function<std::vector<Variant> (Instance*, const std::string&)>> text_entry_completors;
		std::map<Instance*,std::function<void (Instance*, const std::string&, const SDL_Event*)>> text_entry_handlers;
		std::map<Instance*,std::function<void (Instance*, int)>> slider_callbacks;
		std::map<Instance*,std::vector<std::function<void (Instance*, bool)>>> optionbox_callbacks;
	}

	int load() {
		if (internal::is_loaded) {
			return 1;
		}

		// Load sounds
		internal::snd_button_press = Sound::add("__snd_ui_button_press", "$/ui/button_press.wav", false);
		internal::snd_button_release = Sound::add("__snd_ui_button_release", "$/ui/button_release.wav", false);

		// Load objects
		internal::obj_button = Object::add("__obj_ui_button", "$/ui/obj_button.py");
		internal::obj_handle = Object::add("__obj_ui_handle", "$/ui/obj_handle.py");
		internal::obj_text_entry = Object::add("__obj_ui_text_entry", "$/ui/obj_text_entry.py");
		internal::obj_gauge = Object::add("__obj_ui_gauge", "$/ui/obj_gauge.py");
		internal::obj_slider = Object::add("__obj_ui_slider", "$/ui/obj_slider.py");
		internal::obj_optionbox = Object::add("__obj_ui_optionbox", "$/ui/obj_optionbox.py");

		internal::is_loaded = true;

		if ((internal::snd_button_press == nullptr)||(internal::snd_button_release == nullptr)) { // Output sound error after loading the UI elements
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "Failed to load UI sounds");
			return 2;
		}

		return 0;
	}
	#define DEL(x) if (x!=nullptr) {delete x; x=nullptr;}
	int free() {
		if (!internal::is_loaded) {
			return 1;
		}

		// Free sounds
		DEL(internal::snd_button_press);
		DEL(internal::snd_button_release);

		// Free objects
		DEL(internal::obj_button);
		DEL(internal::obj_handle);
		DEL(internal::obj_text_entry);
		DEL(internal::obj_gauge);
		DEL(internal::obj_slider);
		DEL(internal::obj_optionbox);

		internal::is_loaded = false;

		return 0;
	}
	#undef DEL

	int destroy_parent(bee::Instance* parent) {
		for (auto& inst : internal::parents[parent]) {
			inst->ev_update();
			inst->ev_io(Variant(std::vector<Variant>{"bind", nullptr}));

			get_current_room()->destroy(inst);
		}
		internal::parents.erase(parent);
		return 0;
	}

	Instance* create_button(int x, int y, Font* font, const std::string& str, std::function<void (bee::Instance*)> func) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: button not created");
			return nullptr;
		}

		const btVector3 pos (static_cast<btScalar>(x), static_cast<btScalar>(y), 0);
		bee::Instance* button = bee::get_current_room()->add_instance({internal::obj_button}, pos, {
			{"font", Variant(font)},
			{"text", Variant(str)}
		});
		button->set_corner(x, y);

		button->ev_update();
		button->ev_io(Variant(std::vector<Variant>{"center_width"}));

		internal::button_callbacks.emplace(button, func);

		return button;
	}
	int button_callback(Instance* button) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: button callback not run");
			return 1;
		}

		std::map<Instance*,std::function<void (Instance*)>>::iterator b (internal::button_callbacks.find(button));
		if (b == internal::button_callbacks.end()) {
			return 2;
		}

		std::function<void (Instance*)> func = b->second;
		if (func == nullptr) {
			return 3;
		}

		func(button);

		return 0;
	}

	Instance* create_handle(int x, int y, int w, int h, Instance* parent) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: handle not created");
			return nullptr;
		}

		const btVector3 pos (static_cast<btScalar>(x), static_cast<btScalar>(y), 0);
		bee::Instance* handle = bee::get_current_room()->add_instance({internal::obj_handle}, pos, {
			{"w", Variant(w)},
			{"h", Variant(h)}
		});
		handle->set_corner(x, y);

		handle->ev_update();
		handle->ev_io(Variant(std::vector<Variant>{Variant("bind"), Variant(parent)}));

		internal::parents[parent].insert(handle);

		return handle;
	}
	int destroy_handle(Instance* handle) {
		Instance* parent = static_cast<Instance*>(handle->get_data("parent").p);
		if (parent != nullptr) {
			if (internal::parents.find(parent) != internal::parents.end()) {
				internal::parents.at(parent).erase(handle);
			}
		}
		return 0;
	}

	Instance* create_text_entry(int x, int y, int rows, int cols, std::function<void (Instance*, const std::string&)> func) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: text entry not created");
			return nullptr;
		}

		const btVector3 pos (static_cast<btScalar>(x), static_cast<btScalar>(y), 0);
		bee::Instance* text_entry = bee::get_current_room()->add_instance({internal::obj_text_entry}, pos, {});
		text_entry->set_corner(x, y);

		text_entry->ev_update();
		text_entry->ev_io(Variant(std::vector<Variant>{Variant("set_size"), Variant(rows), Variant(cols)}));

		internal::text_entry_callbacks.emplace(text_entry, func);

		return text_entry;
	}
	int add_text_entry_completor(Instance* text_entry, std::function<std::vector<Variant> (Instance*, const std::string&)> func) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: text entry completor not added");
			return 1;
		}

		internal::text_entry_completors.emplace(text_entry, func);

		return 0;
	}
	int add_text_entry_handler(Instance* text_entry, std::function<void (Instance*, const std::string&, const SDL_Event*)> func) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: text entry handler not added");
			return 1;
		}

		internal::text_entry_handlers.emplace(text_entry, func);

		return 0;
	}
	int text_entry_callback(Instance* text_entry, const std::string& input) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: text entry callback not run");
			return 1;
		}

		std::map<Instance*,std::function<void (Instance*, const std::string&)>>::iterator te (internal::text_entry_callbacks.find(text_entry));
		if (te == internal::text_entry_callbacks.end()) {
			return 2;
		}

		std::function<void (Instance*, const std::string&)> func = te->second;
		if (func == nullptr) {
			return 3;
		}

		func(text_entry, input);

		return 0;
	}
	std::vector<Variant> text_entry_completor(Instance* text_entry, const std::string& input) {
		std::vector<Variant> uncomplete = {Variant(input)};

		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: text entry completor not run");
			return uncomplete;
		}

		std::map<Instance*,std::function<std::vector<Variant> (Instance*, const std::string&)>>::iterator tec (internal::text_entry_completors.find(text_entry));
		if (tec == internal::text_entry_completors.end()) {
			return uncomplete;
		}

		std::function<std::vector<Variant> (Instance*, const std::string&)> func = tec->second;
		if (func == nullptr) {
			return uncomplete;
		}

		return func(text_entry, input);
	}
	int text_entry_handler(Instance* text_entry, const std::string& input, const SDL_Event* e) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: text entry handler not run");
			return 1;
		}

		std::map<Instance*,std::function<void (Instance*, const std::string&, const SDL_Event*)>>::iterator teh (internal::text_entry_handlers.find(text_entry));
		if (teh == internal::text_entry_handlers.end()) {
			return 2;
		}

		std::function<void (Instance*, const std::string&, const SDL_Event*)> func = teh->second;
		if (func == nullptr) {
			return 3;
		}

		func(text_entry, input, e);

		return 0;
	}

	Instance* create_gauge(int x, int y, int w, int h, int range) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: gauge not created");
			return nullptr;
		}

		const btVector3 pos (static_cast<btScalar>(x), static_cast<btScalar>(y), 0);
		bee::Instance* gauge = bee::get_current_room()->add_instance({internal::obj_gauge}, pos, {
			{"w", Variant(w)},
			{"h", Variant(h)}
		});
		gauge->set_corner(x, y);

		if (range > 0) {
			gauge->set_data("range", range);
		} else {
			gauge->ev_update();
			gauge->ev_io(Variant(std::vector<Variant>{"start_pulse"}));
		}

		return gauge;
	}

	Instance* create_slider(int x, int y, int w, int h, int range, int value, bool is_continuous_callback, std::function<void (Instance*, int)> func) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: slider not created");
			return nullptr;
		}

		const btVector3 pos (static_cast<btScalar>(x), static_cast<btScalar>(y), 0);
		bee::Instance* slider = bee::get_current_room()->add_instance({internal::obj_slider}, pos, {
			{"w", Variant(w)},
			{"h", Variant(h)}
		});
		slider->set_corner(x, y);

		slider->set_data("range", range);
		slider->set_data("value", value);
		slider->set_data("is_continuous", is_continuous_callback);

		internal::slider_callbacks.emplace(slider, func);

		return slider;
	}
	int slider_callback(Instance* slider, int value) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: slider callback not run");
			return 1;
		}

		std::map<Instance*,std::function<void (Instance*, int)>>::iterator sl (internal::slider_callbacks.find(slider));
		if (sl == internal::slider_callbacks.end()) {
			return 2;
		}

		std::function<void (Instance*, int)> func = sl->second;
		if (func == nullptr) {
			return 3;
		}

		func(slider, value);

		return 0;
	}

	Instance* create_optionbox(int x, int y, int w, int h) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: optionbox not created");
			return nullptr;
		}

		const btVector3 pos (static_cast<btScalar>(x), static_cast<btScalar>(y), 0);
		bee::Instance* optionbox = bee::get_current_room()->add_instance({internal::obj_optionbox}, pos, {
			{"w", Variant(w)},
			{"h", Variant(h)},
			{"adaptive_height", Variant(h < 0)}
		});
		optionbox->set_corner(x, y);

		return optionbox;
	}
	int push_optionbox_option(Instance* optionbox, std::function<void (Instance*, bool)> callback) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: option not added");
			return 1;
		}

		internal::optionbox_callbacks[optionbox].push_back(callback);

		return 0;
	}
	int pop_optionbox_option(Instance* optionbox) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: option not removed");
			return 1;
		}

		if (internal::optionbox_callbacks[optionbox].empty()) {
			return 1;
		}

		internal::optionbox_callbacks[optionbox].pop_back();

		return 0;
	}
	int reset_optionbox_options(Instance* optionbox) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: optionbox not reset");
			return 1;
		}

		std::map<Instance*,std::vector<std::function<void (Instance*, bool)>>>::iterator opb (internal::optionbox_callbacks.find(optionbox));
		if (opb == internal::optionbox_callbacks.end()) {
			return 2;
		}

		internal::optionbox_callbacks.erase(opb);

		return 0;
	}
	int optionbox_callback(Instance* optionbox, size_t option_index, bool state) {
		if (!internal::is_loaded) {
			messenger::send({"engine", "ui"}, E_MESSAGE::WARNING, "UI not initialized: optionbox callback not run");
			return 1;
		}

		std::map<Instance*,std::vector<std::function<void (Instance*, bool)>>>::iterator opb (internal::optionbox_callbacks.find(optionbox));
		if (
			(opb == internal::optionbox_callbacks.end())
			||(option_index >= opb->second.size())
		) {
			return 2;
		}

		std::function<void (Instance*, bool)> func = opb->second.at(option_index);
		if (func == nullptr) {
			return 3;
		}

		func(optionbox, state);

		return 0;
	}
}}
