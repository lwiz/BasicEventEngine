/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <Python.h>
#include <structmember.h>

#include "statemachine.hpp"

#include "../python.hpp"

namespace bee { namespace python {
	PyObject* StateMachine_from(std::shared_ptr<StateMachine> sm) {
		PyObject* py_sm = internal::StateMachine_new(&internal::StateMachineType, nullptr, nullptr);
		internal::StateMachineObject* _py_sm = reinterpret_cast<internal::StateMachineObject*>(py_sm);

		_py_sm->sm = sm;

		return py_sm;
	}
	bool StateMachine_check(PyObject* obj) {
		return PyObject_TypeCheck(obj, &internal::StateMachineType);
	}
namespace internal {
	PyMethodDef StateMachineMethods[] = {
		{"print_state", reinterpret_cast<PyCFunction>(StateMachine_print_state), METH_NOARGS, "Print the currently active States"},
		{"print_graph", reinterpret_cast<PyCFunction>(StateMachine_print_graph), METH_NOARGS, "Print the graph of possible States"},

		// FIXME: Do some terrible casting to get rid of an incompatible function type warning
		{"add_state", reinterpret_cast<PyCFunction>(reinterpret_cast<void (*)(void)>(StateMachine_add_state)), METH_VARARGS | METH_KEYWORDS, "Add the given State to the graph of possible States"},
		{"remove_state", reinterpret_cast<PyCFunction>(StateMachine_remove_state), METH_VARARGS, "Remove the State with the given name from the graph of possible States"},
		{"clear", reinterpret_cast<PyCFunction>(StateMachine_clear), METH_NOARGS, "Remove all States and reinitialize the StateMachine"},

		{"get_state", reinterpret_cast<PyCFunction>(StateMachine_get_state), METH_VARARGS, "Return the name of the State with the given index in the stack of States or an empty string if the given index is too large"},
		{"get_states", reinterpret_cast<PyCFunction>(StateMachine_get_states), METH_VARARGS, "Return a string of space-separated State names on the stack"},
		{"get_graph", reinterpret_cast<PyCFunction>(StateMachine_get_graph), METH_NOARGS, "Return a string of possible States and their associated outputs"},
		{"has_state", reinterpret_cast<PyCFunction>(StateMachine_has_state), METH_VARARGS, "Return whether the State with the given name is on the stack"},

		{"push_state", reinterpret_cast<PyCFunction>(StateMachine_push_state), METH_VARARGS, "Push the State with given name onto the stack"},
		{"pop_state", reinterpret_cast<PyCFunction>(StateMachine_pop_state), METH_VARARGS, "Pop the State with the given name from the stack"},
		{"pop_state_all", reinterpret_cast<PyCFunction>(StateMachine_pop_state_all), METH_VARARGS, "Pop all instances of the State with the given name from the stack"},
		{"pop_all", reinterpret_cast<PyCFunction>(StateMachine_pop_all), METH_NOARGS, "Pop all States from the stack"},

		{"update", reinterpret_cast<PyCFunction>(StateMachine_update), METH_NOARGS, "Call the current State's update callback"},
		{"update_all", reinterpret_cast<PyCFunction>(StateMachine_update_all), METH_NOARGS, "Call the update callback for all States on the stack"},

		{nullptr, nullptr, 0, nullptr}
	};

	PyTypeObject StateMachineType = {
		PyVarObject_HEAD_INIT(NULL, 0)
		"bee.StateMachine",
		sizeof(StateMachineObject), 0,
		StateMachine_dealloc,
		0,
		0, 0,
		0,
		0,
		0, 0, 0,
		0,
		0,
		reinterpret_cast<reprfunc>(StateMachine_str),
		0, 0,
		0,
		Py_TPFLAGS_DEFAULT,
		"StateMachine objects",
		0,
		0,
		0,
		0,
		0, 0,
		StateMachineMethods,
		0,
		0,
		0,
		0,
		0, 0,
		0,
		reinterpret_cast<initproc>(StateMachine_init),
		0, StateMachine_new,
		0, 0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	};

	PyObject* PyInit_bee_data_statemachine(PyObject* module) {
		StateMachineType.tp_new = PyType_GenericNew;
		if (PyType_Ready(&StateMachineType) < 0) {
			return nullptr;
		}

		Py_INCREF(&StateMachineType);
		PyModule_AddObject(module, "StateMachine", reinterpret_cast<PyObject*>(&StateMachineType));

		return reinterpret_cast<PyObject*>(&StateMachineType);
	}

	std::shared_ptr<StateMachine> as_state_machine(StateMachineObject* self) {
		return self->sm;
	}
	std::shared_ptr<StateMachine> as_state_machine(PyObject* self) {
		if (StateMachine_check(self)) {
			return as_state_machine(reinterpret_cast<StateMachineObject*>(self));
		}
		return nullptr;
	}

	void StateMachine_dealloc(PyObject* self) {
		StateMachineObject* _self = reinterpret_cast<StateMachineObject*>(self);
		_self->sm.reset();
		Py_TYPE(self)->tp_free(self);
	}
	PyObject* StateMachine_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
		StateMachineObject* self;

		self = reinterpret_cast<StateMachineObject*>(type->tp_alloc(type, 0));
		if (self != nullptr) {
			self->sm = nullptr;
		}

		return reinterpret_cast<PyObject*>(self);
	}
	int StateMachine_init(StateMachineObject* self, PyObject* args, PyObject* kwds) {
		self->sm = std::make_shared<StateMachine>();
		return 0;
	}

	PyObject* StateMachine_str(StateMachineObject* self) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		std::string s = sm->get_graph();
		return PyUnicode_FromString(s.c_str());
	}

	PyObject* StateMachine_print_state(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		sm->print_state();

		Py_RETURN_NONE;
	}
	PyObject* StateMachine_print_graph(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		sm->print_graph();

		Py_RETURN_NONE;
	}

	PyObject* StateMachine_add_state(StateMachineObject* self, PyObject* args, PyObject* kwds) {
		const char* kwlist[] = {"", "", "start", "update", "end", nullptr};

		PyObject* name;
		PyObject* output_names;
		PyObject *start_func = nullptr, *update_func = nullptr, *end_func = nullptr;

		if (!PyArg_ParseTupleAndKeywords(args, kwds, "UO!|OOO", const_cast<char**>(kwlist), &name, &PyList_Type, &output_names, &start_func, &update_func, &end_func)) {
			return nullptr;
		}

		std::string _name (PyUnicode_AsUTF8(name));

		std::set<std::string> _output_names;
		Py_ssize_t size = PyList_Size(output_names);
		for (Py_ssize_t i=0; i<size; ++i) {
			_output_names.emplace(PyUnicode_AsUTF8(PyList_GetItem(output_names, i)));
		}

		if ((start_func != nullptr)&&(start_func != Py_None)&&(!PyCallable_Check(start_func))) {
			PyErr_SetString(PyExc_TypeError, "parameter must be callable");
			return nullptr;
		}
		if ((update_func != nullptr)&&(update_func != Py_None)&&(!PyCallable_Check(update_func))) {
			PyErr_SetString(PyExc_TypeError, "parameter must be callable");
			return nullptr;
		}
		if ((end_func != nullptr)&&(end_func != Py_None)&&(!PyCallable_Check(end_func))) {
			PyErr_SetString(PyExc_TypeError, "parameter must be callable");
			return nullptr;
		}

		State state (_name, _output_names);

		if ((start_func != nullptr)&&(start_func != Py_None)) {
			Py_INCREF(start_func);
			state.start_func = [start_func] () {
				if (PyEval_CallObject(start_func, nullptr) == nullptr) {
					PyErr_Print();
				}
			};
		}
		if ((update_func != nullptr)&&(update_func != Py_None)) {
			Py_INCREF(update_func);
			state.update_func = [update_func] (Uint32 ticks) {
				PyObject* arg_tup = Py_BuildValue("(k)", ticks);
				if (PyEval_CallObject(update_func, arg_tup) == nullptr) {
					PyErr_Print();
				}
				Py_DECREF(arg_tup);
			};
		}
		if ((end_func != nullptr)&&(end_func != Py_None)) {
			Py_INCREF(end_func);
			state.end_func = [end_func] () {
				if (PyEval_CallObject(end_func, nullptr) == nullptr) {
					PyErr_Print();
				}
			};
		}

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyLong_FromLong(sm->add_state(state));
	}
	PyObject* StateMachine_remove_state(StateMachineObject* self, PyObject* args) {
		PyObject* name;

		if (!PyArg_ParseTuple(args, "U", &name)) {
			return nullptr;
		}

		std::string _name (PyUnicode_AsUTF8(name));

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyLong_FromLong(sm->remove_state(_name));
	}
	PyObject* StateMachine_clear(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		sm->clear();

		Py_RETURN_NONE;
	}

	PyObject* StateMachine_get_state(StateMachineObject* self, PyObject* args) {
		int n = 0;

		if (!PyArg_ParseTuple(args, "|i", &n)) {
			return nullptr;
		}

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyUnicode_FromString(sm->get_state(n).c_str());
	}
	PyObject* StateMachine_get_states(StateMachineObject* self, PyObject* args) {
		int is_forward = true;

		if (!PyArg_ParseTuple(args, "|p", &is_forward)) {
			return nullptr;
		}

		bool _is_forward = is_forward;

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyUnicode_FromString(sm->get_states(_is_forward).c_str());
	}
	PyObject* StateMachine_get_graph(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyUnicode_FromString(sm->get_graph().c_str());
	}
	PyObject* StateMachine_has_state(StateMachineObject* self, PyObject* args) {
		PyObject* name;

		if (!PyArg_ParseTuple(args, "U", &name)) {
			return nullptr;
		}

		std::string _name (PyUnicode_AsUTF8(name));

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyBool_FromLong(sm->has_state(_name));
	}

	PyObject* StateMachine_push_state(StateMachineObject* self, PyObject* args) {
		PyObject* name;

		if (!PyArg_ParseTuple(args, "U", &name)) {
			return name;
		}

		std::string _name (PyUnicode_AsUTF8(name));

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyLong_FromLong(sm->push_state(_name));
	}
	PyObject* StateMachine_pop_state(StateMachineObject* self, PyObject* args) {
		PyObject* name = nullptr;

		if (!PyArg_ParseTuple(args, "|U", &name)) {
			return nullptr;
		}

		std::string _name;
		if (name != nullptr) {
			_name = PyUnicode_AsUTF8(name);
		}

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyLong_FromLong((_name.empty()) ? sm->pop_state() : sm->pop_state(_name));
	}
	PyObject* StateMachine_pop_state_all(StateMachineObject* self, PyObject* args) {
		PyObject* name;

		if (!PyArg_ParseTuple(args, "U", &name)) {
			return nullptr;
		}

		std::string _name (PyUnicode_AsUTF8(name));

		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		return PyLong_FromLong(sm->pop_state_all(_name));
	}
	PyObject* StateMachine_pop_all(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		sm->pop_all();

		Py_RETURN_NONE;
	}

	PyObject* StateMachine_update(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		sm->update();

		Py_RETURN_NONE;
	}
	PyObject* StateMachine_update_all(StateMachineObject* self, PyObject* args) {
		std::shared_ptr<StateMachine> sm = as_state_machine(self);
		if (sm == nullptr) {
			return nullptr;
		}

		sm->update_all();

		Py_RETURN_NONE;
	}
}}}
