/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_DATA_STATEMACHINE_H
#define BEE_PYTHON_DATA_STATEMACHINE_H 1

#include <memory>

#include <Python.h>

#include "../../data/statemachine.hpp"

namespace bee { namespace python {
	PyObject* StateMachine_from(std::shared_ptr<StateMachine>);
	bool StateMachine_check(PyObject*);
namespace internal {
	typedef struct {
		PyObject_HEAD
		std::shared_ptr<StateMachine> sm;
	} StateMachineObject;

	extern PyTypeObject StateMachineType;

	PyObject* PyInit_bee_data_statemachine(PyObject*);

	std::shared_ptr<StateMachine> as_state_machine(StateMachineObject*);
	std::shared_ptr<StateMachine> as_state_machine(PyObject*);

	void StateMachine_dealloc(PyObject*);
	PyObject* StateMachine_new(PyTypeObject*, PyObject*, PyObject*);
	int StateMachine_init(StateMachineObject*, PyObject*, PyObject*);

	// StateMachine methods
	PyObject* StateMachine_str(StateMachineObject*);

	PyObject* StateMachine_print_state(StateMachineObject*, PyObject*);
	PyObject* StateMachine_print_graph(StateMachineObject*, PyObject*);

	PyObject* StateMachine_add_state(StateMachineObject*, PyObject*, PyObject*);
	PyObject* StateMachine_remove_state(StateMachineObject*, PyObject*);
	PyObject* StateMachine_clear(StateMachineObject*, PyObject*);

	PyObject* StateMachine_get_state(StateMachineObject*, PyObject*);
	PyObject* StateMachine_get_states(StateMachineObject*, PyObject*);
	PyObject* StateMachine_get_graph(StateMachineObject*, PyObject*);
	PyObject* StateMachine_has_state(StateMachineObject*, PyObject*);

	PyObject* StateMachine_push_state(StateMachineObject*, PyObject*);
	PyObject* StateMachine_pop_state(StateMachineObject*, PyObject*);
	PyObject* StateMachine_pop_state_all(StateMachineObject*, PyObject*);
	PyObject* StateMachine_pop_all(StateMachineObject*, PyObject*);

	PyObject* StateMachine_update(StateMachineObject*, PyObject*);
	PyObject* StateMachine_update_all(StateMachineObject*, PyObject*);
}}}

#endif // BEE_PYTHON_DATA_STATEMACHINE_H
