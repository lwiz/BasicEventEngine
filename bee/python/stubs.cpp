/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "python.hpp"

namespace bee { namespace python {
	int init() {
		return -1;
	}
	int close() {
		return -1;
	}
}}
