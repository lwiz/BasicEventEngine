/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <iostream>

#include "commands.hpp"

#include "../python.hpp"

#include "../../engine.hpp"

#include "../../util/debug.hpp"
#include "../../util/platform.hpp"
#include "../../util/files.hpp"

#include "../../init/gameoptions.hpp"

#include "../../messenger/messenger.hpp"

#include "../../core/console.hpp"
#include "../../core/enginestate.hpp"
#include "../../core/rooms.hpp"
#include "../../fs/fs.hpp"

#include "../../input/kb.hpp"
#include "../../input/keystrings.hpp"
#include "../../input/keybind.hpp"

#include "../../network/network.hpp"

#include "../../render/drawing.hpp"

#include "../../resource/sound.hpp"
#include "../../resource/room.hpp"

namespace bee { namespace python { namespace internal {
	PyMethodDef BEECommandsMethods[] = {
		{"quit", commands_quit, METH_NOARGS, "End the game"},
		{"find", commands_find, METH_VARARGS, "Output all commands which match a certain string"},
		{"clear", commands_clear, METH_NOARGS, "Clear the console log"},

		{"execfile", commands_execfile, METH_VARARGS, "Execute the specified config file"},
		{"load_map", commands_load_map, METH_VARARGS, "Load the specified map from the filesystem"},
		{"log", commands_log, METH_VARARGS, "Log a message to the console"},

		{"bind", commands_bind, METH_VARARGS, "Bind a key to a command"},
		{"unbind", commands_unbind, METH_VARARGS, "Unbind a key from a command"},

		{"screenshot", commands_screenshot, METH_VARARGS, "Save a screenshot to the given file"},

		{"verbosity", commands_verbosity, METH_VARARGS, "Set the verbosity level of the messenger"},

		{"volume", commands_volume, METH_VARARGS, "Set the global sound volume from 0.0 to 1.0"},

		{"info", commands_info, METH_NOARGS, "Output information about the current room"},
		{"restart", commands_restart, METH_NOARGS, "Restart the game"},
		{"restart_room", commands_restart_room, METH_NOARGS, "Restart the current room"},
		{"pause", commands_pause, METH_NOARGS, "Toggle the pause state of the game"},
		{"io", commands_io, METH_VARARGS, "Generate an Instance I/O event"},

		{"netstatus", commands_netstatus, METH_NOARGS, "Output information about the network session"},

		{nullptr, nullptr, 0, nullptr}
	};
	PyModuleDef BEECommandsModule = {
		PyModuleDef_HEAD_INIT, "commands", nullptr, -1, BEECommandsMethods,
		nullptr, nullptr, nullptr, nullptr
	};

	PyObject* PyInit_bee_commands() {
		return PyModule_Create(&BEECommandsModule);
	}

	PyObject* commands_quit(PyObject* self, PyObject* args) {
		kb::get_keybind("Quit").call(nullptr);

		Py_RETURN_NONE;
	}
	PyObject* commands_find(PyObject* self, PyObject* args) {
		PyObject* object;

		if (!PyArg_ParseTuple(args, "U", &object)) {
			return nullptr;
		}

		std::string _object (PyUnicode_AsUTF8(object));

		PyObject* dict = PyModule_GetDict(self);
		PyObject *key, *value;
		Py_ssize_t pos = 0;
		while (PyDict_Next(dict, &pos, &key, &value)) {
			std::string _key (PyUnicode_AsUTF8(key));
			if (_key.find(_object) != std::string::npos) {
				std::cout << _key << "\n";
				if (PyObject_HasAttrString(value, "__doc__")) {
					PyObject* doc (PyObject_GetAttrString(value, "__doc__"));
					std::string _doc (PyUnicode_AsUTF8(doc));
					std::cout << util::debug_indent(_doc, 1);
				}
			}
		}

		Py_RETURN_NONE;
	}
	PyObject* commands_clear(PyObject* self, PyObject* args) {
		console::clear();

		if (get_option("is_headless").i) {
			util::platform::commandline_clear();
		}

		Py_RETURN_NONE;
	}

	PyObject* commands_execfile(PyObject* self, PyObject* args) {
		PyObject* filename;

		if (!PyArg_ParseTuple(args, "U", &filename)) {
			return nullptr;
		}

		std::string _filename (PyUnicode_AsUTF8(filename));

		return PyLong_FromLong(Script::get_by_name("scr_console")->run_file(_filename));
	}
	PyObject* commands_load_map(PyObject* self, PyObject* args) {
		PyObject* mapname;
		int are_scripts_enabled = false;

		if (!PyArg_ParseTuple(args, "U|p", &mapname, &are_scripts_enabled)) {
			return nullptr;
		}

		std::string _mapname (PyUnicode_AsUTF8(mapname));
		std::string filename (_mapname);
		bool _are_scripts_enabled = are_scripts_enabled;

		// If the map doesn't exist as a directory, look for an XZ archive version of it
		if (!util::directory_exists("maps/"+filename)) {
			if (util::file_exists("maps/"+filename+".tar.xz")) {
				filename += ".tar.xz";
			} else {
				console::log(E_MESSAGE::WARNING, "Failed to find map \"" + _mapname + "\"");
				return PyLong_FromLong(-3);
			}
		}

		return PyLong_FromLong(fs::switch_level(_mapname, "maps/"+filename, "", _are_scripts_enabled));
	}
	PyObject* commands_log(PyObject* self, PyObject* args) {
		PyObject* str;
		unsigned long type = 3;

		if (!PyArg_ParseTuple(args, "U|k", &str, &type)) {
			return nullptr;
		}

		std::string _str (PyUnicode_AsUTF8(str));

		E_MESSAGE _type (static_cast<E_MESSAGE>(type));

		console::log(_type, _str);

		Py_RETURN_NONE;
	}

	PyObject* commands_bind(PyObject* self, PyObject* args) {
		PyObject* keystring;
		PyObject* command = nullptr;
		int is_repeatable = false;

		if (!PyArg_ParseTuple(args, "U|Up", &keystring, &command, &is_repeatable)) {
			return nullptr;
		}

		keystring = PyTuple_GetItem(args, 0);
		std::string _keystring (PyUnicode_AsUTF8(keystring));
		if (_keystring.rfind("SDLK_", 0) != 0) {
			PyErr_SetString(PyExc_ValueError, "the provided key name was not prefixed with \"SDLK_\"");
			return nullptr;
		}

		SDL_Keycode k (kb::keystrings_get_key(_keystring));

		bool _is_repeatable = is_repeatable;

		if (command == nullptr) {
			std::string _name (kb::get_keybind(k).name);
			return PyUnicode_FromString(_name.c_str());
		}

		std::string _command (PyUnicode_AsUTF8(command));
		KeyBind kb;
		if (_command.front() == '$') { // Bind to an existing KeyBind with the given name
			kb = kb::get_keybind(_command.substr(1));
			if (kb.name.empty()) {
				console::log(E_MESSAGE::WARNING, "Failed to find a KeyBind named \"" + _command.substr(1) + "\"");
				return PyLong_FromLong(-1);
			}

			kb.is_repeatable = _is_repeatable;
		} else { // Otherwise bind to the console string
			kb = KeyBind(_command, k, _is_repeatable, [_command] (const SDL_Event* e) {
				console::internal::run(_command, false);
			});
		}

		return PyLong_FromLong(kb::bind(k, kb));
	}
	PyObject* commands_unbind(PyObject* self, PyObject* args) {
		PyObject* keystring;

		if (!PyArg_ParseTuple(args, "U", &keystring)) {
			return nullptr;
		}

		std::string _keystring (PyUnicode_AsUTF8(keystring));

		if (_keystring == "all") {
			kb::unbind_all();
		} else if (_keystring.rfind("SDLK_", 0) == 0) {
			kb::unbind(kb::keystrings_get_key(_keystring));
		} else {
			kb::unbind(KeyBind(_keystring));
		}

		Py_RETURN_NONE;
	}

	PyObject* commands_screenshot(PyObject* self, PyObject* args) {
		PyObject* filename = nullptr;

		if (!PyArg_ParseTuple(args, "|U", &filename)) {
			return nullptr;
		}

		if (filename == nullptr) {
			render::save_screenshot("screenshot.bmp");
		} else {
			std::string _filename (PyUnicode_AsUTF8(filename));
			render::save_screenshot(_filename);
		}

		Py_RETURN_NONE;
	}

	PyObject* commands_verbosity(PyObject* self, PyObject* args) {
		int level = -1;

		if (!PyArg_ParseTuple(args, "|i", &level)) {
			return nullptr;
		}

		if (level < 0) {
			console::log(E_MESSAGE::INFO, std::to_string(static_cast<int>(messenger::get_level())));
		} else {
			level = std::min(level, static_cast<int>(E_OUTPUT::VERBOSE));
			messenger::set_level(static_cast<E_OUTPUT>(level));
		}

		Py_RETURN_NONE;
	}

	PyObject* commands_volume(PyObject* self, PyObject* args) {
		double volume = -1.0;

		if (!PyArg_ParseTuple(args, "|d", &volume)) {
			return nullptr;
		}

		if (volume < 0.0) {
			console::log(E_MESSAGE::INFO, std::to_string(Sound::get_master_volume()));
		} else {
			volume = std::min(volume, 1.0);
			Sound::set_master_volume(volume);
		}

		Py_RETURN_NONE;
	}

	PyObject* commands_info(PyObject* self, PyObject* args) {
		Variant m (get_current_room()->serialize());
		console::log(E_MESSAGE::INFO, "Room " + m.to_str(true));

		Py_RETURN_NONE;
	}
	PyObject* commands_restart(PyObject* self, PyObject* args) {
		restart_game();

		Py_RETURN_NONE;
	}
	PyObject* commands_restart_room(PyObject* self, PyObject* args) {
		restart_room();

		Py_RETURN_NONE;
	}
	PyObject* commands_pause(PyObject* self, PyObject* args) {
		set_is_paused(!get_is_paused());

		Py_RETURN_NONE;
	}
	PyObject* commands_io(PyObject* self, PyObject* args) {
		PyObject* inst_name;
		PyObject* data;
		Uint32 delay = 0;

		if (!PyArg_ParseTuple(args, "UO|I", &inst_name, &data, &delay)) {
			return nullptr;
		}

		std::string _inst_name (PyUnicode_AsUTF8(inst_name));

		Variant _data (pyobj_to_variant(data));

		get_current_room()->io(_inst_name, _data, delay);

		Py_RETURN_NONE;
	}

	PyObject* commands_netstatus(PyObject* self, PyObject* args) {
		console::log(E_MESSAGE::INFO, net::get_print());

		Py_RETURN_NONE;
	}
}}}
