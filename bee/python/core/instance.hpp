/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_INSTANCE_H
#define BEE_PYTHON_INSTANCE_H 1

#include <string>

#include <Python.h>

namespace bee {
	class Instance;
namespace python {
	PyObject* Instance_from(const Instance*);
	bool Instance_check(PyObject*);
namespace internal {
	typedef struct {
		PyObject_HEAD
		size_t id;
	} InstanceObject;

	extern PyTypeObject InstanceType;

	PyObject* PyInit_bee_instance(PyObject*);

	Instance* as_instance(InstanceObject*);
	Instance* as_instance(PyObject*);

	void Instance_dealloc(PyObject*);
	PyObject* Instance_new(PyTypeObject*, PyObject*, PyObject*);
	int Instance_init(InstanceObject*, PyObject*, PyObject*);
	Py_hash_t Instance_hash(InstanceObject*);
	PyObject* Instance_richcmp(InstanceObject*, InstanceObject*, int);

	// Instance methods
	PyObject* Instance_get_id(InstanceObject*, PyObject*);
	PyObject* Instance_get_name(InstanceObject*, PyObject*);
	PyObject* Instance_get_depth(InstanceObject*, PyObject*);

	PyObject* Instance_repr(InstanceObject*);
	PyObject* Instance_str(InstanceObject*);

	PyObject* Instance_serialize(InstanceObject*, PyObject*);
	PyObject* Instance_deserialize(InstanceObject*, PyObject*);
	PyObject* Instance_print(InstanceObject*, PyObject*);

	PyObject* Instance_set_alarm(InstanceObject*, PyObject*);

	PyObject* Instance_set_name(InstanceObject*, PyObject*);
	PyObject* Instance_set_depth(InstanceObject*, PyObject*);

	PyObject* Instance_add_component(InstanceObject*, PyObject*);
	PyObject* Instance_remove_component(InstanceObject*, PyObject*);
	PyObject* Instance_set_sprite(InstanceObject*, PyObject*);
	PyObject* Instance_set_computation_type(InstanceObject*, PyObject*);
	PyObject* Instance_set_is_persistent(InstanceObject*, PyObject*);

	PyObject* Instance_has_data(InstanceObject*, PyObject*);
	PyObject* Instance_get_data(InstanceObject*, PyObject*);
	PyObject* Instance_set_data(InstanceObject*, PyObject*);

	PyObject* Instance_get_pos(InstanceObject*, PyObject*);
	PyObject* Instance_get_aabb(InstanceObject*, PyObject*);
	PyObject* Instance_get_corner(InstanceObject*, PyObject*);
	PyObject* Instance_get_start(InstanceObject*, PyObject*);

	PyObject* Instance_get_components(InstanceObject*, PyObject*);
	PyObject* Instance_has_component(InstanceObject*, PyObject*);
	PyObject* Instance_get_main_object(InstanceObject*, PyObject*);
	PyObject* Instance_get_sprite(InstanceObject*, PyObject*);
	PyObject* Instance_get_physbody(InstanceObject*, PyObject*);
	PyObject* Instance_get_mass(InstanceObject*, PyObject*);
	PyObject* Instance_get_computation_type(InstanceObject*, PyObject*);
	PyObject* Instance_can_collide(InstanceObject*, PyObject*);
	PyObject* Instance_get_is_persistent(InstanceObject*, PyObject*);

	PyObject* Instance_set_pos(InstanceObject*, PyObject*);
	PyObject* Instance_set_to_start(InstanceObject*, PyObject*);
	PyObject* Instance_set_corner(InstanceObject*, PyObject*);
	PyObject* Instance_set_mass(InstanceObject*, PyObject*);
	PyObject* Instance_move(InstanceObject*, PyObject*);
	PyObject* Instance_set_friction(InstanceObject*, PyObject*);
	PyObject* Instance_set_gravity(InstanceObject*, PyObject*);
	PyObject* Instance_set_velocity(InstanceObject*, PyObject*);
	PyObject* Instance_add_velocity(InstanceObject*, PyObject*);
	PyObject* Instance_limit_velocity(InstanceObject*, PyObject*, PyObject*);

	PyObject* Instance_get_speed(InstanceObject*, PyObject*);
	PyObject* Instance_get_velocity(InstanceObject*, PyObject*);
	PyObject* Instance_get_velocity_ang(InstanceObject*, PyObject*);
	PyObject* Instance_get_friction(InstanceObject*, PyObject*);
	PyObject* Instance_get_gravity(InstanceObject*, PyObject*);

	PyObject* Instance_is_place_free(InstanceObject*, PyObject*);
	PyObject* Instance_is_place_empty(InstanceObject*, PyObject*);
	PyObject* Instance_is_place_meeting(InstanceObject*, PyObject*);
	PyObject* Instance_is_move_free(InstanceObject*, PyObject*);
	PyObject* Instance_is_snapped(InstanceObject*, PyObject*);

	PyObject* Instance_get_snapped(InstanceObject*, PyObject*);
	PyObject* Instance_move_random(InstanceObject*, PyObject*);
	PyObject* Instance_move_snap(InstanceObject*, PyObject*);
	PyObject* Instance_move_wrap(InstanceObject*, PyObject*);

	PyObject* Instance_get_distance(InstanceObject*, PyObject*, PyObject*);
	PyObject* Instance_get_direction_of(InstanceObject*, PyObject*, PyObject*);
	PyObject* Instance_get_relation(InstanceObject*, PyObject*);

	PyObject* Instance_draw(InstanceObject*, PyObject*);
}}}

#endif // BEE_PYTHON_INSTANCE_H
