/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_RENDER_TRANSITION_H
#define BEE_PYTHON_RENDER_TRANSITION_H 1

#include <Python.h>

namespace bee { namespace python { namespace internal {
	PyObject* render_get_transition_type(PyObject*, PyObject*);
	PyObject* render_set_transition_type(PyObject*, PyObject*);
	PyObject* render_set_transition_custom(PyObject*, PyObject*);
	PyObject* render_get_transition_speed(PyObject*, PyObject*);
	PyObject* render_set_transition_speed(PyObject*, PyObject*);
}}}

#endif // BEE_PYTHON_RENDER_TRANSITION_H
