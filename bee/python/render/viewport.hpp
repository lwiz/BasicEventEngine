/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_VIEWPORT_H
#define BEE_PYTHON_VIEWPORT_H 1

#include <memory>

#include <Python.h>

namespace bee {
	class ViewPort;
namespace python {
	PyObject* ViewPort_from(std::weak_ptr<ViewPort>);
	bool ViewPort_check(PyObject*);
namespace internal {
	typedef struct {
		PyObject_HEAD
		std::weak_ptr<ViewPort> vp;
	} ViewPortObject;

	extern PyTypeObject ViewPortType;

	PyObject* PyInit_bee_render_viewport(PyObject*);

	std::weak_ptr<ViewPort> as_viewport(ViewPortObject*);
	std::weak_ptr<ViewPort> as_viewport(PyObject*);

	void ViewPort_dealloc(PyObject*);
	PyObject* ViewPort_new(PyTypeObject*, PyObject*, PyObject*);

	// ViewPort methods
	PyObject* ViewPort_load(ViewPortObject*, PyObject*);

	PyObject* ViewPort_get_is_active(ViewPortObject*, PyObject*);
	PyObject* ViewPort_get_view(ViewPortObject*, PyObject*);
	PyObject* ViewPort_get_port(ViewPortObject*, PyObject*);
	PyObject* ViewPort_get_offset(ViewPortObject*, PyObject*);

	PyObject* ViewPort_set_is_active(ViewPortObject*, PyObject*);
	PyObject* ViewPort_set_view(ViewPortObject*, PyObject*);
	PyObject* ViewPort_set_view_center(ViewPortObject*, PyObject*);
	PyObject* ViewPort_set_port(ViewPortObject*, PyObject*);
	PyObject* ViewPort_set_update_func(ViewPortObject*, PyObject*);

	PyObject* ViewPort_update(ViewPortObject*, PyObject*);
	PyObject* ViewPort_draw(ViewPortObject*, PyObject*);
}}}

#endif // BEE_PYTHON_VIEWPORT_H
