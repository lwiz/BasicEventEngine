/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_RENDER_H
#define BEE_PYTHON_RENDER_H 1

#include <Python.h>

namespace bee { namespace python { namespace internal {
	PyObject* PyInit_bee_render();

	PyObject* render_set_is_lightable(PyObject*, PyObject*);

	PyObject* render_get_3d(PyObject*, PyObject*);
	PyObject* render_set_3d(PyObject*, PyObject*);
	PyObject* render_get_camera(PyObject*, PyObject*);
	PyObject* render_set_camera(PyObject*, PyObject*);

	PyObject* render_render_textures(PyObject*, PyObject*);
}}}

#endif // BEE_PYTHON_RENDER_H
