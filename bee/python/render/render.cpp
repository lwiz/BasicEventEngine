/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "render.hpp"

#include "../../messenger/messenger.hpp"

#include "../../render/render.hpp"
#include "../../render/camera.hpp"

#include "../resource/texture.hpp"

#include "drawing.hpp"
#include "transition.hpp"

namespace bee { namespace python { namespace internal {
	PyMethodDef BEERenderMethods[] = {
		// Render functions
		{"set_is_lightable", render_set_is_lightable, METH_VARARGS, "Set whether to enable lighting or not"},

		{"get_3d", render_get_3d, METH_NOARGS, "Return whether 3D mode is enabled or not"},
		{"set_3d", render_set_3d, METH_VARARGS, "Set whether 3D mode is enabled or not"},
		{"get_camera", render_get_camera, METH_NOARGS, "Get a copy of the camera values"},
		{"set_camera", render_set_camera, METH_VARARGS, "Set the camera position and angle for 3D mode"},

		{"render_textures", render_render_textures, METH_NOARGS, "Render all of the queued Textures"},

		// Drawing functions
		{"draw_triangle", render_draw_triangle, METH_VARARGS, "Draw a triangle between the given vertices in the given color"},
		{"draw_line", render_draw_line, METH_VARARGS, "Draw a line in the given color"},
		{"draw_quad", render_draw_quad, METH_VARARGS, "Draw a quad at the given coordinates"},
		{"draw_polygon", render_draw_polygon, METH_VARARGS, "Draw a polygon around the given center coordinate"},
		{"draw_arc", render_draw_arc, METH_VARARGS, "Draw a circular arc around the given center coordinate"},
		{"draw_circle", render_draw_circle, METH_VARARGS, "Draw a circle around the given center coordinate"},

		{"draw_set_color", render_draw_set_color, METH_VARARGS, "Set the current drawing color to the given value"},
		{"draw_get_color", render_draw_get_color, METH_NOARGS, "Return the current drawing color"},
		{"draw_set_blend", render_draw_set_blend, METH_VARARGS, "Set the current drawing blend mode to the given type"},
		{"draw_get_blend", render_draw_get_blend, METH_NOARGS, "Return the current drawing blend mode"},

		{"get_pixel_color", render_get_pixel_color, METH_VARARGS, "Return the color of the given pixel in the window"},
		{"save_screenshot", render_save_screenshot, METH_VARARGS, "Save a bitmap (.bmp) of the window to the given filename"},

		// Transition functions
		{"get_transition_type", render_get_transition_type, METH_NOARGS, "Return the transition type used when changing Rooms"},
		{"set_transition_type", render_set_transition_type, METH_VARARGS, "Set the transition type used when changing Rooms"},
		{"set_transition_custom", render_set_transition_custom, METH_VARARGS, "Set the transition type to a custom function"},
		{"get_transition_speed", render_get_transition_speed, METH_NOARGS, "Return the transition speed when drawing Room transitions"},
		{"set_transition_speed", render_set_transition_speed, METH_VARARGS, "Set the transition speed when drawing Room transitions"},

		{nullptr, nullptr, 0, nullptr}
	};
	PyModuleDef BEERenderModule = {
		PyModuleDef_HEAD_INIT, "render", nullptr, -1, BEERenderMethods,
		nullptr, nullptr, nullptr, nullptr
	};

	PyObject* PyInit_bee_render() {
		return PyModule_Create(&BEERenderModule);
	}

	PyObject* render_set_is_lightable(PyObject* self, PyObject* args) {
		int is_lightable;

		if (!PyArg_ParseTuple(args, "p", &is_lightable)) {
			return nullptr;
		}

		bool _is_lightable = is_lightable;

		render::set_is_lightable(_is_lightable);

		Py_RETURN_NONE;
	}

	PyObject* render_get_3d(PyObject* self, PyObject* args) {
		return PyBool_FromLong(render::get_3d());
	}
	PyObject* render_set_3d(PyObject* self, PyObject* args) {
		int is_3d;

		if (!PyArg_ParseTuple(args, "p", &is_3d)) {
			return nullptr;
		}

		bool _is_3d = is_3d;

		render::set_3d(_is_3d);

		Py_RETURN_NONE;
	}
	PyObject* render_get_camera(PyObject* self, PyObject* args) {
		const Camera& c = render::get_camera();

		return Py_BuildValue("((ddd)(ddd)(ddd))",
			c.position.x, c.position.y, c.position.z,
			c.direction.x, c.direction.y, c.direction.z,
			c.orientation.x, c.orientation.y, c.orientation.z
		);
	}
	PyObject* render_set_camera(PyObject* self, PyObject* args) {
		double px, py, pz;
		double dx, dy, dz;
		double ox, oy, oz;

		if (!PyArg_ParseTuple(
			args, "((ddd)(ddd)(ddd))",
			&px, &py, &pz,
			&dx, &dy, &dz,
			&ox, &oy, &oz
		)) {
			return nullptr;
		}

		glm::vec3 pos (px, py, pz);
		glm::vec3 dir (dx, dy, dz);
		glm::vec3 ori (ox, oy, oz);

		return PyLong_FromLong(render::set_camera(Camera(pos, dir, ori)));
	}

	PyObject* render_render_textures(PyObject* self, PyObject* args) {
		return PyLong_FromSize_t(render::render_textures());
	}
}}}
