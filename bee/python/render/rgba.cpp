/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <Python.h>
#include <structmember.h>

#include "rgba.hpp"

#include "../python.hpp"

namespace bee { namespace python {
	PyObject* RGBA_from(const RGBA& color) {
		PyObject* py_rgba = internal::RGBA_new(&internal::RGBAType, nullptr, nullptr);
		internal::RGBAObject* _py_rgba = reinterpret_cast<internal::RGBAObject*>(py_rgba);

		_py_rgba->color = color;

		return py_rgba;
	}
	bool RGBA_check(PyObject* obj) {
		return PyObject_TypeCheck(obj, &internal::RGBAType);
	}
namespace internal {
	PyMethodDef RGBAMethods[] = {
		{"get", reinterpret_cast<PyCFunction>(RGBA_get), METH_NOARGS, "Return a tuple of the color's RGBA values"},
		{"get_inverse", reinterpret_cast<PyCFunction>(RGBA_get_inverse), METH_NOARGS, "Return a tuple of the color's inverse RGBA values"},

		{"get_hsv", reinterpret_cast<PyCFunction>(RGBA_get_hsv), METH_NOARGS, "Return a tuple of the color's HSV values"},

		// FIXME: Do some terrible casting to get rid of an incompatible function type warning
		{"set_hsv", reinterpret_cast<PyCFunction>(reinterpret_cast<void (*)(void)>(RGBA_set_hsv)), METH_VARARGS | METH_KEYWORDS, "Set the color's HSV values"},
		{"add_hsv", reinterpret_cast<PyCFunction>(reinterpret_cast<void (*)(void)>(RGBA_add_hsv)), METH_VARARGS | METH_KEYWORDS, "Add to the color's HSV values"},

		{nullptr, nullptr, 0, nullptr}
	};

	PyTypeObject RGBAType = {
		PyVarObject_HEAD_INIT(NULL, 0)
		"bee.RGBA",
		sizeof(RGBAObject), 0,
		RGBA_dealloc,
		0,
		0, 0,
		0,
		reinterpret_cast<reprfunc>(RGBA_repr),
		0, 0, 0,
		0,
		0,
		reinterpret_cast<reprfunc>(RGBA_str),
		0, 0,
		0,
		Py_TPFLAGS_DEFAULT,
		"RGBA objects",
		0,
		0,
		0,
		0,
		0, 0,
		RGBAMethods,
		0,
		0,
		0,
		0,
		0, 0,
		0,
		reinterpret_cast<initproc>(RGBA_init),
		0, RGBA_new,
		0, 0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	};

	PyObject* PyInit_bee_render_rgba(PyObject* module) {
		RGBAType.tp_new = PyType_GenericNew;
		if (PyType_Ready(&RGBAType) < 0) {
			return nullptr;
		}

		Py_INCREF(&RGBAType);
		PyModule_AddObject(module, "RGBA", reinterpret_cast<PyObject*>(&RGBAType));

		return reinterpret_cast<PyObject*>(&RGBAType);
	}

	RGBA* as_rgba(RGBAObject* self) {
		return &self->color;
	}
	RGBA* as_rgba(PyObject* self) {
		if (RGBA_check(self)) {
			return as_rgba(reinterpret_cast<RGBAObject*>(self));
		}
		return nullptr;
	}

	void RGBA_dealloc(PyObject* self) {
		Py_TYPE(self)->tp_free(self);
	}
	PyObject* RGBA_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
		RGBAObject* self;

		self = reinterpret_cast<RGBAObject*>(type->tp_alloc(type, 0));
		if (self != nullptr) {
			self->color = {0, 0, 0, 0};
		}

		return reinterpret_cast<PyObject*>(self);
	}
	int RGBA_init(RGBAObject* self, PyObject* args, PyObject* kwds) {
		unsigned char r, g, b, a;
		if (!PyArg_ParseTuple(args, "BBBB", &r, &g, &b, &a)) {
			return -1;
		}

		self->color = {r, g, b, a};

		return 0;
	}

	PyObject* RGBA_repr(RGBAObject* self) {
		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}

		std::string s = "bee.RGBA(" +
			std::to_string(color->r) + ", " +
			std::to_string(color->g) + ", " +
			std::to_string(color->b) + ", " +
			std::to_string(color->a) +
		")";
		return PyUnicode_FromString(s.c_str());
	}
	PyObject* RGBA_str(RGBAObject* self) {
		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}

		std::string s = "(" +
			std::to_string(color->r) + ", " +
			std::to_string(color->g) + ", " +
			std::to_string(color->b) + ", " +
			std::to_string(color->a) +
		")";
		return PyUnicode_FromString(s.c_str());
	}

	PyObject* RGBA_get(RGBAObject* self, PyObject* args) {
		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}

		return Py_BuildValue("(bbbb)", color->r, color->g, color->b, color->a);
	}
	PyObject* RGBA_get_inverse(RGBAObject* self, PyObject* args) {
		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}

		RGBA inverse = color->get_inverse();

		return Py_BuildValue("(bbbb)", inverse.r, inverse.g, inverse.b, inverse.a);
	}

	PyObject* RGBA_get_hsv(RGBAObject* self, PyObject* args) {
		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}

		std::array<float,3> hsv = color->get_hsv();

		return Py_BuildValue("(ddd)", hsv[0], hsv[1], hsv[2]);
	}

	PyObject* RGBA_set_hsv(RGBAObject* self, PyObject* args, PyObject* kwds) {
		const char* kwlist[] = {"h", "s", "v", nullptr};

		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}
		std::array<float,3> hsv = color->get_hsv();

		double h = hsv[0], s = hsv[1], v = hsv[2];
		if (!PyArg_ParseTupleAndKeywords(args, kwds, "|ddd", const_cast<char**>(kwlist), &h, &s, &v)) {
			return nullptr;
		}

		hsv = {static_cast<float>(h), static_cast<float>(s), static_cast<float>(v)};

		color->set_hsv(hsv);

		Py_RETURN_NONE;
	}
	PyObject* RGBA_add_hsv(RGBAObject* self, PyObject* args, PyObject* kwds) {
		const char* kwlist[] = {"h", "s", "v", nullptr};

		RGBA* color = as_rgba(self);
		if (color == nullptr) {
			return nullptr;
		}
		std::array<float,3> hsv = color->get_hsv();

		double h = 0.0, s = 0.0, v = 0.0;
		if (!PyArg_ParseTupleAndKeywords(args, kwds, "|ddd", const_cast<char**>(kwlist), &h, &s, &v)) {
			return nullptr;
		}

		hsv[0] += h;
		hsv[1] += s;
		hsv[2] += v;

		color->set_hsv(hsv);

		Py_RETURN_NONE;
	}
}}}
