/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_PYTHON_RENDER_RGBA_H
#define BEE_PYTHON_RENDER_RGBA_H 1

#include <Python.h>

#include "../../render/rgba.hpp"

namespace bee { namespace python {
	PyObject* RGBA_from(const RGBA&);
	bool RGBA_check(PyObject*);
namespace internal {
	typedef struct {
		PyObject_HEAD
		RGBA color;
	} RGBAObject;

	extern PyTypeObject RGBAType;

	PyObject* PyInit_bee_render_rgba(PyObject*);

	RGBA* as_rgba(RGBAObject*);
	RGBA* as_rgba(PyObject*);

	void RGBA_dealloc(PyObject*);
	PyObject* RGBA_new(PyTypeObject*, PyObject*, PyObject*);
	int RGBA_init(RGBAObject*, PyObject*, PyObject*);

	// RGBA methods
	PyObject* RGBA_repr(RGBAObject*);
	PyObject* RGBA_str(RGBAObject*);

	PyObject* RGBA_get(RGBAObject*, PyObject*);
	PyObject* RGBA_get_inverse(RGBAObject*, PyObject*);

	PyObject* RGBA_get_hsv(RGBAObject*, PyObject*);

	PyObject* RGBA_set_hsv(RGBAObject*, PyObject*, PyObject*);
	PyObject* RGBA_add_hsv(RGBAObject*, PyObject*, PyObject*);
}}}

#endif // BEE_PYTHON_RENDER_RGBA_H
