/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <Python.h>
#include <structmember.h>

#include "lightdata.hpp"

#include "../python.hpp"

#include "../resource/light.hpp"

namespace bee { namespace python {
	PyObject* LightData_from(std::shared_ptr<LightData> ld) {
		PyObject* py_ld = internal::LightData_new(&internal::LightDataType, nullptr, nullptr);
		internal::LightDataObject* _py_ld = reinterpret_cast<internal::LightDataObject*>(py_ld);

		_py_ld->ld = ld;

		return py_ld;
	}
	bool LightData_check(PyObject* obj) {
		return PyObject_TypeCheck(obj, &internal::LightDataType);
	}
namespace internal {
	PyMethodDef LightDataMethods[] = {
		{"get_type", reinterpret_cast<PyCFunction>(LightData_get_type), METH_NOARGS, ""},
		{"get_position", reinterpret_cast<PyCFunction>(LightData_get_position), METH_NOARGS, ""},
		{"get_direction", reinterpret_cast<PyCFunction>(LightData_get_direction), METH_NOARGS, ""},
		{"get_attenuation", reinterpret_cast<PyCFunction>(LightData_get_attenuation), METH_NOARGS, ""},
		{"get_color", reinterpret_cast<PyCFunction>(LightData_get_color), METH_NOARGS, ""},

		{"set_position", reinterpret_cast<PyCFunction>(LightData_set_position), METH_VARARGS, ""},
		{"set_direction", reinterpret_cast<PyCFunction>(LightData_set_direction), METH_VARARGS, ""},
		{"set_attenuation", reinterpret_cast<PyCFunction>(LightData_set_attenuation), METH_VARARGS, ""},
		{"set_color", reinterpret_cast<PyCFunction>(LightData_set_color), METH_VARARGS, ""},

		{"draw", reinterpret_cast<PyCFunction>(LightData_draw), METH_VARARGS, ""},

		{nullptr, nullptr, 0, nullptr}
	};

	PyTypeObject LightDataType = {
		PyVarObject_HEAD_INIT(NULL, 0)
		"bee.LightData",
		sizeof(LightDataObject), 0,
		LightData_dealloc,
		0,
		0, 0,
		0,
		reinterpret_cast<reprfunc>(LightData_repr),
		0, 0, 0,
		0,
		0,
		reinterpret_cast<reprfunc>(LightData_str),
		0, 0,
		0,
		Py_TPFLAGS_DEFAULT,
		"LightData objects",
		0,
		0,
		0,
		0,
		0, 0,
		LightDataMethods,
		0,
		0,
		0,
		0,
		0, 0,
		0,
		reinterpret_cast<initproc>(LightData_init),
		0, LightData_new,
		0, 0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	};

	PyObject* PyInit_bee_render_lightdata(PyObject* module) {
		LightDataType.tp_new = PyType_GenericNew;
		if (PyType_Ready(&LightDataType) < 0) {
			return nullptr;
		}

		Py_INCREF(&LightDataType);
		PyModule_AddObject(module, "LightData", reinterpret_cast<PyObject*>(&LightDataType));

		return reinterpret_cast<PyObject*>(&LightDataType);
	}

	std::shared_ptr<LightData> as_lightdata(LightDataObject* self) {
		return self->ld;
	}
	std::shared_ptr<LightData> as_lightdata(PyObject* self) {
		if (LightData_check(self)) {
			return as_lightdata(reinterpret_cast<LightDataObject*>(self));
		}
		return {};
	}

	void LightData_dealloc(PyObject* self) {
		Py_TYPE(self)->tp_free(self);
	}
	PyObject* LightData_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
		LightDataObject* self;

		self = reinterpret_cast<LightDataObject*>(type->tp_alloc(type, 0));
		if (self != nullptr) {
			self->ld = {};
		}

		return reinterpret_cast<PyObject*>(self);
	}
	int LightData_init(LightDataObject* self, PyObject* args, PyObject* kwds) {
		LightObject* l;
		if (!PyArg_ParseTuple(args, "O!", &LightType, &l)) {
			return -1;
		}

		self->ld = std::make_shared<LightData>(as_light(l));

		return 0;
	}

	PyObject* LightData_repr(LightDataObject* self) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		std::string s = "bee.LightData()";
		return PyUnicode_FromString(s.c_str());
	}
	PyObject* LightData_str(LightDataObject* self) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		std::string s = "()";
		return PyUnicode_FromString(s.c_str());
	}

	PyObject* LightData_get_type(LightDataObject* self, PyObject* args) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		return Py_BuildValue("i", static_cast<int>(ld->type));
	}
	PyObject* LightData_get_position(LightDataObject* self, PyObject* args) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		glm::vec4 pos = ld->position;
		double px = pos.x, py = pos.y, pz = pos.z, pw = pos.w;
		return Py_BuildValue("(dddd)", px, py, pz, pw);
	}
	PyObject* LightData_get_direction(LightDataObject* self, PyObject* args) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		glm::vec4 dir = ld->direction;
		double dx = dir.x, dy = dir.y, dz = dir.z, dw = dir.w;
		return Py_BuildValue("(dddd)", dx, dy, dz, dw);
	}
	PyObject* LightData_get_attenuation(LightDataObject* self, PyObject* args) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		glm::vec4 att = ld->attenuation;
		double ax = att.x, ay = att.y, az = att.z, aw = att.w;
		return Py_BuildValue("(dddd)", ax, ay, az, aw);
	}
	PyObject* LightData_get_color(LightDataObject* self, PyObject* args) {
		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		RGBA color = ld->color;
		return Py_BuildValue("(BBBB)", color.r, color.g, color.b, color.a);
	}

	PyObject* LightData_set_position(LightDataObject* self, PyObject* args) {
		double px, py, pz, pw;

		if (!PyArg_ParseTuple(args, "(dddd)", &px, &py, &pz, &pw)) {
			return nullptr;
		}

		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		ld->set_position(glm::vec4(px, py, pz, pw));

		Py_RETURN_NONE;
	}
	PyObject* LightData_set_direction(LightDataObject* self, PyObject* args) {
		double dx, dy, dz, dw;

		if (!PyArg_ParseTuple(args, "(dddd)", &dx, &dy, &dz, &dw)) {
			return nullptr;
		}

		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		ld->set_direction(glm::vec4(dx, dy, dz, dw));

		Py_RETURN_NONE;
	}
	PyObject* LightData_set_attenuation(LightDataObject* self, PyObject* args) {
		double ax, ay, az, aw;

		if (!PyArg_ParseTuple(args, "(dddd)", &ax, &ay, &az, &aw)) {
			return nullptr;
		}

		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		ld->set_attenuation(glm::vec4(ax, ay, az, aw));

		Py_RETURN_NONE;
	}
	PyObject* LightData_set_color(LightDataObject* self, PyObject* args) {
		RGBA color;

		if (!PyArg_ParseTuple(args, "(BBBB)", &color.r, &color.g, &color.b, &color.a)) {
			return nullptr;
		}

		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		ld->set_color(color);

		Py_RETURN_NONE;
	}

	PyObject* LightData_draw(LightDataObject* self, PyObject* args) {
		int comp_type = static_cast<int>(E_COMPUTATION::DYNAMIC);

		if (!PyArg_ParseTuple(args, "|i", &comp_type)) {
			return nullptr;
		}

		E_COMPUTATION _comp_type = static_cast<E_COMPUTATION>(comp_type);

		std::shared_ptr<LightData> ld = as_lightdata(self);
		if (ld == nullptr) {
			return nullptr;
		}

		ld->draw(_comp_type, ld);

		Py_RETURN_NONE;
	}
}}}
