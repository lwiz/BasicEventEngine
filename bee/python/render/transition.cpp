/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "transition.hpp"

#include "../../render/transition.hpp"

#include "../resource/texture.hpp"

namespace bee { namespace python { namespace internal {
	PyObject* render_get_transition_type(PyObject* self, PyObject* args) {
		return PyLong_FromLong(static_cast<int>(render::get_transition_type()));
	}
	PyObject* render_set_transition_type(PyObject* self, PyObject* args) {
		int type;

		if (!PyArg_ParseTuple(args, "i", &type)) {
			return nullptr;
		}

		E_TRANSITION _type = static_cast<E_TRANSITION>(type);

		render::set_transition_type(_type);

		Py_RETURN_NONE;
	}
	PyObject* render_set_transition_custom(PyObject* self, PyObject* args) {
		PyObject* callback;

		if (!PyArg_ParseTuple(args, "O", &callback)) {
			return nullptr;
		}

		if (!PyCallable_Check(callback)) {
			PyErr_SetString(PyExc_TypeError, "parameter must be callable");
			return nullptr;
		}

		Py_INCREF(callback);
		render::set_transition_custom([callback] (Texture* before, Texture* after) {
			PyObject* arg_tup = Py_BuildValue("(NN)", Texture_from(before), Texture_from(after));
			if (PyEval_CallObject(callback, arg_tup) == nullptr) {
				PyErr_Print();
			}
			Py_DECREF(arg_tup);
		});

		Py_RETURN_NONE;
	}
	PyObject* render_get_transition_speed(PyObject* self, PyObject* args) {
		return PyFloat_FromDouble(render::get_transition_speed());
	}
	PyObject* render_set_transition_speed(PyObject* self, PyObject* args) {
		double speed;

		if (!PyArg_ParseTuple(args, "d", &speed)) {
			return nullptr;
		}

		render::set_transition_speed(speed);

		Py_RETURN_NONE;
	}
}}}
