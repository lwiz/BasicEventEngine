/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include <cstdlib>
#include <vector>

#include "python.hpp"

#include "../util/string.hpp"
#include "../util/platform.hpp"

#include "../messenger/messenger.hpp"

#include "../core/enginestate.hpp"
#include "../core/instance.hpp"
#include "../fs/fs.hpp"

#include "beemodule.hpp"
#include "render/rgba.hpp"
#include "data/statemachine.hpp"

#include "../resource/texture.hpp"
#include "../resource/sound.hpp"
#include "../resource/font.hpp"
#include "../resource/path.hpp"
#include "../resource/timeline.hpp"
#include "../resource/mesh.hpp"
#include "../resource/light.hpp"
#include "../resource/script.hpp"
#include "../resource/object.hpp"
#include "../resource/room.hpp"

namespace bee { namespace python {
	namespace internal {
		wchar_t* program = nullptr;
		std::vector<wchar_t*> argv;

		Variant dh;
	}

	/**
	* Initialize the Python scripting system.
	* @retval 0 success
	* @retval 1 failed to decode argv[0] for the program name
	*/
	int init() {
		if (internal::program != nullptr) {
			return 0; // Return 0 if Python is already initialized
		}

		internal::program = Py_DecodeLocale(engine->argv[0], nullptr);
		if (internal::program == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to decode program argv[0]");
			return 1;
		}

		wchar_t a0[1] = {0};
		internal::argv.push_back(a0);
		for (int i=1; i<engine->argc; ++i) {
			wchar_t* arg = Py_DecodeLocale(engine->argv[i], nullptr);
			if (arg == nullptr) {
				continue;
			}
			internal::argv.push_back(arg);
		}

		internal::init_module();

		switch (util::platform::get_platform()) {
			case util::E_PLATFORM::LINUX: {
				util::platform::setenv("PYTHONPATH", "lib/cpython/build/lib.linux-x86_64-3.7:lib/cpython/Lib:bee/resources", 1);
				util::platform::setenv("PYTHONHOME", "lib/cpython", 1);
				break;
			}
			case util::E_PLATFORM::WINDOWS: {
				util::platform::setenv("PYTHONPATH", "lib/cpython/PCbuild/amd64:lib/cpython/Lib:bee/resources", 1);
				util::platform::setenv("PYTHONHOME", "lib/cpython", 1);
				break;
			}
			case util::E_PLATFORM::MACOS: {
				util::platform::setenv("PYTHONPATH", "lib/cpython/build/lib.macosx-10.14-x86_64-3.7:lib/cpython/Lib:bee/resources", 1);
				util::platform::setenv("PYTHONHOME", "lib/cpython", 1);
				break;
			}
			default: {}
		}

		Py_SetProgramName(internal::program);
		Py_Initialize();

		PySys_SetArgv(static_cast<int>(internal::argv.size()), internal::argv.data());

		return 0;
	}
	/**
	* Close the Python scripting system.
	* @retval 0 success
	* @retval 1 failed to finalize and flush memory buffers
	*/
	int close() {
		if (internal::program == nullptr) {
			return 0; // Return 0 if Python is already closed
		}

		if (Py_FinalizeEx() < 0) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to close python scripting");
			return 1;
		}

		PyMem_RawFree(internal::program);
		internal::program = nullptr;

		while (internal::argv.size() > 1) {
			PyMem_RawFree(internal::argv.back());
			internal::argv.pop_back();
		}
		internal::argv.clear();

		return 0;
	}

	/**
	* Run the given string in the __main__ module
	* @param code the code string to run
	*
	* @retval 0 success
	* @retval 1 a Python exception was raised during execution
	*/
	int run_string(const std::string& code) {
		if (PyRun_SimpleString(code.c_str()) < 0) {
			return 1;
		}
		return 0;
	}
	/**
	* Run the given file in the __main__ module
	* @param filename the file to run
	*
	* @retval 0 success
	* @retval 1 failed to find file
	* @retval 2 failed to open file
	* @retval 3 a Python exception was raised during execution
	*/
	int run_file(const std::string& filename) {
		if (!fs::exists(filename)) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to run file \"" + filename + "\": file does not exist");
			return 1;
		}

		if (run_string(fs::get_file(filename).get())) {
			return 2;
		}

		return 0;
	}

	/**
	* @returns a string of the last Python traceback
	*/
	std::string get_traceback() {
		PyObject* sys = PyImport_AddModule("sys");

		PyObject* dict = PyModule_GetDict(sys);

		PyObject* type = PyDict_GetItemString(dict, "last_type");
		PyObject* value = PyDict_GetItemString(dict, "last_value");
		PyObject* traceback = PyDict_GetItemString(dict, "last_traceback");
		if ((type == nullptr)||(value == nullptr)||(traceback == nullptr)) {
			return "";
		}

		std::string _traceback;
		if (traceback != Py_None) {
			_traceback += "Traceback (most recent call last):\n";
			while (traceback != Py_None) {
				PyObject* frame = PyObject_GetAttrString(traceback, "tb_frame");
				PyObject* code = PyObject_GetAttrString(frame, "f_code");

				_traceback += "File \"";
				_traceback += PyUnicode_AsUTF8(PyObject_Str(
					PyObject_GetAttrString(code, "co_filename")
				));
				_traceback += "\", line ";
				_traceback += PyUnicode_AsUTF8(PyObject_Str(
					PyObject_GetAttrString(frame, "f_lineno")
				));
				_traceback += ", in ";
				_traceback += PyUnicode_AsUTF8(PyObject_Str(
					PyObject_GetAttrString(code, "co_name")
				));
				_traceback += "\n";

				if (PyObject_GetAttrString(traceback, "tb_next") == Py_None) {
					break;
				}
				traceback = PyObject_GetAttrString(traceback, "tb_next");
			}

			_traceback += PyUnicode_AsUTF8(PyObject_GetAttrString(type, "__name__"));
			_traceback += ": ";
			_traceback += PyUnicode_AsUTF8(PyObject_Str(value));
		} else {
			_traceback += "File \"";
			_traceback += PyUnicode_AsUTF8(PyObject_GetAttrString(value, "filename"));
			_traceback += "\", line ";
			_traceback += PyUnicode_AsUTF8(PyObject_Str(PyObject_GetAttrString(value, "lineno")));
			_traceback += "\n  ";
			_traceback += PyUnicode_AsUTF8(PyObject_GetAttrString(value, "text"));
			_traceback += "\n";
			long i = 2 + PyLong_AsLong(PyObject_GetAttrString(value, "offset"));
			_traceback += util::string::repeat(std::max(i-1l, 0l), " ");
			_traceback += "^\n";

			_traceback += PyUnicode_AsUTF8(PyObject_GetAttrString(type, "__name__"));
			_traceback += ": ";
			_traceback += PyUnicode_AsUTF8(PyObject_GetAttrString(value, "msg"));
		}

		return _traceback;
	}

	/**
	* Store the last evaluated Python object in the internal displayhook Variant.
	* @param obj the object to store
	*/
	void set_displayhook(PyObject* obj) {
		internal::dh = pyobj_to_variant(obj);
	}
	/**
	* @returns the internal displayhook Variant
	*/
	Variant get_displayhook() {
		Variant v (internal::dh);
		internal::dh = Variant();
		return v;
	}

	/**
	* Convert a PyObject to a Variant
	* @param obj the PyObject to convert
	*
	* @returns the converted value
	*/
	Variant pyobj_to_variant(PyObject* obj) {
		Variant var;

		if ((obj == nullptr)||(obj == Py_None)) {
			var = Variant(E_DATA_TYPE::NONE);
		} else if (PyLong_Check(obj)) {
			var = PyLong_AsLong(obj);
		} else if (PyBool_Check(obj)) {
			var = (obj == Py_True);
		} else if (PyFloat_Check(obj)) {
			var = PyFloat_AsDouble(obj);
		} else if (PyUnicode_Check(obj)) {
			var = PyUnicode_AsUTF8(obj);
		} else if (PyTuple_Check(obj)) {
			var = Variant(E_DATA_TYPE::VECTOR);

			Py_ssize_t size = PyTuple_Size(obj);
			for (Py_ssize_t i=0; i<size; ++i) {
				var.v.push_back(pyobj_to_variant(PyTuple_GetItem(obj, i)));
			}
		} else if (PyList_Check(obj)) {
			var = Variant(E_DATA_TYPE::VECTOR);

			Py_ssize_t size = PyList_Size(obj);
			for (Py_ssize_t i=0; i<size; ++i) {
				var.v.push_back(pyobj_to_variant(PyList_GetItem(obj, i)));
			}
		} else if (PyDict_Check(obj)) {
			var = Variant(E_DATA_TYPE::MAP);

			PyObject *key, *value;
			Py_ssize_t pos = 0;
			while (PyDict_Next(obj, &pos, &key, &value)) {
				var.m.emplace(pyobj_to_variant(key), pyobj_to_variant(value));
			}
		} else if (Texture_check(obj)) {
			var = internal::as_texture(obj);
		} else if (Sound_check(obj)) {
			var = internal::as_sound(obj);
		} else if (Font_check(obj)) {
			var = internal::as_font(obj);
		} else if (Path_check(obj)) {
			var = internal::as_path(obj);
		} else if (Timeline_check(obj)) {
			var = internal::as_timeline(obj);
		} else if (Mesh_check(obj)) {
			var = internal::as_mesh(obj);
		} else if (Light_check(obj)) {
			var = internal::as_light(obj);
		} else if (Script_check(obj)) {
			var = internal::as_script(obj);
		} else if (Object_check(obj)) {
			var = internal::as_object(obj);
		} else if (Room_check(obj)) {
			var = internal::as_room(obj);
		} else if (Instance_check(obj)) {
			var = internal::as_instance(obj);
		} else if (RGBA_check(obj)) {
			var = internal::as_rgba(obj);
		} else if (StateMachine_check(obj)) {
			var = internal::as_state_machine(obj);
		} else {
			Py_INCREF(obj);
			var = obj;
		}

		return var;
	}
	/**
	* Convert a Variant to a PyObject
	* @param var the Variant to convert
	*
	* @returns the converted value
	*/
	PyObject* variant_to_pyobj(Variant var) {
		switch (var.get_type()) {
			case E_DATA_TYPE::NONE: {
				Py_RETURN_NONE;
			}
			case E_DATA_TYPE::CHAR: {
				return PyUnicode_FromString(std::string(1, var.c).c_str());
			}
			case E_DATA_TYPE::INTEGER: {
				return PyLong_FromLong(var.i);
			}
			case E_DATA_TYPE::FLOATING: {
				return PyFloat_FromDouble(var.f);
			}
			case E_DATA_TYPE::STRING: {
				return PyUnicode_FromString(var.s.c_str());
			}
			case E_DATA_TYPE::VECTOR: {
				PyObject* obj = PyList_New(var.v.size());
				Py_ssize_t i = 0;
				for (auto& e : var.v) {
					PyList_SetItem(obj, i++, variant_to_pyobj(e));
				}
				return obj;
			}
			case E_DATA_TYPE::MAP: {
				PyObject* obj = PyDict_New();
				for (auto& e : var.m) {
					PyDict_SetItem(obj, variant_to_pyobj(e.first), variant_to_pyobj(e.second));
				}
				return obj;
			}
			case E_DATA_TYPE::SERIAL: {
				std::vector<Uint8> sdv (var.sd.get());
				PyObject* obj = PyList_New(sdv.size());
				Py_ssize_t i = 0;
				for (auto& e : sdv) {
					PyList_SetItem(obj, i++, PyLong_FromLong(e));
				}
				return obj;
			}
			case E_DATA_TYPE::POINTER: {
				if (var.p == nullptr) {
					Py_RETURN_NONE;
				} else if (var.get_ptype() == std::type_index(typeid(Texture*))) {
					Texture* p = static_cast<Texture*>(var.p);
					return Texture_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Sound*))) {
					Sound* p = static_cast<Sound*>(var.p);
					return Sound_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Font*))) {
					Font* p = static_cast<Font*>(var.p);
					return Font_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Path*))) {
					Path* p = static_cast<Path*>(var.p);
					return Path_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Timeline*))) {
					Timeline* p = static_cast<Timeline*>(var.p);
					return Timeline_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Mesh*))) {
					Mesh* p = static_cast<Mesh*>(var.p);
					return Mesh_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Light*))) {
					Light* p = static_cast<Light*>(var.p);
					return Light_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Script*))) {
					Script* p = static_cast<Script*>(var.p);
					return Script_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Object*))) {
					Object* p = static_cast<Object*>(var.p);
					return Object_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Room*))) {
					Room* p = static_cast<Room*>(var.p);
					return Room_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(Instance*))) {
					Instance* p = static_cast<Instance*>(var.p);
					return Instance_from(p);
				} else if (var.get_ptype() == std::type_index(typeid(RGBA*))) {
					RGBA* color = static_cast<RGBA*>(var.p);
					return RGBA_from(*color);
				} else if (var.get_ptype() == std::type_index(typeid(PyObject*))) {
					PyObject* obj = static_cast<PyObject*>(var.p);
					Py_INCREF(obj);
					return obj;
				}

				return nullptr;
			}
			case E_DATA_TYPE::SHARED_PTR: {
				if (var.sp == nullptr) {
					Py_RETURN_NONE;
				} else if (var.get_ptype() == std::type_index(typeid(StateMachine))) {
				   std::shared_ptr<StateMachine> sm = std::static_pointer_cast<StateMachine>(var.sp);
				   return StateMachine_from(sm);
			   }

			   return nullptr;
			}
		}
		return nullptr;
	}
}}
