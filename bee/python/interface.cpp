/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "interface.hpp"

#include "../util/string.hpp"

#include "../fs/fs.hpp"
#include "../fs/python.hpp"

#include "python.hpp"

namespace bee {
	PythonScriptInterface::PythonScriptInterface(const std::string& _path) :
		path(_path),
		module(nullptr)
	{}
	PythonScriptInterface::PythonScriptInterface(PyObject* _module) :
		path(),
		module(_module)
	{}
	PythonScriptInterface::~PythonScriptInterface() {
		this->free();
	}
	/**
	* Load the module from the path.
	* @retval 0 success
	* @retval 1 failed since it is already loaded
	* @retval 2 failed since the path does not exist
	* @retval 3 failed to import, see the Python exception for more info
	*/
	int PythonScriptInterface::load() {
		if (module != nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to load python script \"" + path + "\": script is already loaded");
			return 1;
		}

		if (path == ".py") { // An "empty" path loads the main module
			module = PyImport_AddModule("__main__");
			return 0;
		}

		if (!fs::exists(path)) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to load python script \"" + path + "\": file does not exist");
			return 2;
		}

		PyObject* _module = fs::python::import(path, "");
		if (_module == nullptr) {
			PyErr_Print();
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to import python script \"" + path + "\"");

			return 3;
		}

		module = _module;

		return 0;
	}
	/**
	* DecRef the module and remove it from this interface.
	*/
	void PythonScriptInterface::free() {
		if (module == nullptr) {
			return;
		}

		if (module != PyImport_AddModule("__main__")) {
			Py_DECREF(module);
		}
		module = nullptr;
	}
	/**
	* Remove the module from this interface without DecRefing it.
	*/
	void PythonScriptInterface::release() {
		path.clear();
		module = nullptr;
	}
	/**
	* Run the given string in the loaded module.
	* @param code the code string to run
	* @param filename the source file of the code string
	* @param retval the pointer to store the code return value in
	* @param start the start point of the Python interpreter, either Py_file_input or Py_single_input
	*
	* @retval 0 success
	* @retval 1 failed since the module is not loaded
	* @retval 2 failed to compile the code string, see the Python exception for more info
	* @retval 3 failed to evaluate the code, see the Python exception for more info
	*/
	int PythonScriptInterface::run_string(const std::string& code, const std::string& filename, Variant* retval, int start) {
		if (module == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to run python string \"" + path + "\": script is not loaded");
			return 1;
		}

		if (start != Py_file_input) {
			std::vector<std::string> _code (util::splitv(code, '\n', true));
			if (_code.size() > 1) {
				// Check whether the code should be ran as a file
				for (auto& l : _code) {
					if ((l[0] == '\t')||(l[0] == ' ')) {
						start = Py_file_input;
						break;
					}
				}

				if (start != Py_file_input) {
					for (auto& l : _code) {
						int r = run_string(l, retval);
						if (r != 0) {
							if (retval != nullptr) {
								*retval = Variant();
							}
							return r;
						}
					}
					return 0;
				}
			}
		}

		PyObject* codeobj = Py_CompileString(code.c_str(), filename.c_str(), start);
		if (codeobj == nullptr) {
			PyErr_Print();
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Python script codestring \"" + code + "\" compile failed for \"" + path + "\"");

			return 2;
		}

		PyObject* global_dict = PyModule_GetDict(module);
		PyObject* value = PyEval_EvalCode(codeobj, global_dict, global_dict);
		if (value == nullptr) {
			Py_DECREF(codeobj);

			PyErr_Print();
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Python script codestring \"" + code + "\" failed for \"" + path + "\"");

			return 3;
		}

		if (retval != nullptr) {
			if (value == Py_None) {
				*retval = python::get_displayhook();
			} else {
				*retval = python::pyobj_to_variant(value);
			}
		}
		Py_DECREF(value);

		Py_DECREF(codeobj);

		return 0;
	}
	/**
	* Run the given string as a single input.
	* @param code the code string to run
	* @param retval the pointer to store the code return value in
	*
	* @see run_string(const std::string&, Variant*, int) for return values
	*/
	int PythonScriptInterface::run_string(const std::string& code, Variant* retval) {
		return run_string(code, "<string>", retval, Py_single_input);
	}
	/**
	* Run the given file in the loaded module.
	* @param filename the file to run
	*
	* @retval -1 failed since the file doesn't exist in the filesystem
	* @see run_string(const std::string&, Variant*, int) for return values
	*/
	int PythonScriptInterface::run_file(const std::string& filename) {
		FilePath fp (fs::get_file(filename));
		if (fp.exists()) {
			return run_string(fp.get(), fp.get_path(), nullptr, Py_file_input);
		}
		return -1;
	}
	/**
	* Run the given function in the loaded module.
	* @param funcname the function to run
	* @param args the function arguments
	* @param retval the pointer to store the function return value in
	*
	* @retval 0 success
	* @retval 1 failed since the module is not loaded
	* @retval 2 failed to find a callable object with the given function name
	* @retval 3 failed to convert function arguments
	* @retval 4 failed to call the function, see the Python exception for more info
	*/
	int PythonScriptInterface::run_func(const std::string& funcname, const Variant& args, Variant* retval) {
		if (module == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to run python function \"" + path + "\": script is not loaded");
			return 1;
		}

		PyObject* func = PyObject_GetAttrString(module, funcname.c_str());
		if ((func == nullptr)||(!PyCallable_Check(func))) {
			if (PyErr_Occurred()) {
				PyErr_Print();
			}
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to find python script function \"" + funcname + "\" for \"" + path + "\"");

			return 2;
		}

		PyObject* _args = nullptr;
		if (args.get_type() != E_DATA_TYPE::VECTOR) {
			if (args.get_type() != E_DATA_TYPE::NONE) {
				messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to convert function arguments for \"" + funcname + "\" in \"" + path + "\": arguments must be a vector");
				return 3;
			}
		} else {
			const std::vector<Variant>& vargs = args.v;
			_args = PyTuple_New(vargs.size());

			size_t i = 0;
			for (auto& a : vargs) {
				PyTuple_SetItem(_args, i++, python::variant_to_pyobj(a));
			}
		}

		PyObject* value = PyObject_CallObject(func, _args);
		if (value == nullptr) {
			Py_XDECREF(_args);
			Py_DECREF(func);

			PyErr_Print();
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Python script function \"" + funcname + "\" failed for \"" + path + "\"");

			return 4;
		}

		if (retval != nullptr) {
			*retval = python::pyobj_to_variant(value);
		}
		Py_DECREF(value);

		Py_XDECREF(_args);
		Py_DECREF(func);

		return 0;
	}

	/**
	* Set the given Python name to the given value.
	* @param name the name to set
	* @param value the value to set the name to
	*
	* @retval 0 success
	* @retval 1 failed since the module is not loaded
	* @retval 2 failed to set the attribute
	*/
	int PythonScriptInterface::set_var(const std::string& name, PyObject* value) {
		if (module == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to set python variable \"" + name + "\": script \"" + path + "\" is not loaded");
			return 1;
		}

		if (PyObject_SetAttrString(module, name.c_str(), value) < 0) {
			PyErr_Print();
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to set python variable \"" + name + "\" for \"" + path + "\"");

			return 2;
		}

		return 0;
	}
	int PythonScriptInterface::set_var(const std::string& name, const Variant& value) {
		return set_var(name, python::variant_to_pyobj(value));
	}
	/**
	* @param name the name of the variable to check for
	*
	* @returns whether a variable with the given name exists or false when not loaded
	*/
	bool PythonScriptInterface::has_var(const std::string& name) const {
		if (module == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to get python variable \"" + name + "\": script \"" + path + "\" is not loaded");
			return false;
		}

		return PyObject_HasAttrString(module, name.c_str());
	}
	/**
	* @param name the name of the variable to get
	*
	* @returns the variable with the given name or an empty variable when the name does not exist
	*/
	Variant PythonScriptInterface::get_var(const std::string& name) const {
		if (module == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to get python variable \"" + name + "\": script \"" + path + "\" is not loaded");
			return Variant();
		}

		PyObject* var = PyObject_GetAttrString(module, name.c_str());
		if (var == nullptr) {
			PyErr_Print();
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to get python variable \"" + name + "\" from \"" + path + "\"");

			return Variant();
		}

		Variant v (python::pyobj_to_variant(var));

		Py_DECREF(var);

		return v;
	}

	/**
	* Find all attributes which match the given string.
	* @param input the string to match against
	*
	* @returns a vector of possible matches
	*/
	std::vector<Variant> PythonScriptInterface::complete(const std::string& input) const {
		std::vector<Variant> vec;

		if (module == nullptr) {
			messenger::send({"engine", "python"}, E_MESSAGE::ERROR, "Failed to complete python names: script \"" + path + "\" is not loaded");
			return vec;
		}

		PyObject* dict = PyModule_GetDict(module);

		PyObject *key, *value;
		Py_ssize_t pos = 0;
		while (PyDict_Next(dict, &pos, &key, &value)) {
			std::string _key (PyUnicode_AsUTF8(key));

			if (_key.rfind(input, 0) == 0) {
				vec.push_back(Variant(_key));
			}
		}

		return vec;
	}
}
