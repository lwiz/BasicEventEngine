/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_INPUT_KEYBIND_H
#define BEE_INPUT_KEYBIND_H 1

#include <string>
#include <functional>

#include <SDL2/SDL.h>

namespace bee {
	struct KeyBind {
		std::string name;
		SDL_Keycode key;
		bool is_repeatable;
		std::function<void (const SDL_Event*)> func;

		KeyBind(const std::string&, SDL_Keycode, bool, std::function<void (const SDL_Event*)>);
		KeyBind(const std::string&, bool, std::function<void (const SDL_Event*)>);
		KeyBind(const std::string&, std::function<void (const SDL_Event*)>);
		KeyBind(const std::string&);
		KeyBind();

		void call(const SDL_Event*);
	};
}

#endif // BEE_INPUT_KEYBIND_H
