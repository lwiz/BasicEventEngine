/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "filter.hpp"

#include "../resource/room.hpp"

namespace bee { namespace internal {
	/**
	* Passes the Bullet Physics broadphase collision to Room::check_collision_filter()
	*/
	bool PhysicsFilter::needBroadphaseCollision(btBroadphaseProxy* proxy0, btBroadphaseProxy* proxy1) const {
		return Room::check_collision_filter(proxy0, proxy1);
	}
}}
