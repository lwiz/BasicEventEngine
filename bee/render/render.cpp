/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "../defines.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "render.hpp" // Include the engine headers

#include "../engine.hpp"

#include "../util/real.hpp"

#include "../init/gameoptions.hpp"

#include "../messenger/messenger.hpp"

#include "../core/enginestate.hpp"
#include "../core/rooms.hpp"
#include "../core/window.hpp"

#include "camera.hpp"
#include "drawing.hpp"
#include "renderer.hpp"
#include "shader.hpp"
#include "viewport.hpp"

#include "../resource/texture.hpp"
#include "../resource/light.hpp"

namespace bee { namespace render {
	namespace internal {
		GLuint target = 0;
		Texture* target_tex = nullptr;

		std::map<const Texture*,std::list<TextureDrawData>> textures;
		std::list<std::shared_ptr<LightData>> lights_static;
		std::list<std::shared_ptr<LightData>> lights_semistatic;
		std::list<std::shared_ptr<LightData>> lights_dynamic;

		ShaderProgram* program = nullptr;

		Texture* lightmap_static = nullptr;
		Texture* lightmap_semistatic = nullptr;
	}

	int internal::init_lightmaps() {
		if (lightmap_static != nullptr) {
			delete lightmap_static;
			lightmap_static = nullptr;
		}
		if (lightmap_semistatic != nullptr) {
			delete lightmap_semistatic;
			lightmap_semistatic = nullptr;
		}

		lightmap_static = new Texture();
		glActiveTexture(GL_TEXTURE1);
		lightmap_static->load_as_target(get_window().w, get_window().h);
		lightmap_semistatic = new Texture();
		glActiveTexture(GL_TEXTURE2);
		lightmap_semistatic->load_as_target(get_window().w, get_window().h);
		glActiveTexture(GL_TEXTURE0);

		Texture* old_target = get_target();
		RGBA old_color = draw_get_color();
		draw_set_color({0, 0, 0, 0});
		//draw_set_color({0, 0, 255, 255});
		//draw_set_color({255, 255, 255, 255});
		set_target(lightmap_static);
		clear();
		set_target(lightmap_semistatic);
		clear();
		set_target(old_target);
		draw_set_color(old_color);

		return 0;
	}

	/**
	* Set whether to enable lighting or not.
	* @note This should be used to disable lighting only on specific elements, e.g. the HUD.
	* @note After calling this function it is the user's job to reset the lighting to the previous state.
	* @param is_lightable whether to enable lighting
	*/
	void set_is_lightable(bool is_lightable) {
		if (get_program()->get_location("is_lightable", false) != -1) {
			glUniform1i(get_program()->get_location("is_lightable"), (is_lightable) ? 1 : 0);
		}
	}

	/**
	* Prepend a shader string with the current OpenGL version.
	* @note This allows shaders to be written that conform to multiple OpenGL versions.
	* @param shader the shader source code
	*
	* @returns the prepended string
	*/
	std::string opengl_prepend_version(const std::string& shader) {
		std::string spacer;
		if ((shader.find("/*") != 0)&&(shader.find("//") != 0)&&(shader.find("\n") != 0)) {
			spacer = "\n";
		}

		switch (static_cast<E_RENDERER>(get_option("renderer_type").i)) {
			case E_RENDERER::OPENGL4: {
				if (GL_VERSION_4_1) {
					return "#version 410 core" + spacer + shader;
				}
			}
			case E_RENDERER::OPENGL3: {
				if (GL_VERSION_3_3) {
					return "#version 330 core" + spacer + shader;
				}
			}
			default: {
				return shader;
			}
		}
	}

	bool get_3d() {
		return engine->renderer->render_is_3d;
	}
	/**
	* Set whether 3D mode is enabled or not.
	* @param is_3d whether to enable 3D mode
	*/
	void set_3d(bool is_3d) {
		engine->renderer->render_is_3d = is_3d;

		if (engine->renderer->render_is_3d) {
			glEnable(GL_DEPTH_TEST);
		} else {
			glDisable(GL_DEPTH_TEST);
		}

		internal::update_camera();
	}
	const Camera& get_camera() {
		return engine->renderer->render_camera;
	}
	/**
	* Set the camera parameters.
	* @param camera the new camera to render as
	*
	* @retval 0 success
	* @retval 1 skipped camera reset since the provided camera is the same as the current one
	*/
	int set_camera(const Camera& camera) {
		if (engine->renderer->render_camera == camera) {
			return 1;
		}

		engine->renderer->render_camera = camera;

		internal::update_camera();

		return 0;
	}
	int reset_camera() {
		if (get_3d()) {
			engine->renderer->render_camera = Camera(glm::vec3(0.0f, 0.0f, -540.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
		} else {
			engine->renderer->render_camera = Camera(static_cast<float>(get_window().w), static_cast<float>(get_window().h));
		}

		internal::update_camera();

		return 0;
	}
	void internal::update_camera() {
		if (engine->renderer->render_camera.width == 0.0) {
			engine->renderer->render_camera.width = static_cast<float>(get_window().w);
		}
		if (engine->renderer->render_camera.height == 0.0) {
			engine->renderer->render_camera.height = static_cast<float>(get_window().h);
		}

		if (get_program()->get_location("f_camera", false) != -1) {
			glUniform3fv(get_program()->get_location("f_camera"), 1, glm::value_ptr(engine->renderer->render_camera.position));
		}

		calc_projection();
	}
	const glm::mat4& get_projection() {
		return engine->renderer->projection_cache;
	}
	/**
	* Recalculate the projection matrix for the current camera.
	*
	* @returns the calculated matrix
	*/
	const glm::mat4& calc_projection() {
		const Camera& cam = get_camera();

		engine->renderer->projection_cache = glm::mat4(1.0f);
		if (get_3d()) {
			engine->renderer->projection_cache = glm::perspective(
				static_cast<float>(util::degtorad(cam.fov)),
				cam.width/cam.height,
				cam.z_near, cam.z_far
			);
			engine->renderer->projection_cache *= glm::lookAt(
				cam.position,
				cam.position+cam.direction,
				cam.orientation
			);
		} else {
			engine->renderer->projection_cache = glm::ortho(
				0.0f, cam.width,
				cam.height, 0.0f,
				0.0f, cam.z_far
			);
		}

		return engine->renderer->projection_cache;
	}

	/**
	* Set the new drawing viewport within the window.
	* @see https://wiki.libsdl.org/SDL_RenderSetViewport for details
	* @param viewport the rectangle defining the desired viewport
	*/
	void set_viewport(std::shared_ptr<ViewPort> viewport) {
		glm::mat4 projection (get_projection());
		glm::mat4 view;
		glm::vec4 port;

		if (viewport == nullptr) { // If the viewport is not defined then set the drawing area to the entire screen
			view = glm::mat4(1.0f);
			port = glm::vec4(0.0f, 0.0f, get_window().w, get_window().h);
		} else { // If the viewport is defined then use it
			view = glm::translate(glm::mat4(1.0f), glm::vec3(-viewport->view.x, -viewport->view.y, 0.0f));
			port = glm::vec4(viewport->port.x, viewport->port.y, viewport->port.w, viewport->port.h);
		}

		glUniformMatrix4fv(get_program()->get_location("view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniform4fv(get_program()->get_location("port", false), 1, glm::value_ptr(port));
		glUniformMatrix4fv(get_program()->get_location("projection"), 1, GL_FALSE, glm::value_ptr(projection));
	}

	/**
	* Render a Texture from it's queued TextureDrawData.
	* @param td the data to transform the Texture with
	*/
	void internal::render_texture(const TextureDrawData& td) {
		glUniformMatrix4fv(get_program()->get_location("model"), 1, GL_FALSE, glm::value_ptr(td.model));
		glUniformMatrix4fv(get_program()->get_location("rotation"), 1, GL_FALSE, glm::value_ptr(td.rotation));
		glUniform4fv(get_program()->get_location("colorize"), 1, glm::value_ptr(td.color));

		// Bind the texture coordinates
		glEnableVertexAttribArray(get_program()->get_location("v_texcoord"));
		glBindBuffer(GL_ARRAY_BUFFER, td.buffer);
		glVertexAttribPointer(
			get_program()->get_location("v_texcoord"),
			2,
			GL_FLOAT,
			GL_FALSE,
			0,
			0
		);

		// Draw the triangles which form the rectangular subimage
		int size;
		glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
		glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
	}
	/**
	* Queue a Texture and some associated TextureDrawData.
	* @note This function is called by Texture::draw() which should normally be used instead.
	* @param texture the Texture to queue for drawing
	* @param data the draw data which specifies instanced transforms
	*/
	void queue_texture(const Texture* texture, const TextureDrawData& data) {
		std::map<const Texture*,std::list<TextureDrawData>>::iterator tex (internal::textures.find(texture));
		if (tex == internal::textures.end()) {
			internal::textures.emplace(texture, std::list<TextureDrawData>({data}));
		} else {
			tex->second.push_back(data);
		}
	}
	/**
	* Render all of the queued Textures.
	* @returns the number of Textures that were rendered.
	*/
	size_t render_textures() {
		size_t s = internal::textures.size();
		for (auto& t : internal::textures) {
			glBindVertexArray(t.second.front().vao); // Bind the VAO for the texture

			glUniform1i(get_program()->get_location("f_texture"), 0);
			glBindTexture(GL_TEXTURE_2D, t.second.front().texture);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, t.second.front().ibo);

			for (auto& td : t.second) {
				internal::render_texture(td);
			}

			glBindVertexArray(0); // Unbind the VAO
		}

		glUniformMatrix4fv(get_program()->get_location("model"), 1, GL_FALSE, glm::value_ptr(glm::mat4(1.0f))); // Reset the partial transformation matrix
		glUniformMatrix4fv(get_program()->get_location("rotation"), 1, GL_FALSE, glm::value_ptr(glm::mat4(1.0f))); // Reset the rotation matrix

		internal::textures.clear();

		return s;
	}

	/**
	* Queue a LightData for rendering after normalizing some of its components.
	* @param comp_type if the type is STATIC it only needs to be queued once
	* @param lighting the data to render with
	*
	* @retval 0 success
	* @retval 1 failed to queue since the computation type has changed
	* @retval 2 failed to queue since dynamic light rendering is nonexistent
	* @retval 3 failed to queue since dynamic light rendering is full
	*/
	int queue_light(E_COMPUTATION comp_type, std::shared_ptr<LightData> ld) {
		if (ld->comp_type != E_COMPUTATION::NOTHING) {
			if (ld->comp_type != comp_type) {
				messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to queue LightData: computation type changed");
				return 1;
			}
		}
		ld->comp_type = comp_type;

		int amount = get_program()->get_location("light_amount", false);
		if (amount < 0) {
			messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to queue LightData: lighting not supported in shader");
			return 2;
		}

		switch (ld->comp_type) {
			case E_COMPUTATION::STATIC: {
				if (internal::lights_static.size() >= static_cast<unsigned int>(amount)) {
					messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to queue LightData: light limit exceeded");
					return 3;
				}

				internal::lights_static.push_back(ld);

				break;
			}
			case E_COMPUTATION::SEMISTATIC: {
				if (internal::lights_semistatic.size() >= static_cast<unsigned int>(amount)) {
					messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to queue LightData: light limit exceeded");
					return 3;
				}

				internal::lights_semistatic.push_back(ld);

				break;
			}
			case E_COMPUTATION::SEMIPLAYER:
			case E_COMPUTATION::PLAYER:
			case E_COMPUTATION::DYNAMIC: {
				if (internal::lights_dynamic.size() >= static_cast<unsigned int>(amount)) {
					messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to queue LightData: light limit exceeded");
					return 3;
				}

				internal::lights_dynamic.push_back(ld);

				break;
			}
			case E_COMPUTATION::NOTHING:
			default: {}
		}

		return 0;
	}
	void internal::set_light(int i, std::shared_ptr<LightData> ld) {
		glm::vec4 c (ld->color.r, ld->color.g, ld->color.b, ld->color.a);
		c /= 255.0f;

		glUniform1i(get_program()->get_location("lights[" + std::to_string(i) + "].type"), static_cast<int>(ld->type));
		glUniform4fv(get_program()->get_location("lights[" + std::to_string(i) + "].position"), 1, glm::value_ptr(ld->position));
		glUniform4fv(get_program()->get_location("lights[" + std::to_string(i) + "].direction"), 1, glm::value_ptr(ld->direction));
		glUniform4fv(get_program()->get_location("lights[" + std::to_string(i) + "].attenuation"), 1, glm::value_ptr(ld->attenuation));
		glUniform4fv(get_program()->get_location("lights[" + std::to_string(i) + "].color"), 1, glm::value_ptr(c));
	}
	/**
	* Render all of the queued Lights.
	* @returns the number of Lights that were rendered or -1 on failure
	*/
	size_t render_lights() {
		if (get_program()->get_location("light_amount", false) == -1) {
			internal::lights_static.clear();
			internal::lights_semistatic.clear();
			internal::lights_dynamic.clear();
			return -1;
		}

		RGBA old_color = draw_get_color();

		// Draw static lights to their lightmap
		size_t i = 0;
		Texture* old_target = get_target();
		set_target(internal::lightmap_static);
		for (auto& ld : internal::lights_static) {
			if (i >= BEE_MAX_LIGHTS) {
				messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to draw all LightData: light limit exceeded");
				break;
			}

			internal::set_light(i, ld);

			i++;
		}
		glUniform1i(get_program()->get_location("light_amount"), i);
		glUniform1i(get_program()->get_location("is_gen_lightmap"), true);
		draw_quad({0, 0, 0}, {get_window().w, get_window().h, 1}, -1, {255, 255, 255, 255});
		render();
		glUniform1i(get_program()->get_location("is_gen_lightmap"), false);

		// Draw semistatic lights to their lightmap
		for (auto& ld : internal::lights_semistatic) {
			if (ld->has_updated) {
				// Redraw cache Texture
				if (ld->cache == nullptr) {
					ld->cache = new Texture();
					ld->cache->load_as_target(get_window().w, get_window().h);
				}

				set_target(ld->cache);
				draw_set_color({0, 0, 0, 0});
				clear();

				internal::set_light(0, ld);
				glUniform1i(get_program()->get_location("light_amount"), 1);
				draw_quad({0, 0, 0}, {get_window().w, get_window().h, 1}, -1, {255, 255, 255, 255});
				render();
				ld->has_updated = false;
			}
		}
		set_target(internal::lightmap_semistatic);
		draw_set_color({0, 0, 0, 0});
		clear();
		for (auto& ld : internal::lights_semistatic) {
			if (ld->has_updated) {
				messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to draw semistatic LightData: couldn't update cache");
				continue;
			}

			TextureDrawData td = ld->cache->compute_draw_data(0, 0, 0, -1, -1, 0.0, {255, 255, 255, 255});

			glBindVertexArray(td.vao); // Bind the VAO for the texture

			glUniform1i(get_program()->get_location("f_texture"), 0);
			glBindTexture(GL_TEXTURE_2D, td.texture);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, td.ibo);

			internal::render_texture(td);

			glBindVertexArray(0); // Unbind the VAO
		}
		// BEE_MAX_LIGHTS doesn't need to be taken into account since each semistatic light is drawn individually
		glUniform1i(get_program()->get_location("light_amount"), 0);
		render();
		set_target(old_target);
		glUniform1i(get_program()->get_location("is_gen_lightmap"), false);
		internal::lights_semistatic.clear();

		// Set dynamic lights
		i = 0;
		for (auto& ld : internal::lights_dynamic) {
			if (i >= BEE_MAX_LIGHTS) {
				messenger::send({"engine", "render"}, E_MESSAGE::WARNING, "Failed to draw all LightData: light limit exceeded");
				break;
			}

			internal::set_light(i, ld);

			i++;
		}
		glUniform1i(get_program()->get_location("light_amount"), i);
		internal::lights_dynamic.clear();

		// Set lightmaps
		if (get_program()->get_location("lightmap_static", false) != -1) {
			glActiveTexture(GL_TEXTURE1);
			glUniform1i(get_program()->get_location("lightmap_static"), 1);
		}
		if (get_program()->get_location("lightmap_semistatic", false) != -1) {
			glActiveTexture(GL_TEXTURE2);
			glUniform1i(get_program()->get_location("lightmap_semistatic"), 2);
		}
		glActiveTexture(GL_TEXTURE0);

		draw_set_color(old_color);

		return i;
	}
	/**
	* Clear the rendered Lights by resetting the shader's count.
	*
	* @retval 0 success
	* @retval 1 failed since the shader doesn't allow lighting
	*/
	int clear_lights() {
		if (get_program()->get_location("light_amount", false) == -1) {
			return 1;
		}

		glUniform1i(get_program()->get_location("light_amount"), 0);

		return 0;
	}

	/**
	* Set the rendering target back to the screen.
	*
	* @retval 0 success
	* @retval 1 failed since the engine is in headless mode
	*/
	int reset_target() {
		if (get_option("is_headless").i) {
			return 1; // Return 1 when in headless mode
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0); // Reset the bound framebuffer
		internal::target = 0; // Reset the target
		internal::target_tex = nullptr;

		return 0;
	}
	/**
	* Set the given Texture as the render target with the given width and height.
	* @param target the Texture to use as the render target
	*
	* @retval 0 success
	* @retval 1 failed since the engine is in headless mode
	* @retval 2 failed since the Texture isn't loaded as a target, see Texture::load_as_target()
	*/
	int set_target(Texture* target) {
		if (get_option("is_headless").i) {
			return 1; // Return 1 when in headless mode
		}

		if (target == nullptr) { // If the given target is nullptr then reset the render target
			reset_target();
		} else {
			if (!target->get_is_loaded()) {
				return 2;
			}
			internal::target = target->set_as_target();
			internal::target_tex = target;
		}

		return 0;
	}
	/**
	* @returns the Texture that is being used as the render target
	*/
	Texture* get_target() {
		return internal::target_tex;
	}
	/**
	* Set the new ShaderProgram.
	* @param new_program the new shader to use for rendering
	*/
	void set_program(ShaderProgram* new_program) {
		internal::program = new_program;
	}
	/**
	* @returns the current ShaderProgram
	*/
	ShaderProgram* get_program() {
		return internal::program;
	}

	/**
	* Clear the window's color and depth bits.
	*/
	void clear() {
		draw_set_color(*(engine->color));
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	/**
	* Swap the window buffer to screen.
	*/
	void render() {
		SDL_GL_SwapWindow(engine->renderer->window);
	}
}}
