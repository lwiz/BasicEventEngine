/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#ifndef BEE_RENDER_VIEWPORT_H
#define BEE_RENDER_VIEWPORT_H 1

#include <functional>
#include <memory>

#include <SDL2/SDL.h> // Include the required SDL headers

namespace bee {
	class Texture;
	class ShaderProgram;

	struct ViewPort {
		bool is_active;

		SDL_Rect view;
		SDL_Rect port;

		std::shared_ptr<Texture> texture;

		std::function<void (std::shared_ptr<ViewPort>)> update_func;

		ShaderProgram* shader;

		ViewPort();
		ViewPort(bool, SDL_Rect, SDL_Rect);
		int load();

		std::pair<int,int> get_offset() const;

		std::pair<int,int> set_view_center(int, int);

		void update(std::shared_ptr<ViewPort>);
		void draw(std::shared_ptr<ViewPort>);
	};
}

#endif // BEE_RENDER_VIEWPORT_H
