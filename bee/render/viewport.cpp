/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

#include "viewport.hpp"

#include "../defines.hpp"

#include "../core/enginestate.hpp"
#include "../core/window.hpp"

#include "../render/render.hpp"
#include "../render/renderer.hpp"
#include "../render/shader.hpp"

#include "../resource/texture.hpp"

namespace bee {
	ViewPort::ViewPort() :
		ViewPort(
			true,
			{0, 0, DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT},
			{0, 0, DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT}
		)
	{}
	ViewPort::ViewPort(bool _is_active, SDL_Rect _view, SDL_Rect _port) :
		is_active(_is_active),

		view(_view),
		port(_port),

		texture(std::make_shared<Texture>()),

		update_func(nullptr),

		shader(nullptr)
	{}

	/**
	* Load the interior Texture as a target the size of the window.
	*
	* @returns whether the Texture load failed or not
	*/
	int ViewPort::load() {
		if (!texture->get_is_loaded()) {
			int w = port.w;
			int h = port.h;

			if (w == -1) {
				w = get_window_size().first;
			}
			if (h == -1) {
				h = get_window_size().second;
			}

			return texture->load_as_target(w, h);
		}
		return 0;
	}

	/**
	* Get the offset of the ViewPort from display coordinates.
	*
	* @returns the offset as a pair of xy-coordinates
	*/
	std::pair<int,int> ViewPort::get_offset() const {
		return {
			view.x - port.x,
			view.y - port.y
		};
	}

	/**
	* Center the view on the given coordinates.
	* @param x
	* @param y
	*
	* @returns the computed center
	*/
	std::pair<int,int> ViewPort::set_view_center(int x, int y) {
		int w = view.w;
		int h = view.h;
		if (w == -1) {
			w = get_window_size().first;
		}
		if (h == -1) {
			h = get_window_size().second;
		}

		int nx = x - w/2;
		int ny = y - h/2;

		view.x = nx;
		view.y = ny;

		return {nx, ny};
	}

	/**
	* Run the update callback.
	* @param self the shared_ptr to this ViewPort
	*/
	void ViewPort::update(std::shared_ptr<ViewPort> self) {
		load();
		if (update_func != nullptr) {
			update_func(self);
		}
	}
	/**
	* Draw the interior Texture with the interior shader.
	* @param self the shared_ptr to this ViewPort
	*/
	void ViewPort::draw(std::shared_ptr<ViewPort> self) {
		ShaderProgram* s (engine->renderer->program);
		if (shader != nullptr) {
			s = shader;
		}
		s->apply();
		render::set_viewport(nullptr);

		update(self);
		texture->draw(port.x, port.y, 0);

		glUniform1i(s->get_location("flip"), 2);
		render::render_textures();
		glUniform1i(s->get_location("flip"), 0);
	}
}
