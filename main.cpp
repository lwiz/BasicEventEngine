/*
* Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
*
* This file is part of BEE.
* BEE is free software and comes with ABSOLUTELY NO WARRANTY.
* See LICENSE for more details.
*/

// Let SDL2/SDL_main.hpp know that we got this
#define SDL_MAIN_HANDLED

#include "bee/engine.hpp" // Include the engine headers
#include "bee/messenger/messenger.hpp"

#include "bee/init/info.hpp"
#include "bee/init/gameoptions.hpp"
#include "bee/core/console.hpp"
#include "bee/fs/fs.hpp"

int init_resources() {return 0;}
int close_resources() {return 0;}

int main(int argc, char* argv[]) {
	// Add a logfile to the messenger
	bee::messenger::add_log(bee::get_game_name() + ".log", bee::E_OUTPUT::NORMAL);

	// Initialize the game engine
	if (bee::init(argc, argv, nullptr, {}, {})) {
		bee::messenger::handle();
		return 1; // Return 1 on initialization failure
	}

	// Output initialization message
	bee::messenger::send({"engine", "init"}, bee::E_MESSAGE::INFO, "Initialized " + bee::get_game_name() + " v" + bee::get_game_version().to_str());

	// Load resources and run config
	bee::fs::load_level("main", "main", "", true);
	bee::console::run("execfile(\"cfg/config.py\")");

	// Run the game engine event loop
	// This loop will run until the user closes the window, the game closes itself, or the game throws an exception
	int r = bee::loop();

	// Clean up resources and quit SDL and other libraries
	bee::close();

	if (r != 0) { // If an exception was caught by the loop
		return 2; // Return 2 on exception during game loop
	}
	return 0; // Return 0 on successful run
}
